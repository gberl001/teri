# TERI
Tax Efficient Retirement Income

JDK Version:        1.8
SBT:                0.13.8
PLAY:               2.4.6
Typesafe Activator: 1.3.6
Scala:              2.11.6


### SQLite Configuration
Ensure that your path is pointing to correct DB in `application.conf`
`conf/application.conf` isn't tracked by Git -- find the file in your local history to use it

### Troubleshooting
Helpful activator commands:
```bash
% activator clean, compile
```
