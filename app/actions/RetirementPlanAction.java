package actions;

import controllers.routes;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import static play.mvc.Controller.flash;
import static play.mvc.Controller.session;

/**
 * Created by piper on 3/5/16.
 */
public class RetirementPlanAction extends Action<RetirementPlanSelectionRequired> {

    /**
     * Executes this action with the given HTTP context and returns the result.
     *
     * @param ctx
     */
    @Override
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        if (configuration.isRequired()) {

            String authUser = session("authorized");
            String retirementPlanSelected = session("retirementPlanSelected");

            // If not authorized, defer to login page
            if (authUser == null) {
                return F.Promise.pure(redirect(routes.Application.showLogin()));
            } else if (retirementPlanSelected == null || retirementPlanSelected.equals("NO"))  {  // Redirect to plan selection
                flash("error", "Please select a retirement plan.");
                return F.Promise.pure(redirect(routes.Application.landingPage()));
            }
        }
        return delegate.call(ctx);
    }
}

