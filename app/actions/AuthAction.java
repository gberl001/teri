package actions;

import controllers.routes;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import static play.mvc.Controller.session;

/**
 * Created by BerlGeof on 2/22/2016.
 */
public class AuthAction extends Action<TERIAuthRequired> {

	/**
	 * Executes this action with the given HTTP context and returns the result.
	 *
	 * @param ctx
	 */
	@Override
	public F.Promise<Result> call(Http.Context ctx) throws Throwable {
		if (configuration.isRequired()) {
			String authUser = session("authorized");
			// If not authorized, defer to login page
			if (authUser == null) {
				return F.Promise.pure(redirect(routes.Application.showLogin()));
			}
		}
		return delegate.call(ctx);
	}


	//
//	@Override
//	public Result call(Http.Context context) throws Throwable {
//
//		String authHeader = context.request().getHeader(AUTHORIZATION);
//		if (authHeader == null) {
//			context.response().setHeader(WWW_AUTHENTICATE, REALM);
//			return unauthorized();
//		}
//
//		String auth = authHeader.substring(6);
//		byte[] decodedAuth = new sun.misc.BASE64Decoder().decodeBuffer(auth);
//		String[] credString = new String(decodedAuth, "UTF-8").split(":");
//
//		if (credString == null || credString.length != 2) {
//			return unauthorized();
//		}
//
//		String username = credString[0];
//		String password = credString[1];
//		Auth authUser = Auth.authenticate(username, password);
//
//		return (authUser == null) ? unauthorized() : delegate.call(context);
//	}
}
