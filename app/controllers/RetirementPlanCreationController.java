package controllers;

import actions.TERIAuthRequired;
import models.MonetaryAccount;
import models.Mortgage;
import models.RetirementPlan;
import models.AccountInvestments;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.creator.creatorHome;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static play.data.Form.form;

/**
 * RetirementPlanCreationController.java
 * <p>
 * <p>
 * Created by BerlGeof on 2/1/2016.
 */
@TERIAuthRequired()
public class RetirementPlanCreationController extends Controller {

	/**
	 * Create a new retirement plan (this is the landing page)
	 *
	 * @return a blank form for creating a plan
	 */
	public static Result showRetirementPlan(long id) {
		//		Form<RetirementPlan> retirementPlanForm = form(RetirementPlan.class);

		// Get the plan from the DB
		if (id != 0) {

			RetirementPlan plan = RetirementPlan.find.byId(id);
			// If it's found, fill the form but otherwise we'll just return a blank form
			if (plan != null) {
				System.out.println("Filling Plan");
//				for (MonetaryAccount acct : plan.retiree1.getMonetaryAccounts()) {
//					System.out.println(acct.name + ": " + acct.getClass().getSimpleName());
//				}

				return ok(creatorHome.render(form(RetirementPlan.class).fill(plan), id));
			}
		}

		return ok(creatorHome.render(form(RetirementPlan.class), id));
	}

	public static Result updateRetirementPlan(long id) {
		Form<RetirementPlan> retirementPlanForm = form(RetirementPlan.class).bindFromRequest();

		// if(retirementPlanForm.field("retiree2.maxSocialSecurityBenefits").errors().length() != 0) {
		// 	retirementPlan.bind()
		// }


		// Handle cancel button and empty button submissions
		String[] postAction = request().body().asFormUrlEncoded().get("action");
		if (postAction == null || postAction.length == 0) {
			return redirect(routes.RetireeController.listRetirementPlan());
		} else {
			String buttonAction = postAction[0];  // this is the "value" attribute of the button tag
			if ("cancel".equals(buttonAction)) {
				return redirect(routes.RetireeController.listRetirementPlan());
			}
		}
//
//		// Present the error dialog
//		if (retirementPlanForm.hasErrors()) {
//
//			// RetirementPlan plan = retirementPlanForm.get();  FIX: doesn't work yet, see below
//
//			// TODO: drop retiree2 keys from data map if name is empty
//			if (retirementPlanForm.data().get("retiree2.lastName").isEmpty() || retirementPlanForm.data().get("retiree2.firstName").isEmpty()) {
//				// Collect all keys that start with retiree2
//				Set<String> retiree2References = retirementPlanForm.data().entrySet()
//						.stream()
//						.filter(entry -> entry.getKey().startsWith("retiree2.monetaryAccounts"))
//						.map(Map.Entry::getKey)
//						.collect(Collectors.toSet());
//
//				// Remove them from the map
//				retirementPlanForm.data().keySet().removeAll(retiree2References);
//
//				retiree2References = retirementPlanForm.data().entrySet()
//						.stream()
//						.filter(entry -> entry.getKey().startsWith("retiree2.investmentAccounts"))
//						.map(Map.Entry::getKey)
//						.collect(Collectors.toSet());
//
//				// Remove them from the map
//				retirementPlanForm.data().keySet().removeAll(retiree2References);
//
////				retirementPlanForm.data().put("retiree2", "");
//
//				// Now collect the retiree 2 ERROR references
//				retiree2References = retirementPlanForm.errors().entrySet()
//						.stream()
//						.filter(entry -> entry.getKey().startsWith("retiree2"))
//						.map(Map.Entry::getKey)
//						.collect(Collectors.toSet());
//
//				// And remove them
//				retirementPlanForm.errors().keySet().removeAll(retiree2References);
//			}
//
//			// FIX: Trying to re-bind the map, not quite the right way
//			retirementPlanForm.bind(retirementPlanForm.data());

			if (retirementPlanForm.hasErrors()) {
				// ERRORS
				return badRequest(creatorHome.render(retirementPlanForm, id));
			} else {
				System.out.println("No errors!");
			}
//		}

		RetirementPlan plan = retirementPlanForm.get();
		// There are optional retiree account forms to check and remove
		// Mortgage r1LastMortgage = plan.retiree1.mortgages.get(plan.retiree1.mortgages.size() - 1);
		// if (r1LastMortgage.name.equals("")) {
		// 	plan.retiree1.mortgages.remove(r1LastMortgage);
		// }

		MonetaryAccount r1LastMonetaryAcct = plan.retiree1.monetaryAccounts.get(
				plan.retiree1.monetaryAccounts.size() - 1);

		if (r1LastMonetaryAcct.name.equals("")) {
			plan.retiree1.monetaryAccounts.remove(r1LastMonetaryAcct);
		}

		AccountInvestments r1LastInvestmentAcct = plan.retiree1.investmentAccounts.get(
				plan.retiree1.investmentAccounts.size() - 1);

		if (r1LastInvestmentAcct.name.equals("")) {
			plan.retiree1.investmentAccounts.remove(r1LastInvestmentAcct);
		}

		// Retiree 2 is optional so set it to null if they didn't fill the field in
		if (plan.retiree2.firstName.equals("")) {
			plan.retiree2 = null;
		} else {
			// TODO: Check for retiree2 Form Errors and return if they exist
			// Since we added a blank form for each retiree to add a monetary account or an
			// 	investment account, we need to remove those blank accounts if they weren't added
			//	in the form submission.

			// Remove Retiree 2's last Monetary account if it's blank (assume no name means blank)
			MonetaryAccount r2LastMonetaryAcct = plan.retiree2.monetaryAccounts.get(
					plan.retiree2.monetaryAccounts.size() - 1);
			if (r2LastMonetaryAcct.name.equals("")) {
				plan.retiree2.monetaryAccounts.remove(r2LastMonetaryAcct);
			}
			// Remove Retiree 2's last Investment account if it's blank
			AccountInvestments r2LastInvestmentAcct = plan.retiree2.investmentAccounts.get(
				plan.retiree2.investmentAccounts.size() - 1);
			if (r2LastInvestmentAcct.name.equals("")) {
				plan.retiree2.investmentAccounts.remove(r2LastInvestmentAcct);
			}
		}

		String action = "Updated";
		if (id == 0) {
			plan.save();
			action = "Created";
		} else {
			plan.update();
		}
		flash("success", "'" + plan.name + "' has been " + action);
		return redirect(routes.RetirementPlanCreationController.showRetirementPlan(plan.id));
	}

	public static Result deleteMonetaryAccount(long planId, long acctId) {

		MonetaryAccount acctToDelete = MonetaryAccount.find.byId(acctId);
		if (acctToDelete != null) {
			acctToDelete.delete();
		}

		flash("success", "Account Deleted");
		return redirect(routes.RetirementPlanCreationController.showRetirementPlan(planId));
	}

	public static Result deleteInvestmentAccount(long planId, long acctId) {

		AccountInvestments acctToDelete = AccountInvestments.find.byId(acctId);
		if (acctToDelete != null) {
			acctToDelete.delete();
		}

		flash("success", "Account Deleted");
		return redirect(routes.RetirementPlanCreationController.showRetirementPlan(planId));
	}


	public static Result deleteMortgageAccount(long planId, long acctId) {

		Mortgage acctToDelete = Mortgage.find.byId(acctId);
		if (acctToDelete != null) {
			acctToDelete.delete();
		}

		flash("success", "Mortgage Deleted");
		return redirect(routes.RetirementPlanCreationController.showRetirementPlan(planId));
	}

}
