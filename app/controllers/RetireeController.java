package controllers;

import actions.TERIAuthRequired;
import models.MonetaryAccount;
import models.Retiree;
import models.RetirementPlan;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Content;
import views.html.creator.creatorHome;
import views.html.editor.accounts.*;

import static play.data.Form.form;

/**
 * Created by BerlGeof on 2/1/2016.
 */
@TERIAuthRequired()
public class RetireeController extends Controller {

	public static Result MONETARY_ACCOUNT_HOME = redirect(routes.RetireeController.listMonetaryAccount());
	public static Result RETIREE_HOME = redirect(routes.RetireeController.listRetiree());
	public static Result RET_PLAN_HOME = redirect(routes.RetireeController.listRetirementPlan());

	public static Result createMonetaryAccount() {
		Form<MonetaryAccount> monetaryAccountForm = form(MonetaryAccount.class).bindFromRequest();
		System.out.println(monetaryAccountForm.data().keySet());

		System.out.println(monetaryAccountForm.get());
		System.out.println("Hello world");
		return ok();//createMonetaryAccount.render(monetaryAccountForm));
	}

	public static Result saveMonetaryAccount() {
		Form<MonetaryAccount> monetaryAccountForm = form(MonetaryAccount.class).bindFromRequest();
		if (monetaryAccountForm.hasErrors()) {
			return badRequest(createMonetaryAccount.render(monetaryAccountForm));
		}
		monetaryAccountForm.get().save();
		flash("success",
				"MonetaryAccount '" + monetaryAccountForm.get().getName() + "' has been " +
						"created");
		return MONETARY_ACCOUNT_HOME;
	}

	public static Result editMonetaryAccount(long id) {
		Form<MonetaryAccount> monetaryAccountForm = form(MonetaryAccount.class).fill(MonetaryAccount.find.byId(id));
		return ok(editMonetaryAccount.render(id, monetaryAccountForm));
	}

	public static Result updateMonetaryAccount(long id) {
		Form<MonetaryAccount> monetaryAccountForm = form(MonetaryAccount.class).bindFromRequest();
		if (monetaryAccountForm.hasErrors()) {
			return badRequest(editMonetaryAccount.render(id, monetaryAccountForm));
		}
//		monetaryAccountForm.get().update();
		flash("success", "MonetaryAccount '" +
				monetaryAccountForm.get().name + " has been created.");
		return MONETARY_ACCOUNT_HOME;
	}

	public static Result deleteMonetaryAccount(long id) {
		MonetaryAccount.find.ref(id).delete();
		flash("success", "MonetaryAccount record '" + id + "' has been deleted");
		return MONETARY_ACCOUNT_HOME;
	}

	public static Result listMonetaryAccount() {
		return ok(listMonetaryAccount.render(MonetaryAccount.find.all()));
	}


	public static Result listRetiree() {
		return ok(listRetiree.render(Retiree.find.all()));
	}

	public static Result createRetiree() {
		Form<Retiree> retireeForm = form(Retiree.class);
		return ok(createRetiree.render(retireeForm));
	}

	public static Result saveRetiree() {
		Form<Retiree> retireeForm = form(Retiree.class).bindFromRequest();
		if (retireeForm.hasErrors()) {
			return badRequest(createRetiree.render(retireeForm));
		}
		retireeForm.get().save();
		flash("success",
				"Retiree '" + retireeForm.get().firstName + " " + retireeForm.get().lastName + "' has been " +
						"created");
		return RETIREE_HOME;
	}

	public static Result editRetiree(long id) {
		Form<Retiree> retireeForm = form(Retiree.class).fill(Retiree.find.byId(id));

		return ok(editRetiree.render(id, retireeForm));
	}

	public static Result updateRetiree(long id) {
		Form<Retiree> retireeForm = form(Retiree.class).bindFromRequest();
		if (retireeForm.hasErrors()) {
			return badRequest(editRetiree.render(id, retireeForm));
		}
		//		retireeForm.get().setId(id);
		retireeForm.get().update();
		flash("success", "Retiree '" +
				retireeForm.get().firstName + " " + retireeForm.get().lastName + "' has been updated");
		return RETIREE_HOME;
	}

	public static Result deleteRetiree(long id) {
		Retiree.find.ref(id).delete();
		flash("success", "Retiree record '" + id + "' has been deleted");
		return RETIREE_HOME;
	}

	public static Result listRetirementPlan() {
		return ok(listRetirementPlan.render(RetirementPlan.find.all()));
	}

	public static Result createRetirementPlan() {
		Form<RetirementPlan> retirementPlanForm = form(RetirementPlan.class);
		return ok(createRetirementPlan.render(retirementPlanForm));
	}

	public static Result saveRetirementPlan() {
		Form<RetirementPlan> retirementPlanForm = form(RetirementPlan.class).bindFromRequest();
		if (retirementPlanForm.hasErrors()) {
			return badRequest(createRetirementPlan.render(retirementPlanForm));
		}
		retirementPlanForm.get().save();
		flash("success", "RetirementPlan '" + retirementPlanForm.get().name + "' has been created");
		return RET_PLAN_HOME;
	}

	public static Result editRetirementPlan(long id) {
		Form<RetirementPlan> retirementPlanForm = form(RetirementPlan.class).fill(RetirementPlan.find.byId(id));
		return ok(creatorHome.render(retirementPlanForm, id));
	}

	public static Result updateRetirementPlan(long id) {
		Form<RetirementPlan> retirementPlanForm = form(RetirementPlan.class).bindFromRequest();
		if (retirementPlanForm.hasErrors()) {
			return badRequest(creatorHome.render(retirementPlanForm, id));
		}
		//		retirementPlanForm.get().setId(id);
		retirementPlanForm.get().update();
		flash("success", "RetirementPlan '" + retirementPlanForm.get().name + "' has been updated");
		return RET_PLAN_HOME;
	}

	public static Result deleteRetirementPlan(long id) {
		RetirementPlan.find.ref(id).delete();
		flash("success", "RetirementPlan record '" + id + "' has been deleted");
		return RET_PLAN_HOME;
	}

}
