package controllers;

import actions.TERIAuthRequired;
import models.pojo.FilingStatus;
import models.pojo.TaxThresholds;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.editor.taxes.createFilingStatus;
import views.html.editor.taxes.editFilingStatus;
import views.html.editor.taxes.listFilingStatus;
import views.html.editor.taxes.createTaxSettings;
import views.html.editor.taxes.editTaxSettings;
import views.html.editor.taxes.listTaxSettings;

import static play.data.Form.form;

/**
 * Created by BerlGeof on 2/1/2016.
 */
@TERIAuthRequired()
public class TaxesController extends Controller {

	public static Result FILING_STATUS_HOME = redirect(routes.TaxesController.listFilingStatus());
	public static Result TAXES_HOME = redirect(routes.TaxesController.listTaxSettings());

	public static Result listTaxSettings() {
		return ok(listTaxSettings.render(TaxThresholds.find.all()));
	}

	public static Result createTaxSettings() {
		Form<TaxThresholds> taxThresholdsForm = form(TaxThresholds.class);
		return ok(createTaxSettings.render(taxThresholdsForm));
	}

	public static Result editTaxSettings(long id) {
		Form<TaxThresholds> taxThresholdsForm = form(TaxThresholds.class).fill(
				TaxThresholds.find.byId(id));
		return ok(editTaxSettings.render(id, taxThresholdsForm));
	}

	public static Result updateTaxSettings() {
		Form<TaxThresholds> taxThresholdsForm = form(TaxThresholds.class).bindFromRequest();
		if (taxThresholdsForm.hasErrors()) {
			return TAXES_HOME;
		}

		taxThresholdsForm.get().update();
		flash("success", "Settings for '" + taxThresholdsForm.get().year + "' has been updated");

		return TAXES_HOME;
	}

	public static Result saveTaxSettings() {
		Form<TaxThresholds> taxThresholdsForm = form(TaxThresholds.class).bindFromRequest();
		if (taxThresholdsForm.hasErrors()) {
			return badRequest(createTaxSettings.render(form(TaxThresholds.class)));
		}
		taxThresholdsForm.get().save();

		flash("success", "Settings for '" + taxThresholdsForm.get().year + "' has been saved");
		return TAXES_HOME;
	}

	public static Result deleteTaxSettings(long id) {
		TaxThresholds.find.ref(id).delete();
		flash("success", "Tax Settings record '" + id + "' has been deleted");
		return TAXES_HOME;
	}

	public static Result listFilingStatus() {
		return ok(listFilingStatus.render(FilingStatus.find.all()));
	}

	public static Result createFilingStatus() {
		Form<FilingStatus> filingStatusForm = form(FilingStatus.class);
		return ok(createFilingStatus.render(filingStatusForm));
	}

	public static Result editFilingStatus(long id) {
		Form<FilingStatus> filingStatusForm = form(FilingStatus.class).fill(
				FilingStatus.find.byId(id));
		return ok(editFilingStatus.render(id, filingStatusForm));
	}

	public static Result updateFilingStatus() {
		Form<FilingStatus> filingStatusForm = form(FilingStatus.class).bindFromRequest();
		if (filingStatusForm.hasErrors()) {
			return FILING_STATUS_HOME;
		}

		filingStatusForm.get().update();
		flash("success", "Filing Status '" + filingStatusForm.get().name + "' has been updated");

		return FILING_STATUS_HOME;
	}

	public static Result saveFilingStatus() {
		Form<FilingStatus> userForm = form(FilingStatus.class).bindFromRequest();
		if (userForm.hasErrors()) {
			return badRequest(createFilingStatus.render(form(FilingStatus.class)));
		}
		userForm.get().save();

		flash("success", "Filing Status '" + userForm.get().name + "' has been saved");
		return FILING_STATUS_HOME;
	}

	public static Result deleteFilingStatus(long id) {
		FilingStatus.find.ref(id).delete();
		flash("success", "Filing Status record '" + id + "' has been deleted");
		return FILING_STATUS_HOME;
	}

}
