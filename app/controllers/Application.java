package controllers;

import actions.RetirementPlanSelectionRequired;
import actions.TERIAuthRequired;
import models.*;
import models.pojo.FakeDB;
import models.pojo.FilingStatus;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import views.html.editor.accounts.listRetirementPlan;

import java.util.ArrayList;
import java.util.List;


import static play.data.Form.form;

public class Application extends Controller {

	/**
	 * Default ten forty calculator with a default user
	 */
	private static TenFortyCalculator tenForty = new TenFortyCalculator(new RetirementPlan(new Retiree(), new Retiree(), "Circumflex",new FilingStatus(1, FilingStatus.SINGLE), 1));
	private static final FakeDB db = new FakeDB();
	private static RetirementPlan retirementPlan;

	/**
	 * The personal info home page
	 *
	 * @return the personal info home page
	 */
	@TERIAuthRequired()
	public static Result personalInfo(long id) {
		RetirementPlan result = RetirementPlan.find.byId(id);

		retirementPlan = result;

		tenForty.setRetirementPlan(result);

		// Allow user to proceed to Summary as they've selected a plan
		session().put("retirementPlanSelected", "YES");

		return ok(personalInfo.render(result));
	}

	public static Result showLogin() {
		String authUser = session("authorized");
		// If authorized, defer to landing page
		if (authUser != null) {
			return redirect(routes.Application.landingPage());
		}
		return ok(login.render(form()));
	}

	@RetirementPlanSelectionRequired()
	public static Result summary() {
        // Redirect to RP list on error (usually if user refreshes mid-flow)
		if (retirementPlan == null) {
			return redirect(routes.RetireeController.listRetirementPlan());
		}

		retirementPlan.makeRealAccounts();

		Yoy y = new Yoy();
		RetirementPlan optimizePlan = y.superUglyHack(retirementPlan);

		ArrayList<YearlyTaxes> conventionalTaxes = y.runConventionalYoyTaxes(30, retirementPlan);

		SummaryData summaryData = new SummaryData();
		summaryData.setConventional(conventionalTaxes);

		ArrayList<YearlyTaxes> optimizedTaxes = y.runOptimizeYoyTaxes(30, summaryData.totalTaxesConventional, conventionalTaxes.size(), optimizePlan);
		summaryData.setOptimized(optimizedTaxes);
		// summaryData.setOptimized(conventionalTaxes);

		// String output = "";

		// for(YearlyTaxes yt : years) {
		// 	output += yt.retirementPlan.toString();
		// }
		// return ok(output);
		return ok(summary.render(summaryData.conventionalSummary.get(0).retirementPlan, summaryData));
	}

	public static Result login() {
		DynamicForm loginForm = Form.form().bindFromRequest();

		if (loginForm.hasErrors()) {
			return badRequest(login.render(loginForm));
		}

		// Authenticate
		Auth authUser = Auth.authenticate(loginForm.get("username"), loginForm.get("password"));

		if (authUser == null) {
			loginForm.reject("auth", "Incorrect username and/or password");
			return badRequest(login.render(loginForm));
		}

		session().put("authorized", authUser.username);
		return redirect(routes.Application.landingPage());
	}

	public static Result logout() {
		session().clear();
		return showLogin();
	}

	@TERIAuthRequired()
	public static Result landingPage() {

		// Require user to reselect plan upon returning to landing
		session().put("retirementPlanSelected", "NO");

		return ok(listRetirementPlan.render(RetirementPlan.find.all()));
	}

	/**
	 * Update the income object
	 *
	 * @return the income landing page
	 */
	@TERIAuthRequired()
	public static Result updateIncome() {

		Form<Income> incomeForm = form(Income.class).bindFromRequest();
		Form<ScheduleD> scheduleDForm = form(ScheduleD.class).bindFromRequest();
		Form<SSIWorksheet> ssiForm = form(SSIWorksheet.class).bindFromRequest();
		if (incomeForm.hasErrors() || scheduleDForm.hasErrors() || ssiForm.hasErrors()) {
			return badRequest(incomeEditForm.render(incomeForm, scheduleDForm, ssiForm));
		}

		// Should use this below... when we get an ORM
		// incomeForm.get().updateFilingStatus();
		// Instead, manually updateFilingStatus for now.
		tenForty.addAlimonyIncome(incomeForm.get().getAlimony());
		tenForty.addBusinessIncome(incomeForm.get().getBusinessIncome());
		tenForty.addFarmIncome(incomeForm.get().getFarmIncome());
		tenForty.addInterestIncome(incomeForm.get().getTaxableInterest());
		tenForty.addIraDistributions(incomeForm.get().getIraDistributions());
		tenForty.addOtherGains(incomeForm.get().getOtherGains());
		tenForty.addOtherIncome(incomeForm.get().getOtherIncome());
		tenForty.addPensionsAndAnnuities(incomeForm.get().getPensionsAndAnnuities());
		tenForty.addQualifiedDividends(incomeForm.get().getQualifiedDividends());
		tenForty.addRealEstateIncome(incomeForm.get().getRealEstate());
		tenForty.addUnemploymentIncome(incomeForm.get().getUnemployment());
		tenForty.addWages(incomeForm.get().getWages());
		tenForty.addLongTermCapitalGains(scheduleDForm.get().getNetLongTerm());
		tenForty.setSocialSecurityIncome(ssiForm.get().getSocialSecurityIncome());
		tenForty.setTaxableRefunds(incomeForm.get().getTaxableRefunds());

		// Move to the next page
		return redirect(routes.Application.agiHome());
	}

	/**
	 * The income home page
	 *
	 * @return the income home page
	 */
	public static Result incomeHome() {
		Form<Income> incomeForm = form(Income.class).fill(tenForty.income());
		Form<ScheduleD> scheduleDForm = form(ScheduleD.class).fill(tenForty.getScheduleD());
		Form<SSIWorksheet> ssiForm = form(SSIWorksheet.class).fill(
				tenForty.getSocialSecurityWorksheet());
		return ok(incomeEditForm.render(incomeForm, scheduleDForm, ssiForm));
	}


	/**
	 * Update the AGI object
	 *
	 * @return the AGI landing page
	 */
	@TERIAuthRequired()
	public static Result updateAGI() {

		Form<AGI> agiForm = form(AGI.class).bindFromRequest();
		if (agiForm.hasErrors()) {
			return badRequest(agiEditForm.render(agiForm));
		}

		tenForty.setAlimony(agiForm.get().getAlimony());
		tenForty.setDomesticProductionDeduction(agiForm.get().getDomesticProductionDeduction());
		tenForty.setEarlyWithdrawlPenalty(agiForm.get().getEarlyWithdrawlPenalty());
		tenForty.setEducatorExpenses(agiForm.get().getEducatorExpenses());
		tenForty.setForm2106Value(agiForm.get().getForm2106Value());
		tenForty.setHealthSavingsDeduction(agiForm.get().getHealthSavingsDeduction());
		tenForty.setIraDeduction(agiForm.get().getIraDeduction());
		tenForty.setMovingExpenses(agiForm.get().getMovingExpenses());
		tenForty.setSelfEmployHealthInsuranceDeduction(
				agiForm.get().getSelfEmployHealthInsuranceDeduction());
		tenForty.setSelfEmploymentDeductable(agiForm.get().getSelfEmploymentDeductable());
		tenForty.setSepSIMPPlans(agiForm.get().getSepSIMPPlans());
		tenForty.setStudentLoanInterest(agiForm.get().getStudentLoanInterest());
		tenForty.setTuitionAndFees(agiForm.get().getTuitionAndFees());

		// Move to the next page
		return redirect(routes.Application.taxAndCreditsHome());
	}

	/**
	 * The AGI home page
	 *
	 * @return the AGI home page
	 */
	public static Result agiHome() {
		Form<AGI> agiForm = form(AGI.class).fill(tenForty.agi());
		return ok(agiEditForm.render(agiForm));
	}

	/**
	 * Update the TaxAndCredits object
	 *
	 * @return the TaxAndCredits landing page
	 */
	@TERIAuthRequired()
	public static Result updateTaxAndCredits() {

		Form<TaxAndCredits> taxAndCreditsForm = form(TaxAndCredits.class).bindFromRequest();
		if (taxAndCreditsForm.hasErrors()) {
			return badRequest(taxAndCreditsEditForm.render(taxAndCreditsForm));
		}

		tenForty.setAlternativeMinTax(taxAndCreditsForm.get().getAlternativeMinTax());
		tenForty.setCharitableContributionsCredit(
				taxAndCreditsForm.get().getCharitableContributionsCredit());
		tenForty.setAdditionalChildTaxCredit(taxAndCreditsForm.get().getChildTaxCredit());
		tenForty.setDependantExpenses(taxAndCreditsForm.get().getDependentExpenses());
		tenForty.setEduCredits(taxAndCreditsForm.get().getEduCredits());
		tenForty.setForeignTaxCredit(taxAndCreditsForm.get().getForeignTaxCredit());
		tenForty.setMortgageInterestCredit(taxAndCreditsForm.get().getMortgageInterestCredit());
		tenForty.setOtherCredits(taxAndCreditsForm.get().getOtherCredits());
		tenForty.setPremiumTaxCredit(taxAndCreditsForm.get().getPremiumTaxCredit());
		tenForty.setPropertyTaxCredit(taxAndCreditsForm.get().getPropertyTaxCredit());
		tenForty.setResidentialEnergyCredits(taxAndCreditsForm.get().getResidentialEnergyCredits
				());
		tenForty.setRetirementSavingsCredit(taxAndCreditsForm.get().getRetirementSavingsCredit());

		// Move to the next page
		return redirect(routes.Application.summary());
	}

	/**
	 * The TaxAndCredits home page
	 *
	 * @return the TaxAndCredits home page
	 */
	public static Result taxAndCreditsHome() {
		Form<TaxAndCredits> taxAndCreditsForm = form(TaxAndCredits.class).fill(
				tenForty.taxAndCredits());
		return ok(taxAndCreditsEditForm.render(taxAndCreditsForm));
	}

	public static Result list() {
		return ok(list.render((List<TenFortyCalculator>) db.list()));
	}

	/**
	 * Populate the application with an existing form
	 * @param id the id of the form
	 * @return a populated application starting on the personal info page
	 */
	public static Result populate(long id) {
		tenForty = db.find(id);
		return landingPage();
	}

}
