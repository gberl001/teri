package models;

import models.pojo.TaxThresholds;

/**
 * SSIWorksheet.java
 * <p>
 * Used to calculate the total taxable amount of Social Security Benefit Income received.  Covers
 * line 20a and 20b on Form 1040 for 2014.
 * <p>
 * Created by geoffberl on 10/21/15.
 */
public class SSIWorksheet {
	// Requires from outside sources:
	// Total Income (minus SSI because that is kept here)
	// Tax Exempt Interest
	// Adjustments from AGI calculation
	// SSI Bracket for the year calculated (Referred to as MAGI)

	private int socialSecurityIncome;
	private TaxThresholds thresholds;

	public void setSocialSecurityIncome(int socialSecurityIncome) {
		this.socialSecurityIncome = socialSecurityIncome;
	}

	public int getSocialSecurityIncome() {
		return socialSecurityIncome;
	}

	public SSIWorksheet(TaxThresholds thresholds) {
		this(thresholds, 0);
	}

	public SSIWorksheet() {
	}

	public SSIWorksheet(TaxThresholds thresholds, int socialSecurityIncome) {
		this.thresholds = thresholds;
		this.socialSecurityIncome = socialSecurityIncome;
	}

	/**
	 * Covers line 20b of the 1040 form.  Requires that you have properly set the Income, AGI and
	 * MAGI brackets.
	 *
	 * @param agi
	 * @param income
	 * @param scheduleD
	 * @return the amount of taxable social security income.
	 * @throws RuntimeException when MAGIBrackets have not been set
	 */
	protected int calculateTaxableAmount(AGI agi, Income income, ScheduleD scheduleD) {
		if (thresholds == null) {
			throw new RuntimeException("MAGIBrackets must be set, cannot be null");
		}
		if (socialSecurityIncome == 0) {
			return 0;
		}

		// Enter one-half of line 1 (Social Security Income)
		int line2 = socialSecurityIncome / 2;

		// Combine Form 1040 lines 7, 8a, 9a, 10-14, 15b, 16b, 17-19 and 21
		int line3;
		if (income == null) {
			line3 = 0;
		} else {
			line3 = income.getWages() + income.getOrdinaryDividends() + income.getTaxableInterest
					() + income.getTaxableRefunds() + income.getAlimony() + income
					.getBusinessIncome() + income.getOtherGains() + income.getIraDistributions() +
					income.getPensionsAndAnnuities() + income.getRealEstate() + income
					.getFarmIncome() + income.getUnemployment() + income.getOtherIncome();
		}
		if (scheduleD != null) {
			line3 += scheduleD.getCapitalGains();
		}

		// Enter the amount, if any, from Form 1040, line 8b (Tax Exempt Interest)
		// Line 4

		// Combine lines 2, 3, and 4
		int line5 = line2 + line3 + income.getTaxExemptInterest();

		// Enter the total of the amounts from Form 1040, lines 23 through 32, plus any
		// write-in adjustments you entered on the dotted line next to line 36
		int line6;
		if (agi == null) {
			line6 = 0;
		} else {
			line6 = agi.getEducatorExpenses() + agi.getBusinessExpenses() + agi
					.getHealthSavingsDeduction() + agi.getMovingExpenses() +
					agi.getSelfEmploymentDeductable() + agi.getSelfEmployHealthInsuranceDeduction
					() +
					agi.getEarlyWithdrawlPenalty() + agi.getAlimony() + agi.getIraDeduction();
		}

		// If line 6 is less than line 5, subtract line 6 from line 5, otherwise return 0
		int line7;
		if (line6 < line5) {
			line7 = line5 - line6;
		} else {
			return 0; // No taxable income
		}

		// From here, we tax based on the MAGI brackets
		return (int) Math.round((thresholds.calculateMAGITaxRate(line7) * socialSecurityIncome));
	}
}