package models;

import models.pojo.FilingStatus;
import models.pojo.TaxThresholds;

/**
 * Created by Dolan on 10/21/2015.
 */
public class 	TaxAndCredits {

	// These are values that change yearly and will be provided
	private final static int YEARLY_EXEMPTIONS_MULTIPLIER = 3950;

	//    private Object scheduleA;                   // This would contain the itemized
	// deductions.
	private int alternativeMinTax = 0;
	private int premiumTaxCredit = 0;
	private int foreignTaxCredit = 0;
	private int dependentExpenses = 0;
	private int eduCredits = 0;
	private int retirementSavingsCredit = 0;
	private int childTaxCredit = 0;
	private int residentialEnergyCredits = 0;
	private int otherCredits = 0;
	private int mortgageInterestCredit = 0;
	private int propertyTaxCredit = 0;
	private int charitableContributionsCredit = 0;

	/**
	 * Tax and Credits summary
	 */
	// Line 38: Amount from line 37 (AGI)
	// Line 40: Itemized deductions OR Standard Deduction                           (Should be
	// done by TaxAndCredits)
	// Line 41: Subtract line 40 from line 38                                       (Should be
	// done by TaxAndCredits)
	// Line 42: Exemptions.  If line 38 is $152,525 or less, multiply $3,950 by the number on line
	// 6d (dependents), otherwise see instructions. (Should be done by TaxAndCredits)
	// Line 43: Taxable Income. Subtract line 42 from line 41.  If line 42 is more than line 41,
	// enter 0 (Should be done by TaxAndCredits)
	// Line 44: Tax (see instructions)                                              (Should be
	// done by TaxAndCredits)
	// Line 45: Alternative Minimum Tax (see instructions)                          (Should be
	// done by TaxAndCredits)
	// Line 46: Excess Advance Premium Tax Credit (form 8962)                       (Should be
	// done by TaxAndCredits)
	// Line 47: Add lines 44, 45 and 46                                             (Should be
	// done by TaxAndCredits)
	// Line 55: Add lines 48 through 55. These are your Total Credits               (Should be
	// done by TaxAndCredits)
	// Line 56: Subtract line 55 from line 47. If line 55 is more than line 47, enter 0 (Should be
	// done by TaxAndCredits)
	public TaxAndCredits() {

	}

	/** Calculations */
	/**
	 * Covers line 42 on the 1040 form. Computes the exemptions earned based on other criteria
	 *
	 * @return the exemption earned
	 */
	public int calculateExemptions(int agi, int numExemptions, FilingStatus filingStatus) {

		// See Deduction for Exemptions Worksheet
		int retVal;

		// Is the amount on Form 1040, line 38, more than the amount shown on line 4 below for
		// your filing status?
		// TODO: These values are going to need to be stored somewhere
		if (filingStatus.getName().equals(FilingStatus.MARRIED_JOINTLY) && agi <= 305050 ||
				filingStatus.getName().equals(FilingStatus.SINGLE) && agi <= 254200) {
			// Multiply $3,950 by the total number of exemptions claimed on Form 1040, line 6d,
			// and enter the result on line 42.
			return YEARLY_EXEMPTIONS_MULTIPLIER * numExemptions;
		} else {
			int line2 = YEARLY_EXEMPTIONS_MULTIPLIER * numExemptions;
			int line3 = agi;
			int line4 = 0;
			int line5 = 0;
			int line6 = 0;
			if (filingStatus.getName().equals(FilingStatus.MARRIED_JOINTLY)) {
				line4 = 305050;
				line5 = line3 - line4;
				if (line5 > 122500) {
					return 0;
				}
				line6 = (int) Math.ceil(line5 / 2500);
			} else if (filingStatus.getName().equals(FilingStatus.SINGLE)) {
				line4 = 152525;
				line5 = line3 - line4;
				if (line5 > 61250) {
					return 0;
				}
				line6 = (int) Math.ceil(line5 / 1250);
			}

			double line7 = line6 * 0.02;
			double line8 = line2 * line7;
			return (int) (line2 - line8);

		}

	}

	public int getCredits() {
		// Sum of lines 48 through 54 of 1040
		return this.foreignTaxCredit +
				this.dependentExpenses +
				this.eduCredits +
				this.retirementSavingsCredit +
				this.childTaxCredit +
				this.residentialEnergyCredits +
				this.otherCredits;
	}

	/**
	 * Covers line 47 on the 1040 form
	 *
	 * @return the total taxes due before credits
	 */
	private int calculateTaxes(TaxThresholds taxThresholds, int agi, int numExemptions,
							   FilingStatus filingStatus) {
		// Add lines 44, 45, and 46
		return calculateTax(taxThresholds, agi, numExemptions, filingStatus) +
				alternativeMinTax +
				premiumTaxCredit;
	}

	/**
	 * Covers line 55 on the 1040 form
	 *
	 * @return the total credits earned
	 */
	public int calculateTotalCredits() {
		// are expenses. Attach Form 2441 49 50 Education credits from Form 8863, line 19 .....50
		// 51 Retirement savings contributions credit. Attach Form 888051 52 Child tax credit.
		// Attach Schedule 8812, if required...52 53 Residential energy credits. Attach Form
		// 5695....5354 Other credits from Form: a 3800 b 8801 c
		return this.foreignTaxCredit +
				this.dependentExpenses +
				this.eduCredits +
				this.retirementSavingsCredit +
				this.childTaxCredit +
				this.residentialEnergyCredits +
				this.otherCredits;
	}

	/**
	 * Covers line 56 on the 1040 form.  Calculate the total tax owed after all calculations are
	 * performed for the Tax And Credits section of the 1040 form.
	 *
	 * @param agi
	 * @param taxThresholds
	 * @param numExemptions
	 * @param filingStatus
	 * @return the total tax owed from this section
	 */
	public int calculateTaxLessCredits(int agi, TaxThresholds taxThresholds, int
			numExemptions,
									   FilingStatus filingStatus) {
		// Subtract line 55 from line 47
		int tax = calculateTaxes(taxThresholds, agi, numExemptions, filingStatus);
		int credits = this.calculateTotalCredits();

		// If line 55 is more than line 47 enter 0
		return Math.max(0, tax - credits);
	}

	/**
	 * Covers line 40 on the 1040 form, the determination of standard vs itemized is made here
	 *
	 * @param standardDeduction
	 * @return the deductions earned
	 */
	public int calculateDeductions(int standardDeduction) {
		// Determine which is greater and return either itemized or the standard deduction
		return Math.max(getItemizedDeductions(), standardDeduction);
	}

	/**
	 * Technically this is Schedule A stuff but for now we can put it here
	 *
	 * @return the value of itemized deductions
	 */
	private int getItemizedDeductions() {
		return this.mortgageInterestCredit +
				this.propertyTaxCredit +
				this.charitableContributionsCredit;
	}

	/**
	 * Covers line 43 on the 1040 Form
	 *
	 * @return the total taxable models.income
	 */
	public int calculateTaxableIncome(int agi, int numExemptions, FilingStatus filingStatus,
										 int standardDeduction) {
		// AGI minus Deductions
		// Subtract line 40 from line 38
		int line41 = agi - calculateDeductions(standardDeduction);

		// Subtract line 42 from line 41. If line 42 is more than line 41, enter -0-
		return Math.max(line41 - calculateExemptions(agi, numExemptions, filingStatus), 0);
	}

	/**
	 * Covers line 44 on the 1040 form
	 *
	 * @param taxThresholds
	 * @return the amount of tax owed based on the yearly tax brackets provided
	 */
	protected int calculateTax(TaxThresholds taxThresholds, int agi, int numExemptions,
							   FilingStatus filingStatus) {
		return taxThresholds.calculateIncomeTax(
				calculateTaxableIncome(agi, numExemptions, filingStatus,
						taxThresholds.standardDeduction));
	}

	/**
	 * Setters
	 */
	protected void setAlternativeMinTax(int alternativeMinTax) {
		this.alternativeMinTax = alternativeMinTax;
	}

	protected void setPremiumTaxCredit(int premiumTaxCredit) {
		this.premiumTaxCredit = premiumTaxCredit;
	}

	protected void setForeignTaxCredit(int foreignTaxCredit) {
		this.foreignTaxCredit = foreignTaxCredit;
	}

	protected void setDependentExpenses(int dependentExpenses) {
		this.dependentExpenses = dependentExpenses;
	}

	protected void setEduCredits(int eduCredits) {
		this.eduCredits = eduCredits;
	}

	protected void setRetirementSavingsCredit(int retirementSavingsCredit) {
		this.retirementSavingsCredit = retirementSavingsCredit;
	}

	protected void setChildTaxCredit(int childTaxCredit) {
		this.childTaxCredit = childTaxCredit;
	}

	protected void setResidentialEnergyCredits(int residentialEnergyCredits) {
		this.residentialEnergyCredits = residentialEnergyCredits;
	}

	protected void setOtherCredits(int otherCredits) {
		this.otherCredits = otherCredits;
	}

	protected void setPropertyTaxCredit(int propertyTaxCredit) {
		this.propertyTaxCredit = propertyTaxCredit;
	}

	protected void setCharitableContributionsCredit(int charitableContributionsCredit) {
		this.charitableContributionsCredit = charitableContributionsCredit;
	}

	protected void setMortgageInterestCredit(int mortgageInterestCredit) {
		this.mortgageInterestCredit = mortgageInterestCredit;
	}

	public int getMortgageInterestCredit() {
		return mortgageInterestCredit;
	}

	public int getEduCredits() {
		return eduCredits;
	}

	public int getPropertyTaxCredit() {
		return propertyTaxCredit;
	}

	public int getCharitableContributionsCredit() {
		return charitableContributionsCredit;
	}

	public int getAlternativeMinTax() {
		return alternativeMinTax;
	}

	public int getChildTaxCredit() {
		return childTaxCredit;
	}

	public int getDependentExpenses() {
		return dependentExpenses;
	}

	public int getForeignTaxCredit() {
		return foreignTaxCredit;
	}

	public int getOtherCredits() {
		return otherCredits;
	}

	public int getPremiumTaxCredit() {
		return premiumTaxCredit;
	}

	public int getResidentialEnergyCredits() {
		return residentialEnergyCredits;
	}

	public int getRetirementSavingsCredit() {
		return retirementSavingsCredit;
	}
}
