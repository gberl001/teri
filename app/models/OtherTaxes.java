package models;

/**
 * OtherTaxes.java
 * <p>
 * Represents the Other Taxes section of the 1040 (lines 57-63). Includes lingering tax fields like
 * self-employment tax, household employment tax, and taxes from Form8959.
 * </p>
 * Created by piperchester on 10/20/2015.
 */
public class OtherTaxes {
    /**
     * Self-employment tax. Attach Schedule Self-Employment.
     */
    private int selfEmploymentTax;

    /**
     * Additional tax on IRAs, other qualified retirement plans, etc.
     * (Form 5329 if required).
     */
    private int additionalRetirementTax;

    /**
     * First-time homebuyer credit repayment. (Form 5405 if required).
     */
    private int firstTimeHomebuyerCreditRepayment;

    /**
     * Health care: individual responsibility.
     */
    private int healthCare;


    // Calculated from other forms

    /**
     * Unreported social secuirty and Medicare tax from Form 4137 or Form 8919.
     */
    private int unreportedSSMedicareTax;

    /**
     * Taxes from Form 8959 or Form 8960.
     */
    private int taxes;

    /**
     * Household employment taxes from Schedule H.
     */
    private int householdEmploymentTax;

    protected OtherTaxes() {
		this(0,0,0,0);
	}

    /**
     * Local values for OtherTaxes segment.
     * @param selfEmploymentTax
     * @param additionalRetirementTax
     * @param firstTimeHomebuyerCreditRepayment
     * @param healthCare
     */
    protected OtherTaxes(int selfEmploymentTax, int additionalRetirementTax, int firstTimeHomebuyerCreditRepayment,
                      int healthCare) {
        this.selfEmploymentTax = selfEmploymentTax;
        this.additionalRetirementTax = additionalRetirementTax;
        this.firstTimeHomebuyerCreditRepayment = firstTimeHomebuyerCreditRepayment;
        this.healthCare = healthCare;
    }
	
	protected void setSelfEmploymentTax(int set) {
		this.selfEmploymentTax = set;
	}

    protected void setAdditionalRetirementTax(int art) {
		this.additionalRetirementTax = art;
	}

    protected void setFirstTimeHomebuyerCreditRepayment(int fthcr) {
		this.firstTimeHomebuyerCreditRepayment = fthcr;
	}

    protected void setHealthCare(int hc) {
		this.healthCare = hc;
	}
    /**
     * Calculates the Other Taxes section amount and returns a int. Should sum
     * seven distinct fields.
     *
     * @return the Other Taxes section amount.
     */
    public int getOtherTaxes() {
        return this.selfEmploymentTax +
                this.additionalRetirementTax +
                this.firstTimeHomebuyerCreditRepayment +
                this.healthCare +
                this.unreportedSSMedicareTax +
                this.taxes +
                this.householdEmploymentTax;
    }

    public int getAdditionalRetirementTax() {
        return additionalRetirementTax;
    }

    public int getFirstTimeHomebuyerCreditRepayment() {
        return firstTimeHomebuyerCreditRepayment;
    }

    public int getHealthCare() {
        return healthCare;
    }

    public int getHouseholdEmploymentTax() {
        return householdEmploymentTax;
    }

    public int getSelfEmploymentTax() {
        return selfEmploymentTax;
    }

    public int calculateTotalTax() {
        return taxes;
    }

    public int getUnreportedSSMedicareTax() {
        return unreportedSSMedicareTax;
    }
}
