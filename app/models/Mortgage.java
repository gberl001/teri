package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Assumptions:
 * Twelve payments per year
 * Interest is compounded monthly
 * <p>
 * Created by piper on 3/1/16.
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
public class Mortgage extends Model {

    @Transient
    public static final int MONTHS_IN_YEAR = 12;
    @Transient
    private static NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
    @Transient
    private static DateFormat df = new SimpleDateFormat("MMM yyyy", Locale.US);

    @Id
    public long id;

	public String name;
    public int principalBalance;    // This is the remaining principal balance
    public int term;				// This is the number of years total
    public double interestRate;		// This is the ANNUAL interest rate

    @Formats.DateTime(pattern = "M/d/yyyy")
    public Date beginDate;			// This is the date at which this mortgage payment started.

    @ManyToOne
    public Retiree retiree;


    public Mortgage() { }

    public Mortgage(int principalBalance, int term, double interestRate, Date beginDate) {
        this.principalBalance = principalBalance;
        this.term = term;
        this.interestRate = interestRate;
        this.beginDate = beginDate;
    }


    public String getAmortizationSummary() {
        StringBuilder sb = new StringBuilder();
        int totalPayPeriods = term * MONTHS_IN_YEAR;

        Date printDate = new Date(beginDate.getTime());
        double currentPrincipal = principalBalance;
        double intPerPeriod = ratePerPeriod(interestRate, MONTHS_IN_YEAR, MONTHS_IN_YEAR);
        double monthlyPayment = getMonthlyPayment();
        sb.append("Payment: ").append(monthlyPayment).append("\n");
        for (int i = 0; i < totalPayPeriods; i++) {
            double interest = currentPrincipal * intPerPeriod;
            double principal = monthlyPayment - interest;

            sb.append("Date: ").append(df.format(printDate)).append(", Principal: ").append(
                    nf.format(principal)).append(", Interest: ").append(nf.format(interest))
                    .append(
                            ", Remaining Principle: ").append(
                    nf.format(currentPrincipal - principal)).append("\n");

            // Update for the next go round
            printDate.setMonth(printDate.getMonth() + 1);
            currentPrincipal -= principal;
        }


        return sb.toString();
    }

    /**
     * Calculate the monthly payment for this mortgage
     *
     * @return the amount of (principal + interest) owed each month
     */
    public double getMonthlyPayment() {
        int totalPayPeriods = term * MONTHS_IN_YEAR;
        double intPerPeriod = ratePerPeriod(interestRate, MONTHS_IN_YEAR, MONTHS_IN_YEAR);
        return valueOfLoanPayment(principalBalance, intPerPeriod, totalPayPeriods);
    }

    /**
     * Calculate the interest rate per period for an annuity
     *
     * @param annualRate      the annual interest rate
     * @param compoundPerYear the number of compounded periods per year
     * @param paymentsPerYear the number of payments made each year
     * @return the rate per period for an annuity
     */
    private double ratePerPeriod(double annualRate, int compoundPerYear, int paymentsPerYear) {
        return Math.pow(1 + annualRate / compoundPerYear, compoundPerYear / paymentsPerYear) - 1;
    }

    /**
     * Calculate how much a loan payment will be per period in order to pay off a loan for the
     * specified number of periods.
     *
     * @param principal     the total value of the loan
     * @param ratePerPeriod the rate of interest per period
     * @param numPeriods    the number of periods of the loan
     * @return the payment per period.
     */
    private double valueOfLoanPayment(double principal, double ratePerPeriod, int numPeriods) {
        //		double ratePerPeriod = interestRate / numPeriods;
        //		double denominator = 1 - Math.pow(1 + ratePerPeriod, -numPeriods);

        double numerator = ratePerPeriod * Math.pow(1 + ratePerPeriod, numPeriods);
        double denominator = Math.pow(1 + ratePerPeriod, numPeriods) - 1;

        return principal * numerator / denominator;
    }


	/**
	 * Generic query helper for entity with id Long
	 */
	public static Finder<Long, Mortgage> find = new Finder<>(Mortgage.class);

	public static Map<String, String> options() {
		LinkedHashMap<String, String> options = new LinkedHashMap<>();
		for (Mortgage a : Mortgage.find.orderBy("name desc").findList()) {
			options.put(String.valueOf(a.id), a.name);
		}
		return options;
	}

}
