package models;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.validation.Constraint;
import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import com.sun.javafx.beans.IDProperty;

/**
 * Created by Sean Dolan on 3/1/2016.
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
public class AccountInvestments extends Model implements Serializable {

    @Id
    public long id;

    // Name for the account, Not really neccesary for the specific accounts
    public String name;

    // Id of the User retiree that the account is associated with
    @ManyToOne(fetch = FetchType.EAGER)
    public Retiree retiree;

    // Pre or post tax
    public boolean preTax = false;

    @Constraints.Min(0)
    public double balance;

    @Constraints.Min(0)
    public double costBasis;

    // Percentage growth of the account every year
    public double growthRate;

    public String type;

    @Transient
    public int amountRemoved = 0;
    /**
     * Creates a new roth Account pointed at the given retiree
     * @param newRetiree
     */
    public AccountInvestments(Retiree newRetiree){this.retiree = newRetiree; this.preTax = false;}
    public AccountInvestments(Retiree retiree, String name, int startingBalance, double growthRate ){
        this.retiree = retiree;
        this.name = name;
        this.balance = startingBalance;
        this.growthRate = growthRate;
        this.preTax = false;
    }
    public AccountInvestments(MonetaryAccount a) {
        this.id = a.id;
        this.retiree = a.retiree;
        this.name = a.name;
        this.balance = a.balance;
        this.growthRate = a.growthRate;
        this.preTax = false;
    }

    public static com.avaje.ebean.Model.Finder<Long, AccountInvestments> find = new com.avaje.ebean.Model.Finder<>(AccountInvestments.class);

    public static Map<String, String> options() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for (AccountInvestments a : AccountInvestments.find.orderBy("name").findList()) {
            options.put(String.valueOf(a.id), a.name);
        }
        return options;
    }
    // @Override
    // public int compareTo(MonetaryAccount a) {
    //     return (int)((this.growthRate - a.growthRate) * 1000);
    // }

    @Override
    public String toString() {
        return "AccountInvestments{" +
                "id=" + id +
                ", name='" + name + '\'' +
                //", retiree=" + (retiree == null ? "null" : retiree.toString()) +
                ", preTax=" + preTax +
                ", balance=" + balance +
                ", growthRate=" + growthRate +
                ", type=" + type +
                '}';
    }
    public void addBalance(int amount) {
        balance += amount;
        costBasis += amount;
    }

    /**
     * returns whether or not there is enough funds available in the account for withdrawl
     *
     * @param amount
     * @return True if blance > amount
     */
    public boolean isFundsAvailable(int amount) {
        return balance - amount >= 0;
    }

    /**
     * remove the amount from the balance, it does not check if the balance is available
     *
     * @param amount
     */
    public int removeBalance(int amount) {
        double percent = costBasis /balance;
        balance -= (double)amount;
        amountRemoved += (double)amount;
        costBasis -= amount * percent;
        return (int)((double)amount * percent);
    }

    /**
     * returns the RMD eligibility for the account
     * False means RMD exmept
     * @return the eligibility of the account
     */
    public boolean requiredRMDAccount(){
        return false;
    }

    public void setBalance(int amount) {
        balance = amount;
    }

    /**
     * updates the balance to the next year
     */
    public void addOneYearGrowth() {
        balance += balance * growthRate;
    }

    /**
     * undos one year of growth
     */
    public void removeOneYearGrowth() {
        balance += balance / growthRate;
    }

    /**
     * Take the amount and add half before a years growth, and then the other half after
     * Used for better estimating account growth
     *
     * @param amount
     */
    public void addAmountAndGrowth(int amount) {
        addBalance((int)Math.floor(amount / 2));
        addOneYearGrowth();
        addBalance((int)Math.ceil((amount / 2)));
    }

    /**
     * Getter for monetary account name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Retiree getRetiree() {
        return retiree;
    }

    public void setRetiree(Retiree retiree) {
        this.retiree = retiree;
    }

    public boolean isPreTax() {
        return preTax;
    }

    public void setPreTax(boolean preTax) {
        this.preTax = preTax;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getGrowthRate() {
        return growthRate;
    }

    public void setGrowthRate(double growthRate) {
        this.growthRate = growthRate;
    }

    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
        stream.writeObject(amountRemoved);
    }


    private void readObject(ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        amountRemoved = (int) stream.readObject();

    }
}
