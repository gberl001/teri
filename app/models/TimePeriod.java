package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.lang.String;
import com.sun.javafx.beans.IDProperty;

/**
 * Created by Sean Dolan on 3/1/2016.
 */
@Entity
public class TimePeriod {
    @Id
    public long id;

    private int StartYear;
    private int EndYear;
    private int Amount;

    @ManyToOne
    public MonetaryAccount account;

    public TimePeriod(int start, int end, int amount){
        this.Amount = amount;
        this.EndYear = end;
        this.StartYear = start;
    }

    public static com.avaje.ebean.Model.Finder<Long, TimePeriod> find = new com.avaje.ebean.Model.Finder<>(TimePeriod.class);

    public int getStartYear(){return this.StartYear;}
    public int getEndYear(){return this.EndYear;}
    public int getAmount(){return this.Amount;}
}

