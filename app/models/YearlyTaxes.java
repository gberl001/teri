package models;

import java.io.Serializable;

public class YearlyTaxes implements Serializable {
	public RetirementPlan retirementPlan;
	public int year;
	public Taxes taxes;
	public int totalIncome;
	public double nonTaxDeferred;
	public double taxDeferred;

	public void setYear(int y) {
		this.year = y;
		this.taxes.setAges(this.retirementPlan.getNameYearString(y));
	}

	public YearlyTaxes(RetirementPlan retirementPlan, Taxes taxes, int totalIncome, double ntd, double td) {
		this.retirementPlan = retirementPlan;
		this.taxes = taxes;
		this.totalIncome = totalIncome;
		this.nonTaxDeferred = ntd;
		this.taxDeferred = td;
	}

	public YearlyTaxes() {}

	public Taxes getTaxes() {
		return this.taxes;
	}
	public int getNetIncome() {
		return this.totalIncome - this.taxes.getTotalDue();
	}
}