package models;

import models.pojo.TaxThresholds;
import models.pojo.TaxBracket;

import java.util.Collections;
import java.util.List;

/**
 * Created by Sean Dolan on 4/11/2016.
 */
public class ReverseTaxCalc {
    public ReverseTaxCalc(){

    }
    public int getPretaxIncome(int income, TaxThresholds taxBracket) {
        int desired = income;

        int previousTax = 0;
        int tax = Integer.MAX_VALUE;
        while(desired != (income - tax )) {
            tax = taxBracket.calculateIncomeTax(income);
            income += tax - previousTax;
            previousTax = tax;
        }
            
        return income + tax;
    }

    /**
     * the usage assumes that the optimization engine says i want 80000 a year, this thing then tells you the post tax amount needed,
     * and then you take into account RMDs and that shit
     * @param income desired income amount
     * @param taxBracket tax bracket structure
     * @return
     */
    public int getPretaxIncomeExact(int income, TaxThresholds taxBracket){
        int totalDesiredIncome = income;
        int addedCash = 0;
        int shortfall = taxBracket.calculateIncomeTax(totalDesiredIncome);
        List<TaxBracket> incomeBracket = taxBracket.getIncomeBrackets();
        Collections.sort(incomeBracket);
        //Income neded to survive the year
        int i=0;
        while(shortfall >0){
            TaxBracket a = incomeBracket.get(i);
            if(a.getHigh() <= totalDesiredIncome){
                i++;
            }
            else{
                // if there is enough money in the current bracket
                if( (a.getHigh() - (totalDesiredIncome + addedCash))>= (shortfall / (1-a.getRate()) )) {
                    return  (int)Math.ceil((totalDesiredIncome + (shortfall /(1-a.getRate())))) + addedCash;
                }
                //if there is not
                else{
                    shortfall -= (a.getHigh()- (totalDesiredIncome+addedCash)) *(1-a.getRate());
                    addedCash += (a.getHigh() - (totalDesiredIncome+addedCash));
                    i++;
                }
            }
        }
        return income +addedCash;
    }
}
