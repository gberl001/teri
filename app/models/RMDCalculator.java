package models;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dolan on 2/14/2016.
 */
public class RMDCalculator {
        private static int startAge = 70;
        private static int maxAge = 115;
        //TODO: Something better than the hardcoded array
        private static Double[] RMDTable = {27.4,26.5,25.6,24.7,23.8,22.9,22.0,
                                     21.2,20.3,19.5,18.7,17.9,17.1,16.3,
                                     15.5,14.8,14.1,13.4,12.7,12.0,11.4,
                                     10.8,10.2,9.6,9.1,8.6,8.1,7.6,7.1,
                                     6.7,6.3,5.9,5.5,5.2,4.9,4.5,4.2,
                                     3.9,3.7,3.4,3.1,2.9,2.6,2.4,2.1,
                                     1.9};

        /**
         * Will return the amount that must be withdrawed from IRA at the current age
         * @param age age of retiree
         * @param money amount of money they have
         * @return the amount that must be withdrawn this year
         */
        public static double calculateRMD(int age, double money){
            if (age < startAge){
                return 0;
            }
            else if(age >= maxAge){
                return (money / RMDTable[RMDTable.length -1]);

            }
            else{
                return (money / RMDTable[age-startAge]);
            }

        }
        public static int calculateRMD(Retiree retiree){
            double amountOfRMDCash = 0;
            for(MonetaryAccount x : retiree.getConvertedAccounts()){
                if(x.getClass().equals(Account401k.class)){
                    amountOfRMDCash += x.getBalance();
                }
                else if(x.getClass().equals(AccountIRA.class)){
                    amountOfRMDCash += x.getBalance();
                }
            }
            return (int)calculateRMD(retiree.getCurrentAge(), amountOfRMDCash);
        }
        public void updateRMD(){
            try {
                ArrayList<Double> newRMDTable = new ArrayList<>();
                List<String> lines = Files.readAllLines(Paths.get("RMD.txt"), StandardCharsets.UTF_8);
                for (String line: lines){
                    String[] lineSplit =line.split(",");
                    newRMDTable.add(Double.parseDouble(lineSplit[1]));
                }
                Double[] newRMDArray = new Double[newRMDTable.size()];
                newRMDTable.toArray(newRMDArray);
                RMDTable = newRMDArray;
                startAge = Integer.parseInt(lines.get(0).split(",")[0]);
                maxAge = Integer.parseInt(lines.get(lines.size()-1).split(",")[0]);

            }
            catch (IOException e){
                System.out.println("Using Default Table");
                Double[] newRMDTable = {27.4,26.5,25.6,24.7,23.8,22.9,22.0,
                        21.2,20.3,19.5,18.7,17.9,17.1,16.3,
                        15.5,14.8,14.1,13.4,12.7,12.0,11.4,
                        10.8,10.2,9.6,9.1,8.6,8.1,7.6,7.1,
                        6.7,6.3,5.9,5.5,5.2,4.9,4.5,4.2,
                        3.9,3.7,3.4,3.1,2.9,2.6,2.4,2.1,
                        1.9};
                RMDTable = newRMDTable;
                startAge = 70;
                maxAge = 115;
                return;
            }
        }
}
