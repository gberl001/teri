package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.*;
import java.io.*;
import models.pojo.*;
import static com.avaje.ebean.Expr.eq;


public class Yoy {
	public int inflationRate = 25; //PUT THIS IN DATABASE
	//Earned income
	//pension income
	//social security
	//distributions from qualified plans
	//capital gains
	//dividends
	//roth conversion
	//qualified plan contirbutions
	//hsa contributions
	//mortage interest
	//student loan interest
	//property tax
	//charitable donations


	public RetirementPlan retirementPlan = new RetirementPlan();

	public Yoy() {
	}


	public ArrayList<YearlyTaxes> runOptimizeYoyTaxes(int years, int conventionalTaxes, int conventionalYears, RetirementPlan rp) {
		RetirementPlan originalPlan = superUglyHack(rp);

		
		ArrayList<ArrayList<YearlyTaxes>> potentialOptimized = new ArrayList<ArrayList<YearlyTaxes>>();
		
		//First pass
		// while(potentialOptimized.size() < 1) {
		while(potentialOptimized.size() < 2000) {

			this.retirementPlan = superUglyHack(originalPlan);
			ArrayList<YearlyTaxes> yoyTaxes = runYoy(years, true, null);
			if(yoyTaxes.size() >= conventionalYears ) {
					 System.out.println("ADDING FIRST PASS " + potentialOptimized.size());
					potentialOptimized.add(yoyTaxes);
			}
		}


		//Get biggest
		int size = conventionalYears;
		ArrayList<ArrayList<YearlyTaxes>> longestTime = new ArrayList<ArrayList<YearlyTaxes>>();
		for(int i = 0; i < potentialOptimized.size(); i++) {
			if(potentialOptimized.get(i).size() > size) {
				size = potentialOptimized.get(i).size();
				longestTime.add(potentialOptimized.get(i));
			}
		}

		int index = 0;
		int min = Integer.MAX_VALUE;

		if(longestTime.size() > 0) {
			potentialOptimized = longestTime;
			index = longestTime.size() - 1;
		} else {
	 		for(int i = 0; i < potentialOptimized.size(); i++) {
				int total = 0;
				for (YearlyTaxes y : potentialOptimized.get(i)) {
					total += y.taxes.getTotalDue();
				}
				if(total < min) {
					min = total;
					index = i;
				}

			}
		}


		ArrayList<ArrayList<YearlyTaxes>> furtherInvestigations = new ArrayList<ArrayList<YearlyTaxes>>();
		// while(furtherInvestigations.size() < 1) {
		while(furtherInvestigations.size() < 1000) {

				this.retirementPlan = superUglyHack(originalPlan);
				ArrayList<YearlyTaxes> yoyTaxes = runYoy(years, true, potentialOptimized.get(index));

				if(yoyTaxes.size() >= conventionalYears) {
					System.out.println("ADDING SECOND PASS " + furtherInvestigations.size());
					furtherInvestigations.add(yoyTaxes);
				}
		}
		

		

		index = 0;
		min = Integer.MAX_VALUE;
	
		for(int i = 0; i < furtherInvestigations.size(); i++) {
			int total = 0;
			for (YearlyTaxes y : furtherInvestigations.get(i)) {
				total += y.taxes.getTotalDue();
			}
			if(total < min) {
				min = total;
				index = i;
			}
		}
		return furtherInvestigations.get(index);
	}

	public ArrayList<YearlyTaxes> runConventionalYoyTaxes(int years, RetirementPlan rp) {
		this.retirementPlan = rp;
		ArrayList<YearlyTaxes> yoyTaxes = runYoy(years, false, null);

		return yoyTaxes;
	}

	public double getSecondPass(double prev, int year) {
		Random rand = new Random();
		
		// if (year < 8) {
		// 	prev += .2;
		// } 
		if(prev == 0) {
			return (double)rand.nextInt(21)/100;
		} else if (prev == 0.30) {
			return (double)(rand.nextInt((40-15)+1) + 15)/100;
		} else if (prev == 0.5) {
			return (double)(rand.nextInt((65-40)+1) + 40)/100;
		} else if (prev == 0.80) {
			return (double)(rand.nextInt((90-65)+1) + 65)/100;
		} else {
			return (double)(rand.nextInt((100-85)+1) + 85)/100;
		}
	}

	public ArrayList<YearlyTaxes> runYoy(int years, boolean optimize, ArrayList<YearlyTaxes> comp) {
		RetirementPlan currentRetirementPlan = retirementPlan;

		int yearsUntilRetirement = currentRetirementPlan.addInterestUntilRetirement();
		TaxThresholds taxThresholds = TaxThresholds.find.where().and(eq("year", 2014), 
			eq("filingStatus.id", currentRetirementPlan.filingStatus.id)).findUnique();


		for(int i = 0; i < yearsUntilRetirement; i++) {
			taxThresholds.inflateOneYear((double)inflationRate / 100.0);
		}
		
		currentRetirementPlan.setRetireeAges();

		ArrayList<YearlyTaxes> yoyTaxes = new ArrayList<YearlyTaxes>();
		double nonTaxDeferred = -1;
		double taxDeferred = -1;
		double invest = -1;
		int failures = 0;

		for(int y = 0; y <= years; y++) { 
			RetirementPlan backup = superUglyHack(currentRetirementPlan);
			if(optimize) {
				Random rand = new Random();
				
				double takeout = (double)( rand.nextInt(5) / 100.0);
				
				if(comp != null) {
					if(comp.size() > y) {
						invest = takeout;
						nonTaxDeferred = getSecondPass(comp.get(y).nonTaxDeferred, y);
						taxDeferred = (Math.floor((1 - nonTaxDeferred - takeout) * 100) / 100);
					}
				} else {
					invest = takeout;
					nonTaxDeferred = getRandomPercentage(y);
					taxDeferred = 1 - nonTaxDeferred - takeout;	
					
				}
			}
			//Add Account Interest
			for(MonetaryAccount ma : currentRetirementPlan.getAccounts()) {
				ma.addOneYearGrowth();
			}
			for(AccountInvestments a : currentRetirementPlan.getInvestmentAccounts()) {
				a.addOneYearGrowth();
			}

			//Optimize
			OptimizeEngine engine = new OptimizeEngine(currentRetirementPlan, taxThresholds);
			YearlyTaxes year = engine.optimize(nonTaxDeferred, taxDeferred, invest);
			if(year.retirementPlan != null) {
				failures = 0;
				year.setYear(y);

				//Add that son of a gun
				yoyTaxes.add(uglyHack(year));		

				/*
				Inflate SS benefits, if applicable. Needs to be done here because there's no check
				to make sure it's not inflating the first year ss benefits, which would be NO GOOD!
				Designing up front is good.
				*/
				currentRetirementPlan.inflateSS((double)((inflationRate / 1000.0) + 1));

				//Update inflation, ages, and tax bracket
				currentRetirementPlan.desiredIncome = calcuateInflation(currentRetirementPlan.desiredIncome, 1);
				currentRetirementPlan.nextYear();
				taxThresholds.inflateOneYear((double)inflationRate / 1000.0);


				//Update accounts
				for(MonetaryAccount ma : currentRetirementPlan.retiree1.getMonetaryAccounts()) {
					for(MonetaryAccount mac : year.retirementPlan.retiree1.getMonetaryAccounts()) {
						if(ma.id == mac.id) {
							ma.balance = mac.balance;
							ma.amountRemoved = 0;
							break;
						}
					}
				}
				for(AccountInvestments ac : currentRetirementPlan.retiree1.getInvestmentAccounts()) {
					for(AccountInvestments mac : year.retirementPlan.retiree1.getInvestmentAccounts()) {
						if(ac.id == mac.id) {
							ac.balance = mac.balance;
							ac.amountRemoved = 0;
							break;
						}
					}
				}

				if(currentRetirementPlan.retiree2 != null) {
					for(MonetaryAccount ma : currentRetirementPlan.retiree2.getMonetaryAccounts()) {
						for(MonetaryAccount mac : year.retirementPlan.retiree2.getMonetaryAccounts()) {
							if(ma.id == mac.id) {
								ma.balance = mac.balance;
								ma.amountRemoved = 0;
								break;
							}
						}
					}
					for(AccountInvestments ac : currentRetirementPlan.retiree2.getInvestmentAccounts()) {
						for(AccountInvestments mac : year.retirementPlan.retiree2.getInvestmentAccounts()) {
							if(ac.id == mac.id) {
								ac.balance = mac.balance;
								ac.amountRemoved = 0;
								break;
							}
						}
					}
				}
			} else {
				y -= 1;
				failures += 1;
				currentRetirementPlan = backup;
				if (failures > 10) {
					break;
				}
			}
			
		}
		return yoyTaxes;
	}

	private double getRandomPercentage(int year) {
		double random = Math.random();
		
		//Weigh the first few years higher.
		// if(year < 8) {
		// 	random += .7;
		// }

		if (random < .3) {
			return 0;
		} else if (random >= .3 && random < .5) {
			return .30;
		} else if (random >= .5 && random < .7) {
			return .5;
		} else if (random >= .7 && random < .85) {
			return .80;
		}  else {
			return 1;
		}
		
	}

	private int calcuateInflation(int amount, int year) {
		//Remember A = P (1 + r/n) ^ nt from accounting 101, yep.
		return (int)(amount * Math.pow((1 + ((double)this.inflationRate / (double)1000)), (double)year));
	}

	public RetirementPlan superUglyHack(RetirementPlan rp) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(rp);
			byte[] bytes = bos.toByteArray();
			ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			ObjectInput in = null;
			try {
			  in = new ObjectInputStream(bis);
			  RetirementPlan o = (RetirementPlan)in.readObject(); 
			  return o;
			} catch(Exception e) {} finally {
			  try {
			    bis.close();
			  } catch (IOException ex) {
			    // ignore close exception
			  }
			  try {
			    if (in != null) {
			      in.close();
			    }
			  } catch (IOException ex) {
			    // ignore close exception
			  }
			}
		} catch(Exception e) {}
		finally {
		  try {
		    if (out != null) {
		      out.close();
		    }
		  } catch (IOException ex) {
		    // ignore close exception
		  }
		  try {
		    bos.close();
		  } catch (IOException ex) {
		    // ignore close exception
		  }
		}
		return new RetirementPlan();
	} 
	

	private YearlyTaxes uglyHack(YearlyTaxes year) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(year);
			byte[] bytes = bos.toByteArray();
			ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			ObjectInput in = null;
			try {
			  in = new ObjectInputStream(bis);
			  YearlyTaxes o = (YearlyTaxes)in.readObject(); 
			  return o;
			} catch(Exception e) {} finally {
			  try {
			    bis.close();
			  } catch (IOException ex) {
			    // ignore close exception
			  }
			  try {
			    if (in != null) {
			      in.close();
			    }
			  } catch (IOException ex) {
			    // ignore close exception
			  }
			}
		} catch(Exception e) {}
		finally {
		  try {
		    if (out != null) {
		      out.close();
		    }
		  } catch (IOException ex) {
		    // ignore close exception
		  }
		  try {
		    bos.close();
		  } catch (IOException ex) {
		    // ignore close exception
		  }
		}
		return new YearlyTaxes();
	} 
}