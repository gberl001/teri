package models;

import java.util.LinkedHashMap;
import java.util.Map;
import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import com.sun.javafx.beans.IDProperty;

/**
 * Created by Sean Dolan on 3/1/2016.
 */

public class AccountRoth extends MonetaryAccount {
    /**
     * Creates a new roth Account pointed at the given retiree
     * @param newRetiree
     */
    public AccountRoth(Retiree newRetiree){this.retiree = newRetiree; this.preTax = false;}
    public AccountRoth(Retiree retiree, String name, int startingBalance, double growthRate ){
        this.retiree = retiree;
        this.name = name;
        this.balance = startingBalance;
        this.growthRate = growthRate;
        this.preTax = false;
    }

   public AccountRoth(MonetaryAccount a) {
        this.id = a.id;
        this.retiree = a.retiree;
        this.name = a.name;
        this.balance = a.balance;
        this.growthRate = a.growthRate;
        this.preTax = false;
    }

    public static com.avaje.ebean.Model.Finder<Long, AccountRoth> find = new com.avaje.ebean.Model.Finder<>(AccountRoth.class);

    public static Map<String, String> options() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for (AccountRoth a : AccountRoth.find.orderBy("name").findList()) {
            options.put(String.valueOf(a.id), a.name);
        }
        return options;
    }

}
