package models.pojo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * CapitalGainsBracket.java
 *
 * A class to represent a Capital Gains related tax bracket
 *
 * Created by BerlGeof on 4/11/2016.
 */
@Entity
@DiscriminatorValue("cap_gains")
public class CapitalGainsBracket extends ThresholdBracket {

}
