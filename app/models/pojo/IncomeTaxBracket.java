package models.pojo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * IncomeTaxBracket.java
 *
 * A class to represent an Income related tax bracket
 *
 * Created by BerlGeof on 4/11/2016.
 */
@Entity
@DiscriminatorValue("income")
public class IncomeTaxBracket extends TaxBracket {

}
