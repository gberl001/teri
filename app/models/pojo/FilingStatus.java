package models.pojo;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by geoffberl on 11/29/15.
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
public class FilingStatus extends Model {

	@Id
	public long id;
	@Transient
	public static final String MARRIED_JOINTLY = "Married Filing Jointly";
	@Transient
	public static final String SINGLE = "Single";
	@Constraints.Required(message="Name is a required field")
	public String name;

	public FilingStatus(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Generic query helper for entity with id Long
	 */
	public static Finder<Long, FilingStatus> find = new Finder<>(FilingStatus.class);

	/**
	 * Helper method to return all possible options, intended for use in drop down field.
	 * @return a map of Filing Status options from the database
	 */
	public static Map<String, String> options() {
		LinkedHashMap<String, String> options = new LinkedHashMap<>();

		for (FilingStatus fs : FilingStatus.find.orderBy("id").findList()) {
			options.put(String.valueOf(fs.id), fs.name);
		}

		return options;
	}

	@Override
	public String toString() {
		return name;
	}
}
