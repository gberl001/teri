package models.pojo;

import models.Retiree;
import models.RetirementPlan;
import models.TenFortyCalculator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * FakeDB.java
 * <p>
 * A simple class to simulate a DB
 * <p>
 * Created by BerlGeof on 11/30/2015.
 */
public class FakeDB {

	private final List<TenFortyCalculator> db = new ArrayList<>();

	public FakeDB() {
		FilingStatus married = new FilingStatus(2, FilingStatus.MARRIED_JOINTLY);
		FilingStatus single = new FilingStatus(1, FilingStatus.SINGLE);

		createCase(new Retiree("Married", "2014", new Date(), new Date()), null, "Married 2014",
				married, 2, 0, 0, 17500, 92500, 75000, 22000, 3000);
		createCase(new Retiree("Working", "Couple", new Date(), new Date()), null, "Working " +
				"Couple",
				married, 2, 140000, 0, -3000, 15000, 0, 350, 0);
		createCase(new Retiree("Working", "Single", new Date(), new Date()), null, "Working " +
				"Single",
				single, 3, 101000, 0, 8000, 15000, 0, 13000, 2500);
		createCase(new Retiree("Elderly", "Couple", new Date(), new Date()), null, "Elderly " +
				"Couple",
				married, 2, 27500, 4500, -2500, 88500, 60000, 14800, 0);
		createCase(new Retiree("Retired", "Couple", new Date(), new Date()), null, "Retired " +
				"Couple",
				married, 2, 15000, 15000, 5000, 70000, 33000, 15000, 0);
		createCase(new Retiree("Retired", "Guy", new Date(), new Date()), null, "Retired Guy",
				single, 1, 0, 2500, -3000, 60000, 40000, 13500, 0);

		createCase(new Retiree("Scrooge", "McDuck", new Date(), new Date()), null, "Scrooge",
				single, 1, 130000, 20000, 102000, 300000, 12000, 16760, 0);
		createCase(new Retiree("Han", "Solo", new Date(), new Date()), null, "Han Solo", single, 2,
				0, 0, 17500, 92500, 75000, 22000, 3000);
		createCase(new Retiree("Luke", "Skywalker", new Date(), new Date()), null, "Luke " +
				"Skywalker",
				married, 2, 1500, 1000, 25000, 5000, 18000, 6200, 3000);
		createCase(new Retiree("Willy", "Loman", new Date(), new Date()), null, "Mr. Loman",
				single,
				2, 0, 1500, -3000, 3200, 24000, 12400, 3000);
	}

	private void createCase(Retiree retiree1, Retiree retiree2, String name, FilingStatus status,
							int exemptions, int wages, int dividends, int capitalGains, int
									pensions, int ssIncome, int itemizedDeductions, int
									hsaDeduction) {
		retiree1.id = db.size() + 1;
		if (retiree2 != null) {
			retiree2.id = db.size() + 1;
		}
		TenFortyCalculator form = new TenFortyCalculator(
				new RetirementPlan(retiree1, retiree2, name, status, exemptions));
		form.addWages(wages);
		form.addOrdinaryDividends(dividends);
		form.addLongTermCapitalGains(capitalGains);
		form.addPensionsAndAnnuities(pensions);
		form.setSocialSecurityIncome(ssIncome);
		form.setTotalItemizedDeductionAmount(itemizedDeductions);
		form.setHealthSavingsDeduction(hsaDeduction);
		form.setId(db.size() + 1);

		// Add the new form to the listFilingStatus
		db.add(form);
	}

	/**
	 * List all forms in the collection
	 *
	 * @return a collection of forms
	 */
	public Collection<TenFortyCalculator> list() {
		return db;
	}

	/**
	 * Find a form by id
	 *
	 * @param id the id of the form
	 * @return the form if it exists
	 */
	public TenFortyCalculator find(long id) {
		return db.get((int) (id - 1));
	}

}
