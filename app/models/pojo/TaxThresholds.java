package models.pojo;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.Where;

import javax.persistence.*;
import java.util.*;

/**
 * TaxThresholds.java
 * <p>
 * A class to represent all tax related brackets, limits, thresholds, etc for a given year.
 * <p>
 * Created by BerlGeof on 4/11/2016.
 */
@Entity
public class TaxThresholds extends Model {

	@Id
	public long id;
	public int year;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "tax_threshold_id")
	@Where(clause = "type='comp_table'")
	private List<TaxBracket> taxComputationBrackets = new ArrayList<>();
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "tax_threshold_id")
	@Where(clause = "type='cap_gains'")
	private List<ThresholdBracket> capitalGainsBrackets = new ArrayList<>();
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "tax_threshold_id")
	@Where(clause = "type='income'")
	private List<TaxBracket> incomeBrackets = new ArrayList<>();
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "tax_threshold_id")
	@Where(clause = "type='magi'")
	private List<ThresholdBracket> magiBrackets = new ArrayList<>();
	/* Multiplied by two if married */
	public int standardDeduction = 6200;					// NOTE: Historically increases $100/yr
	@Column(name = "qualified_div_threshold")
	public int qualifiedDividendThreshold = 100000;
	/* Multiplied by two if married */
	@Column(name = "capital_loss_deduct_limit")
	public int capitalLossesDeductibleLimit = 1500;			// NOTE: This hasn't changed in decades
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public FilingStatus filingStatus;

	public TaxThresholds() {
		/* Required for EBeans */
		// FetchType.EAGER not working, slight fix
//		forceEagerFetch();
	}

	public TaxThresholds(long id, int year, List<TaxBracket> taxComputationBrackets,
						 List<ThresholdBracket> capitalGainsBrackets, List<TaxBracket>
								 incomeBrackets,
						 List<ThresholdBracket> magiBrackets, int standardDeduction,
						 int qualifiedDividendThreshold, int capitalLossesDeductibleLimit,
						 FilingStatus filingStatus) {
		this.id = id;
		this.year = year;
		this.taxComputationBrackets = taxComputationBrackets;
		this.capitalGainsBrackets = capitalGainsBrackets;
		this.incomeBrackets = incomeBrackets;
		this.magiBrackets = magiBrackets;
		this.standardDeduction = standardDeduction;
		this.qualifiedDividendThreshold = qualifiedDividendThreshold;
		this.capitalLossesDeductibleLimit = capitalLossesDeductibleLimit;
		this.filingStatus = filingStatus;
		// FetchType.EAGER not working, slight fix
//		forceEagerFetch();
	}

//	private void forceEagerFetch() {
//		this.getCapitalGainsBrackets().size();
//		this.getIncomeBrackets().size();
//		this.getMagiBrackets().size();
//		this.getTaxComputationBrackets().size();
//	}

	public int calculateIncomeTax(int value) {
		double retVal = 0;
		Collections.sort(incomeBrackets);
		for (TaxBracket bracket : incomeBrackets) {
			int range = (Math.min(bracket.getHigh(), value)) - bracket.getLow();
			retVal += bracket.getRate() * range;
			if (value <= bracket.getHigh()) {
				break;
			}
		}
		return Math.toIntExact(Math.round(retVal));
	}

	/**
	 * Calculate and return a tax rate based on a threshold
	 *
	 * @param value the value to compare with the threshold
	 * @return the tax rate that should be applied
	 */
	public double calculateCapitalGainsRate(int value) {
		return calculateRateFromThresholds(capitalGainsBrackets, value);
	}

	public int calculateWithComputationTable(int value) {
		int retVal = 0;
		Collections.sort(taxComputationBrackets);
		for (TaxBracket bracket : taxComputationBrackets) {
			// Skip brackets until we find the ONE that encompasses the value
			// TODO: Be sure to check the == bracket.getLow() case in tests
			if (value < bracket.getLow() || value > bracket.getHigh()) {
				continue;
			}

			// In this case, simply multiply the value by the rate and end
			retVal = (int) (bracket.getRate() * value - bracket.getSubtractionAmount());
			break;
		}
		return Math.toIntExact(Math.round(retVal));
	}

	private double calculateRateFromThresholds(List<ThresholdBracket> brackets, int value) {
		double retVal = 0;
		Collections.sort(brackets);
		for (ThresholdBracket bracket : brackets) {
			// If the value is less than this threshold, we're done, leave with the last
			// calculated taxable amount
			if (value <= bracket.getThreshold()) {
				break;
			}
			retVal = bracket.getRate();
		}
		return retVal;
	}

	/**
	 * Calculate and return a tax rate based on a threshold
	 *
	 * @param value the value to compare with the threshold
	 * @return the tax rate that should be applied
	 */
	public double calculateMAGITaxRate(int value) {
		return calculateRateFromThresholds(magiBrackets, value);
	}

	/**
	 * Generic query helper for entity with id Long
	 */
	public static Model.Finder<Long, TaxThresholds> find = new Model.Finder<>(TaxThresholds.class);

	public FilingStatus getFilingStatus() {
		return filingStatus;
	}

	public int getYear() {
		return year;
	}

	public List<TaxBracket> getTaxComputationBrackets() {
		Collections.sort(taxComputationBrackets);
		return taxComputationBrackets;
	}

	public List<ThresholdBracket> getCapitalGainsBrackets() {
		Collections.sort(taxComputationBrackets);
		return capitalGainsBrackets;
	}

	public List<TaxBracket> getIncomeBrackets() {
		Collections.sort(taxComputationBrackets);
		return incomeBrackets;
	}

	public List<ThresholdBracket> getMagiBrackets() {
		Collections.sort(taxComputationBrackets);
		return magiBrackets;
	}

	public int getStandardDeduction() {
		return standardDeduction;
	}

	public int getQualifiedDividendThreshold() {
		return qualifiedDividendThreshold;
	}

	public int getCapitalLossesDeductibleLimit() {
		return capitalLossesDeductibleLimit;
	}

	public void inflateOneYear(double rate) {
		// Increment year
		year += 1;
		// Incrementing is doubled for married couples
		if (filingStatus.getName().equals(FilingStatus.MARRIED_JOINTLY)) {
			standardDeduction += 200;
		} else {
			standardDeduction += 100;
		}
		// Loop through each TaxBracket object and inflate the number
		inflateTaxBrackets(incomeBrackets, rate);
		inflateTaxBrackets(taxComputationBrackets, rate);
		inflateThresholdBrackets(capitalGainsBrackets, rate);
		inflateThresholdBrackets(magiBrackets, rate);

	}

	private void inflateThresholdBrackets(List<ThresholdBracket> brackets, double rate) {
		for (ThresholdBracket bracket : brackets) {
			bracket.setThreshold((int) (bracket.getThreshold() * (1 + rate)));
		}
	}

	private void inflateTaxBrackets(List<TaxBracket> brackets, double rate) {
		for (TaxBracket bracket : brackets) {
			bracket.setLow((int) (bracket.getLow() * (1 + rate)));
			bracket.setHigh((int) (bracket.getHigh() * (1 + rate)));
			bracket.setSubtractionAmount((int) (bracket.getSubtractionAmount() * (1 + rate)));
		}
	}

	@Override
	public String toString() {
		return "TaxThresholds{" +
				"id=" + id +
				", year=" + year +
				", taxComputationBrackets=" + taxComputationBrackets +
				", capitalGainsBrackets=" + capitalGainsBrackets +
				", incomeBrackets=" + incomeBrackets +
				", magiBrackets=" + magiBrackets +
				", standardDeduction=" + standardDeduction +
				", qualifiedDividendThreshold=" + qualifiedDividendThreshold +
				", capitalLossesDeductibleLimit=" + capitalLossesDeductibleLimit +
				", filingStatus=" + filingStatus +
				'}';
	}

}
