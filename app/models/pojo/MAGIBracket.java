package models.pojo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * MAGIBracket.java
 *
 * A class to represent a MAGI related tax bracket
 *
 * Created by BerlGeof on 4/11/2016.
 */
@Entity
@DiscriminatorValue("magi")
public class MAGIBracket extends ThresholdBracket {

}
