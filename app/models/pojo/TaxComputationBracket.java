package models.pojo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * TaxComputationBracket.java
 *
 * A class to represent a Tax computation table related tax bracket
 *
 * Created by BerlGeof on 4/11/2016.
 */
@Entity
@DiscriminatorValue("comp_table")
public class TaxComputationBracket extends TaxBracket {

}
