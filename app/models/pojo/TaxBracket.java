package models.pojo;

import com.avaje.ebean.Model;

import javax.persistence.*;

/**
 * TaxBracket.java
 * <p>
 * A bean class that represents a database object
 * <p>
 * Created by geoffberl on 10/10/15.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
public class TaxBracket extends Model implements Comparable<TaxBracket> {

	@Id
	public long id;
//	@ManyToOne(fetch = FetchType.EAGER)
//	public TaxThresholds taxThreshold;
	public int low;
	public int high;
	public double rate;
	public double subtractionAmount = 0;

	public TaxBracket() { /* Required by EBean */ }

	public TaxBracket(int low, int high, double rate) {
        this.high = high;
        this.low = low;
        this.rate = rate;
    }

    public TaxBracket(int low, int high, double subtractionAmount, double rate) {
        this.high = high;
        this.low = low;
        this.rate = rate;
        this.subtractionAmount = subtractionAmount;
    }

    public int getHigh() {
        return high;
    }

    public int getLow() {
        return low;
    }

    public double getRate() {
        return rate;
    }

    public double getSubtractionAmount() {
        return subtractionAmount;
    }

	public void setLow(int low) {
		this.low = low;
	}

	public void setHigh(int high) {
        if(high < 0) {
            high = Integer.MAX_VALUE;
        }
		this.high = high;
	}

	public void setSubtractionAmount(double subtractionAmount) {
		this.subtractionAmount = subtractionAmount;
	}

	@Override
	public String toString() {
		return "TaxBracket{" +
				"high=" + high +
				", low=" + low +
				", rate=" + rate +
				", subtractionAmount=" + subtractionAmount +
				'}';
	}

	@Override
    public int compareTo(TaxBracket that) {
        if (that == null) {
            return -1;
        }
        return Integer.compare(this.low, that.low);
    }
}
