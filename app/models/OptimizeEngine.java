package models;

import java.util.*;
import models.pojo.*;

public class OptimizeEngine {

	private RetirementPlan retirementPlan;
	private TaxThresholds taxThresholds;

	public OptimizeEngine(RetirementPlan rp, TaxThresholds tp) {
		this.retirementPlan = rp;
		this.taxThresholds = tp;
	}

	public YearlyTaxes optimize(double nonTaxDeferred, double taxDeferred, double invest) {
		TenFortyCalculator tfc = new TenFortyCalculator(this.retirementPlan, this.taxThresholds);

		int desiredIncome = this.retirementPlan.desiredIncome;

		int postTaxIncome = 0;
		int totalIncome = 0;

		int retiree1RMD = RMDCalculator.calculateRMD(this.retirementPlan.retiree1);
		int retiree2RMD = 0;

		if(this.retirementPlan.retiree2 != null) {
			retiree2RMD = RMDCalculator.calculateRMD(this.retirementPlan.retiree2);
 
		} 

		/**
		* RMD, SS, AND PENSION MUST BE TAKEN OUT. No optimizing here :(
		*/
		//Take out RMD		
		if(retiree1RMD > 0) {
			postTaxIncome += takeOutRMD(this.retirementPlan.retiree1, retiree1RMD);
		}
		if(retiree2RMD > 0) {
			postTaxIncome += takeOutRMD(this.retirementPlan.retiree2, retiree2RMD);
		}	
		
		//Pension
		int pensionIncome = 0;
		pensionIncome += this.retirementPlan.retiree1.pension;
		if(this.retirementPlan.retiree2 != null) {
			pensionIncome += this.retirementPlan.retiree2.pension;
		}
		
		postTaxIncome += pensionIncome;

		desiredIncome -= postTaxIncome;
		totalIncome += postTaxIncome;
		
		//Social Security
		int ssincome = 0;

		if(desiredIncome > 0) {

			if(this.retirementPlan.retiree1.canGetSSBenefits()) {
				this.retirementPlan.retiree1.ssBenefits = this.retirementPlan.retiree1.getMaxSocialSecurityBenefits();
				ssincome += this.retirementPlan.retiree1.ssBenefits;
			}
			if(this.retirementPlan.retiree2 != null) {
				if(this.retirementPlan.retiree2.canGetSSBenefits()) {
					this.retirementPlan.retiree2.ssBenefits = this.retirementPlan.retiree2.getMaxSocialSecurityBenefits();
					ssincome += this.retirementPlan.retiree2.ssBenefits;
				}
			}
			desiredIncome -= ssincome;
			totalIncome += ssincome;
			tfc.setSocialSecurityIncome(ssincome);
		}

/**
*		BEGIN OPTIMIZING
*/

		int nonTaxDeferredPreferred = desiredIncome;
		int taxDeferredPreferred = desiredIncome;
		int investmentsPreferred = desiredIncome;
		if(nonTaxDeferred != -1) {
			nonTaxDeferredPreferred *= nonTaxDeferred;
			taxDeferredPreferred *= taxDeferred;
			investmentsPreferred *= invest;

			//Because of float BS, just add the two together and call that desired, it's off by one crap.
			desiredIncome = nonTaxDeferredPreferred + taxDeferredPreferred + investmentsPreferred;
		}

		//Misc accounts - Tax Deferred
		if(desiredIncome > 0) {
			
			List<MonetaryAccount> retireeMisc = this.retirementPlan.retiree1.getMiscAccounts();
			if(this.retirementPlan.retiree2 != null) {
				retireeMisc.addAll(this.retirementPlan.retiree2.getMiscAccounts());
			}
			// retireeMisc.addAll(this.retirementPlan.retiree1.getCashAccounts());
			// if(this.retirementPlan.retiree2 != null) {
			// 	retireeMisc.addAll(this.retirementPlan.retiree2.getCashAccounts());
			// }
			
			for(MonetaryAccount a : retireeMisc) {
				if(a.isFundsAvailable(taxDeferredPreferred)) {
					a.removeBalance(taxDeferredPreferred);
					totalIncome += taxDeferredPreferred;
					desiredIncome -= taxDeferredPreferred;
					taxDeferredPreferred -= taxDeferredPreferred;
				} else {
					int takeOut = (int)a.getBalance();
					a.removeBalance(takeOut);
					desiredIncome -= takeOut;
					totalIncome += takeOut;
					taxDeferredPreferred -= takeOut;
				}
			}
		}

		if(nonTaxDeferred == -1) {
			nonTaxDeferredPreferred = desiredIncome;
			investmentsPreferred = desiredIncome;
		}

		ReverseTaxCalc rtc = new ReverseTaxCalc();
		nonTaxDeferredPreferred = rtc.getPretaxIncomeExact(nonTaxDeferredPreferred + postTaxIncome + tfc.calculateTaxableSSI(), this.taxThresholds);
		nonTaxDeferredPreferred -= (postTaxIncome + tfc.calculateTaxableSSI()); 

		//Non-Tax deferred 
		if(desiredIncome > 0) {
			List<MonetaryAccount> retireeMisc = this.retirementPlan.retiree1.getRequiredRMDAccounts();
			if(this.retirementPlan.retiree2 != null) {
				//Combine accounts
				retireeMisc.addAll(this.retirementPlan.retiree2.getRequiredRMDAccounts());
			}

			for(MonetaryAccount a : retireeMisc) {
				if(a.isFundsAvailable(nonTaxDeferredPreferred)  && desiredIncome > 0) {
					a.removeBalance(nonTaxDeferredPreferred);
					totalIncome += nonTaxDeferredPreferred;
					postTaxIncome += nonTaxDeferredPreferred;

					desiredIncome -= nonTaxDeferredPreferred;
					nonTaxDeferredPreferred -= nonTaxDeferredPreferred;
				} else {
					int takeOut = (int)a.getBalance();
					a.removeBalance(takeOut);
					desiredIncome -= takeOut;
					totalIncome += takeOut;
					postTaxIncome += takeOut;
					nonTaxDeferredPreferred -= takeOut;
				}
			}
		}

		if(nonTaxDeferred == -1) {
			investmentsPreferred = desiredIncome;
		}
		
		//Investments
		int investmentIncome = 0;
		if(desiredIncome > 0) {
			List<AccountInvestments> retireeInvest = this.retirementPlan.getInvestmentAccounts();
			for(AccountInvestments i : retireeInvest) {
				if(i.isFundsAvailable(investmentsPreferred)  && desiredIncome > 0 ) {
					i.removeBalance(investmentsPreferred);
					investmentIncome += investmentsPreferred;
					desiredIncome -= investmentsPreferred;
				} else {
					int takeOut = (int)i.getBalance();
					i.removeBalance(takeOut);
					investmentIncome += takeOut;
					desiredIncome -= takeOut;
				}
			}

		}
		tfc.addLongTermCapitalGains(investmentIncome);

		//Cash
		if(desiredIncome > 0) {
			List<MonetaryAccount> retireeMisc = this.retirementPlan.retiree1.getCashAccounts();
			if(this.retirementPlan.retiree2 != null) {
				retireeMisc.addAll(this.retirementPlan.retiree2.getCashAccounts());
			}
			
			for(MonetaryAccount a : retireeMisc) {
				if(a.isFundsAvailable(desiredIncome) && desiredIncome > 0) {
					a.removeBalance(desiredIncome);
					totalIncome += desiredIncome;
					desiredIncome -= desiredIncome;
				} else {
					int takeOut = (int)a.getBalance();
					a.removeBalance(takeOut);
					desiredIncome -= takeOut;
					totalIncome += takeOut;
				}
			}
		}

		//Check if we failed, if we did, we suck.
		if(desiredIncome > 0) {
			return new YearlyTaxes(null, null, 0, 0, 0);
		}

		tfc.addWages(postTaxIncome);
		Taxes taxes = new Taxes(tfc.calculateAGI(), tfc.getTotalTaxableIncome(), tfc.calculateTax());

		//The converted accounts are marked transient. We don't want that
		this.retirementPlan.retiree1.finalizeAccounts();
		if(this.retirementPlan.retiree2 != null) {
			this.retirementPlan.retiree2.finalizeAccounts();
		}
		
		return new YearlyTaxes(this.retirementPlan, taxes, totalIncome, nonTaxDeferred, taxDeferred);
		
	}

	/**
	*	Takes out RMD, returns value taken out
	*/
	private int takeOutRMD(Retiree retiree, int rmd) {
		int amountOut = 0;
		List<MonetaryAccount> rmdAccounts = retiree.getRequiredRMDAccounts();
		int idealEach = (int)Math.ceil(rmd / rmdAccounts.size());
		for(MonetaryAccount a : rmdAccounts) {
			if(amountOut <= rmd) {
				
				if(a.isFundsAvailable(idealEach)) {
					retiree.removeMoneyFromAccount(a, idealEach);
					amountOut += idealEach;
				} else {
					int out = (int)a.balance;
					retiree.removeMoneyFromAccount(a, out);
					amountOut += out;
					idealEach = (int)Math.ceil(((rmd - amountOut) / rmdAccounts.size()));
				}

			} else { break; }
		}
		return amountOut;
	}

	/**
	*	Create a sorted set of monetary accounts based on interest
	*/
	private SortedSet createSortedSet(List<MonetaryAccount> accounts) {
		SortedSet accountSet = new TreeSet();
		for(MonetaryAccount a : accounts) {
			accountSet.add(a);
		}
		return accountSet;
	}
}
