package models;

import models.pojo.FilingStatus;
import models.pojo.TaxThresholds;

import static com.avaje.ebean.Expr.eq;

/**
 * TenFortyCalculator.java
 * <p>
 * <p>
 * Created by geoffberl on 10/21/15.
 */
public class TenFortyCalculator {

	private long id;
	private RetirementPlan retirementPlan;
	private TaxThresholds taxThresholds = new TaxThresholds();
	private Income income = new Income();
	private ScheduleD scheduleD;
	private SSIWorksheet ssiWS;
	private AGI agi = new AGI();
	private TaxAndCredits taxAndCredits = new TaxAndCredits();
	private OtherTaxes otherTaxes = new OtherTaxes();
	private Payments payments = new Payments();

	/**
	 * Single argument constructor, will likely include year in the future so that we know which
	 * tables to use.
	 *
	 * @param retirementPlan	The retirement plan that contains all necessary information for
	 *                          calculating taxes for the year.
	 */
	public TenFortyCalculator(RetirementPlan retirementPlan) {
		this.retirementPlan = retirementPlan;

		if (retirementPlan != null) {
			updateBrackets(2014, retirementPlan.filingStatus);
		}
	}

	public TenFortyCalculator(RetirementPlan retirementPlan, TaxThresholds taxThresholds) {
		this.retirementPlan = retirementPlan;
		setTaxThresholds(taxThresholds);
	}

	private void updateBrackets(int year, FilingStatus filingStatus) {
		if (filingStatus == null) {
			return;
		}

		setTaxThresholds(TaxThresholds.find.where().and(eq("year", year),
				eq("filingStatus.id", filingStatus.id)).findUnique());
	}

	private void setTaxThresholds(TaxThresholds taxThresholds) {
		this.taxThresholds = taxThresholds;
		this.scheduleD = new ScheduleD(0, 0, taxThresholds.capitalLossesDeductibleLimit);
		ssiWS = new SSIWorksheet(taxThresholds);
	}

	/**
	 * Represents line 73 from the 1040 form.  Giving the total amount of taxes that have been
	 * overpaid and should be refunded.
	 *
	 * @return the refund amount due
	 */
	public int calculateTaxRefund() {
		// Line 63: Add Lines 56 through 63. This is your Total Tax
		int tax;
		if (scheduleD.getCapitalGains() != 0) {
			tax = scheduleD.calculateTaxOnIncomeWithGains(taxThresholds,
					taxAndCredits.calculateTaxableIncome(calculateAGI(), retirementPlan.exemptions,
							retirementPlan.filingStatus, taxThresholds.standardDeduction),
					income.getQualifiedDividends());
		} else {
			tax = taxAndCredits.calculateTax(taxThresholds, calculateAGI(),
					retirementPlan.exemptions, retirementPlan.filingStatus);
		}

		int totalTaxes = tax - taxAndCredits.calculateTotalCredits() + otherTaxes.getOtherTaxes();
		return payments.calculateTotalPayments() - totalTaxes;
	}

	/**
	 * Represents line 76 from the 1040 form.  Giving the total amount of tax owed to the federal
	 * government after all calculations have been made
	 *
	 * @return the total tax owed to the government
	 */
	public int calculateTaxOwed() {
		return calculateTaxRefund() * -1;
	}

	/**
	 * Calculates tax on taxable income, this is the equivalent of line 44 on the Form 1040 for
	 * 2014
	 *
	 * @return the Tax owed on taxable income
	 */
	public int calculateTax() {
		// If there are capital gains we compute the tax using Schedule D
		if (scheduleD.getCapitalGains() != 0) {
			return scheduleD.calculateTaxOnIncomeWithGains(taxThresholds,
					taxAndCredits.calculateTaxableIncome(calculateAGI(), retirementPlan.exemptions,
							retirementPlan.filingStatus, taxThresholds.standardDeduction),
					income.getQualifiedDividends());
		} else { // Otherwise, we compute tax using TaxAndCredits
			return taxAndCredits.calculateTax(taxThresholds, calculateAGI(),
					retirementPlan.exemptions, retirementPlan.filingStatus);
		}
	}


	/************************************************/
	/***************** Form Related *****************/
	/************************************************/

	/***********
	 * GETTERS
	 **********/
	public FilingStatus getFilingStatus() {
		return retirementPlan.filingStatus;
	}

	public int getStandardDeduction() {
		return taxThresholds.standardDeduction;
	}

	public int getExemptions() {
		return retirementPlan.exemptions;
	}

	public ScheduleD getScheduleD() {
		return scheduleD;
	}

	public SSIWorksheet getSocialSecurityWorksheet() {
		return ssiWS;
	}

	/***********
	 * SETTERS
	 **********/
	public void setExemptions(int exemptions) {
		retirementPlan.exemptions = exemptions;
	}


	/************************************************/
	/**************** Income Related ****************/
	/************************************************/

	/***********
	 * GETTERS
	 **********/
	public RetirementPlan retirementPlan() { return retirementPlan; }

	public Income income() {
		return income;
	}

	/**
	 * Warning: Income cannot be calculated properly without having the adjustments from the AGI
	 * section filled in.  Please ensure adjustments are entered before this calculation.
	 *
	 * @return the total income (line 22)
	 */
	public int calculateTotalIncome() {
		// be sure that we set the Schedule D form first
		return income.calculateIncome(ssiWS, scheduleD, agi);
	}

	public String getIncomeSummary() {
		StringBuilder sb = new StringBuilder();

		sb.append("Wages: ").append(income.getWages()).append("\n");
		sb.append("Taxable Interest: ").append(income.getTaxableInterest()).append("\n");
		sb.append("Qualified Dividends: ").append(income.getQualifiedDividends()).append("\n");
		sb.append("Ordinary Dividends: ").append(income.getOrdinaryDividends()).append("\n");
		sb.append("Taxable Refunds: ").append(income.getTaxableRefunds()).append("\n");
		sb.append("Alimony Received: ").append(income.getAlimony()).append("\n");
		sb.append("Business Income: ").append(income.getBusinessIncome()).append("\n");
		sb.append("Capital Gains: ").append(scheduleD.calculateCapitalGains()).append("\n");
		sb.append("Other Gains: ").append(income.getOtherGains()).append("\n");
		sb.append("IRA Distributions: ").append(income.getIraDistributions()).append("\n");
		sb.append("ROTH Conversions: ").append(income.getRothConversion()).append("\n");
		sb.append("Pensions: ").append(income.getPensionsAndAnnuities()).append("\n");
		sb.append("Rental Real Estate: ").append(income.getRealEstate()).append("\n");
		sb.append("Farm Income: ").append(income.getFarmIncome()).append("\n");
		sb.append("Unemployment: ").append(income.getUnemployment()).append("\n");
		sb.append("Social Security: ").append(ssiWS.getSocialSecurityIncome()).append(" (").append(
				ssiWS.calculateTaxableAmount(agi, income, scheduleD)).append(")").append("\n");
		sb.append("Other Income: ").append(income.getOtherIncome()).append("\n");
		sb.append("Total Income: ").append(income.calculateIncome(ssiWS, scheduleD, agi)).append(
				"\n");

		return sb.toString();
	}

	/***********
	 * SETTERS
	 **********/
	public void addAlimonyIncome(int alimony) {
		income.setAlimony(alimony);
	}

	public void addBusinessIncome(int businessIncome) {
		income.setBusinessIncome(businessIncome);
	}

	public void addFarmIncome(int farmIncome) {
		income.setFarmIncome(farmIncome);
	}

	public void addIraDistributions(int iraDistributions) {
		income.setIraDistributions(iraDistributions);
	}

	public void addOtherGains(int otherGains) {
		income.setOtherGains(otherGains);
	}

	public void addOtherIncome(int otherIncome) {
		income.setOtherIncome(otherIncome);
	}

	public void addPensionsAndAnnuities(int pensionsAndAnnuities) {
		income.setPensionsAndAnnuities(pensionsAndAnnuities);
	}

	public void addRealEstateIncome(int realEstateIncome) {
		income.setRealEstate(realEstateIncome);
	}

	public void setSocialSecurityIncome(int socialSecurityIncome) {
		ssiWS.setSocialSecurityIncome(socialSecurityIncome);
	}

	public void addTaxableRefunds(int taxableRefunds) {
		income.setTaxableRefunds(taxableRefunds);
	}

	public void addUnemploymentIncome(int unemploymentIncome) {
		income.setUnemployment(unemploymentIncome);
	}

	public void addWages(int wages) {
		income.setWages(wages);
	}

	public void addRothConversion(int rothConversion) {
		income.setRothConversion(rothConversion);
	}

	public void addInterestIncome(int interestIncome) {
		income.setTaxableInterest(interestIncome);
	}

	public void addShortTermCapitalGains(int shortTermGains) {
		scheduleD.setNetShortTerm(shortTermGains);
	}

	public void addLongTermCapitalGains(int longTermGains) {
		scheduleD.setNetLongTerm(longTermGains);
	}

	public void addQualifiedDividends(int qualifiedDividends) {
		income.setQualifiedDividends(qualifiedDividends);
	}

	public void addOrdinaryDividends(int ordinaryDividends) {
		income.setOrdinaryDividends(ordinaryDividends);
	}

	public void setTaxableRefunds(int taxableRefunds) {
		income.setTaxableRefunds(taxableRefunds);
	}


	/*
		Taxable income
	*/

	public int getTotalTaxableIncome() {
		return taxAndCredits.calculateTaxableIncome(calculateAGI(), retirementPlan.exemptions,
				retirementPlan.filingStatus, taxThresholds.standardDeduction);
	}
	/************************************************/
	/************** AGI Related Setters *************/
	/************************************************/

	/***********
	 * GETTERS
	 **********/
	public AGI agi() {
		return agi;
	}

	/**
	 * Warning: This calculation assumes that the income section has been completely filled out
	 * and that the adjustments have been added with regards to the AGI section (lines 23-35)
	 *
	 * @return the adjusted gross income (line 37)
	 */
	public int calculateAGI() {
		// Before we calculate AGI we must ensure that Income is set
		// Income is also dependent on the adjustments made.
		agi.setTotalIncome(calculateTotalIncome());
		return agi.calculateAGI();
	}

	public String getAGISummary() {
		StringBuilder sb = new StringBuilder();

		sb.append("Educator Expenses: ").append(agi.getEducatorExpenses()).append("\n");
		sb.append("Certain Business Expenses: ").append(agi.getForm2106Value()).append("\n");
		sb.append("Health Savings Contributions: ").append(agi.getHealthSavingsDeduction()).append(
				"\n");
		sb.append("Moving Expenses: ").append(agi.getMovingExpenses()).append("\n");
		sb.append("Deductible Self Employment Tax: ").append(
				agi.getSelfEmploymentDeductable()).append("\n");
		sb.append("Self Employment Plan Contributions: ").append(agi.getSepSIMPPlans()).append(
				"\n");
		sb.append("Self Employment Health Insurance: ").append(
				agi.getSelfEmployHealthInsuranceDeduction()).append("\n");
		sb.append("Early Withdrawal Penalty: ").append(agi.getEarlyWithdrawlPenalty()).append
				("\n");
		sb.append("Alimony Paid: ").append(agi.getAlimony()).append("\n");
		sb.append("IRA Deductions: ").append(agi.getIraDeduction()).append("\n");
		sb.append("Student Loan Interest Paid: ").append(agi.getStudentLoanInterest()).append
				("\n");
		sb.append("Tuition And Fees: ").append(agi.getTuitionAndFees()).append("\n");
		sb.append("Domestic Production Activities: ").append(
				agi.getDomesticProductionDeduction()).append("\n");
		sb.append("Total Adjustments: ").append(agi.calculateTotalAdjustment()).append("\n");
		sb.append("Total Adjusted Gross Income: ").append(agi.calculateAGI()).append("\n");

		return sb.toString();
	}

	/***********
	 * SETTERS
	 **********/
	public void setAlimony(int alimony) {
		agi.setAlimony(alimony);
	}

	public void setDomesticProductionDeduction(int domesticProductionDeduction) {
		agi.setDomesticProductionDeduction(domesticProductionDeduction);
	}

	public void setEarlyWithdrawlPenalty(int earlyWithdrawlPenalty) {
		agi.setEarlyWithdrawlPenalty(earlyWithdrawlPenalty);
	}

	public void setEducatorExpenses(int educatorExpenses) {
		agi.setEducatorExpenses(educatorExpenses);
	}

	public void setForm2106Value(int form2106Value) {
		agi.setForm2106Value(form2106Value);
	}

	public void setHealthSavingsDeduction(int healthSavingsDeduction) {
		agi.setHealthSavingsDeduction(healthSavingsDeduction);
	}

	public void setIraDeduction(int iraDeduction) {
		agi.setIraDeduction(iraDeduction);
	}

	public void setMovingExpenses(int movingExpenses) {
		agi.setMovingExpenses(movingExpenses);
	}

	public void setSelfEmployHealthInsuranceDeduction(int selfEmployHealthInsuranceDeduction) {
		agi.setSelfEmployHealthInsuranceDeduction(selfEmployHealthInsuranceDeduction);
	}

	public void setSelfEmploymentDeductable(int selfEmploymentDeductable) {
		agi.setSelfEmploymentDeductable(selfEmploymentDeductable);
	}

	public void setSepSIMPPlans(int sepSIMPPlans) {
		agi.setSepSIMPPlans(sepSIMPPlans);
	}

	public void setStudentLoanInterest(int studentLoanInterest) {
		agi.setStudentLoanInterest(studentLoanInterest);
	}

	public void setTuitionAndFees(int tuitionAndFees) {
		agi.setTuitionAndFees(tuitionAndFees);
	}

	/************************************************/
	/******* Tax and Credits Related Setters ********/
	/************************************************/

	/***********
	 * GETTERS
	 **********/
	public TaxAndCredits taxAndCredits() {
		return taxAndCredits;
	}

	public int calculateExemptions() {
		return taxAndCredits.calculateExemptions(calculateAGI(), retirementPlan.exemptions,
				retirementPlan.filingStatus);
	}

	/**
	 * Warning: This needs to have AGI completed to properly calculate.  You must have also
	 * supplied the exemptions and filing status.
	 *
	 * @return the total taxes due less credits (line 55)
	 */
	public int calculateTaxAndCredits() {
		// Determine if there is a schedule D
		if (scheduleD.getCapitalGains() == 0) {
			return taxAndCredits.calculateTaxLessCredits(calculateAGI(), taxThresholds,
					retirementPlan.exemptions, retirementPlan.filingStatus);
		} else {
			return scheduleD.calculateTaxOnIncomeWithGains(taxThresholds,
					taxAndCredits.calculateTaxableIncome(calculateAGI(), retirementPlan.exemptions,
							retirementPlan.filingStatus, taxThresholds.standardDeduction),
					income.getQualifiedDividends()) - taxAndCredits.calculateTotalCredits();
		}
	}

	public String getTaxAndCreditsSummary() {
		StringBuilder sb = new StringBuilder();

		sb.append("Calculated Deduction: ").append(
				taxAndCredits.calculateDeductions(taxThresholds.standardDeduction)).append("\n");
		sb.append("Exemptions: ").append(
				taxAndCredits.calculateExemptions(calculateAGI(), retirementPlan.exemptions,
						retirementPlan.filingStatus)).append("\n");
		sb.append("Taxable Income: ").append(
				taxAndCredits.calculateTaxableIncome(calculateAGI(), retirementPlan.exemptions,
						retirementPlan.filingStatus, taxThresholds.standardDeduction)).append("\n");
		sb.append("Tax: ").append(calculateTax()).append("\n");
		sb.append("Alternative Minimum Tax: ").append(taxAndCredits.getAlternativeMinTax()).append(
				"\n");
		sb.append("Excess Advance Premium Tax Credit: ").append(
				taxAndCredits.getPremiumTaxCredit()).append("\n");
		sb.append("Foreign Tax Credit: ").append(taxAndCredits.getForeignTaxCredit()).append("\n");
		sb.append("Child and Dependent Care Credit").append(
				taxAndCredits.getDependentExpenses()).append("\n");
		sb.append("Education Credits: ").append(taxAndCredits.getEduCredits()).append("\n");
		sb.append("Retirement Savings Contributions Credit: ").append(
				taxAndCredits.getRetirementSavingsCredit()).append("\n");
		sb.append("Child Tax Credit: ").append(taxAndCredits.getChildTaxCredit()).append("\n");
		sb.append("Residential Energy Credits: ").append(
				taxAndCredits.getResidentialEnergyCredits()).append("\n");
		sb.append("Other Credits: ").append(taxAndCredits.getOtherCredits()).append("\n");
		sb.append("Total Credits: ").append(taxAndCredits.calculateTotalCredits()).append("\n");
		sb.append("Tax After Credits: ").append(
				taxAndCredits.calculateTaxLessCredits(calculateAGI(), taxThresholds,
						retirementPlan.exemptions, retirementPlan.filingStatus)).append("\n");

		return sb.toString();
	}

	/***********
	 * SETTERS
	 **********/
	public void setAlternativeMinTax(int alternativeMinTax) {
		taxAndCredits.setAlternativeMinTax(alternativeMinTax);
	}

	public void setPremiumTaxCredit(int premiumTaxCredit) {
		taxAndCredits.setPremiumTaxCredit(premiumTaxCredit);
	}

	public void setForeignTaxCredit(int foreignTaxCredit) {
		taxAndCredits.setForeignTaxCredit(foreignTaxCredit);
	}

	public void setDependantExpenses(int dependantExpenses) {
		taxAndCredits.setDependentExpenses(dependantExpenses);
	}

	public void setEduCredits(int eduCredits) {
		taxAndCredits.setEduCredits(eduCredits);
	}

	public void setRetirementSavingsCredit(int retirementSavingsCredit) {
		taxAndCredits.setRetirementSavingsCredit(retirementSavingsCredit);
	}

	public void setChildTaxCredit(int childTaxCredit) {
		taxAndCredits.setChildTaxCredit(childTaxCredit);
	}

	public void setResidentialEnergyCredits(int residentialEnergyCredits) {
		taxAndCredits.setResidentialEnergyCredits(residentialEnergyCredits);
	}

	public void setOtherCredits(int otherCredits) {
		taxAndCredits.setOtherCredits(otherCredits);
	}

	public void setPropertyTaxCredit(int propertyTaxCredit) {
		taxAndCredits.setPropertyTaxCredit(propertyTaxCredit);
	}

	public void setCharitableContributionsCredit(int charitableContributionsCredit) {
		taxAndCredits.setCharitableContributionsCredit(charitableContributionsCredit);
	}

	public void setMortgageInterestCredit(int mortgageInterestCredit) {
		taxAndCredits.setMortgageInterestCredit(mortgageInterestCredit);
	}

	public void setTotalItemizedDeductionAmount(int itemizedDeductionAmount) {
		// TODO: Add a setTotalItemized... and leave or remove the individual methods in
		// TaxAndCredits
		taxAndCredits.setMortgageInterestCredit(itemizedDeductionAmount);
	}

	/************************************************/
	/******** Other Taxes Related Setters ***********/
	/************************************************/

	/***********
	 * GETTERS
	 **********/
	public OtherTaxes otherTaxes() {
		return otherTaxes;
	}

	public int calculateOtherTaxes() {
		return otherTaxes.getOtherTaxes();
	}

	public String getOtherTaxesSummary() {
		StringBuilder sb = new StringBuilder();

		sb.append("Self Employment Tax: ").append(otherTaxes.getSelfEmploymentTax()).append("\n");
		sb.append("Unreported SS and Medicare Tax: ").append(
				otherTaxes.getUnreportedSSMedicareTax()).append("\n");
		sb.append("Additional Tax on IRA: ").append(otherTaxes.getAdditionalRetirementTax())
				.append(
				"\n");
		sb.append("Household Employment Tax: ").append(
				otherTaxes.getHouseholdEmploymentTax()).append("\n");
		sb.append("First Time Homebuyer Repayment: ").append(
				otherTaxes.getFirstTimeHomebuyerCreditRepayment()).append("\n");
		sb.append("Individual Health Care: ").append(otherTaxes.getHealthCare()).append("\n");
		sb.append("Other Taxes: ").append(otherTaxes.getOtherTaxes()).append("\n");
		sb.append("Total 'Other Taxes': ").append(otherTaxes.calculateTotalTax()).append("\n");

		return sb.toString();
	}

	/***********
	 * SETTERS
	 **********/
	public void setSelfEmploymentTax(int set) {
		otherTaxes.setSelfEmploymentTax(set);
	}

	public void setAdditionalRetirementTax(int art) {
		otherTaxes.setAdditionalRetirementTax(art);
	}

	public void setFirstTimeHomebuyerCreditRepayment(int fthcr) {
		otherTaxes.setFirstTimeHomebuyerCreditRepayment(fthcr);
	}

	public void setHealthCare(int hc) {
		otherTaxes.setHealthCare(hc);
	}

	/************************************************/
	/********** Payments Related Setters ************/
	/************************************************/

	/***********
	 * GETTERS
	 **********/
	public Payments payments() {
		return payments;
	}

	public int calculateTotalPayments() {
		return payments.calculateTotalPayments();
	}

	public String getPaymentsSummary() {
		StringBuilder sb = new StringBuilder();

		sb.append("Federal Income Tax Withheld: ").append(payments.getFederalIncomeTax()).append(
				"\n");
		sb.append("Estimated Tax Payments from prior year: ").append(
				payments.getTaxPayments()).append("\n");
		sb.append("Earned Income Credit: ").append(payments.getEarnedIncomeCredit()).append("\n");
		sb.append("Child Tax Credit: ").append(payments.getAdditionalChildTaxCredit()).append
				("\n");
		sb.append("American Opportunity Credit: ").append(
				payments.getAmericanOpportunityCredit()).append("\n");
		sb.append("Net Premium Tax Credit: ").append(payments.getNetPremiumTaxCredit()).append(
				"\n");
		sb.append("Excess SS Tax Withheld: ").append(payments.getExcessSSTaxWithheld()).append(
				"\n");
		sb.append("Federal Fuel Tax Credit: ").append(payments.getFuelTaxCredit()).append("\n");
		sb.append("Additional Credits: ").append(payments.getAdditionalCredits()).append("\n");
		sb.append("Total Payments: ").append(payments.calculateTotalPayments()).append("\n");

		return sb.toString();
	}

	/***********
	 * SETTERS
	 **********/
	public void setIncomeTaxWithheld(int itw) {
		payments.setIncomeTaxWithheld(itw);
	}

	public void setEarnedIncomeCredit(int eic) {
		payments.setEarnedIncomeCredit(eic);
	}

	public void setAdditionalChildTaxCredit(int actc) {
		payments.setAdditionalChildTaxCredit(actc);
	}

	public void setNetPremiumTaxCredit(int nptc) {
		payments.setNetPremiumTaxCredit(nptc);
	}

	public void setExcessSSTaxWithheld(int esstw) {
		payments.setExcessSSTaxWithheld(esstw);
	}

	public void setFuelTaxCredit(int ftc) {
		payments.setFuelTaxCredit(ftc);
	}

	public RetirementPlan getRetirementPlan() {
		return retirementPlan;
	}

	public void setRetirementPlan(RetirementPlan retirementPlan) {
		this.retirementPlan = retirementPlan;
		// With a new retirementPlan we need to updateFilingStatus the brackets
		// TODO: Will need to update this to use the retirement date... with inflation.  Or do it somewhere else
//		updateBrackets(retirementPlan.getEarliestRetirementDate().getYear());
		if (retirementPlan != null) {
			updateBrackets(2014, retirementPlan.filingStatus);
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int calculateTaxableSSI() {
		return ssiWS.calculateTaxableAmount(agi, income, scheduleD);
	}
}
