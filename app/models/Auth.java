package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Created by geoffberl on 2/18/16.
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
public class Auth extends Model {

	@Id
	public long id;
	@Constraints.Required(message="Please enter a user name")
	public String username;
	@Constraints.Required(message="Please enter a password")
	public String passwordHash;

	public static Finder<Long, Auth> find = new Finder<>(Auth.class);

	public Auth(String username) {
		this.username = username;
	}

	public Auth() {
	}

	public static Auth authenticate(String username, String password) {
		Auth user = Auth.find.where().eq("username", username).findUnique();
		if (user != null && BCrypt.checkpw(password, user.passwordHash)) {
			return user;
		} else {
			return null;
		}
	}
}
