package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Collections;
import java.text.SimpleDateFormat;
import java.io.*;

/**
 * models.Retiree.java
 *
 * Created by Piper on 10/8/2015.
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
public class Retiree extends Model implements Serializable {

	@Id
	public long id;

    @Constraints.Required(message = "Please provide a first name")
    public String firstName;
	@Constraints.Required(message = "Please provide a last name")
	public String lastName;

//	@Constraints.Required(message = "Please provide a date of birth")
	@Formats.DateTime(pattern = "M/d/yyyy")
	public Date dateOfBirth;
//	@Constraints.Required(message = "Please provide a date of retirement")
@Formats.DateTime(pattern = "M/d/yyyy")
	public Date dateOfRetirement;
//	@Constraints.Required(message = "Please provide an expected date of death")
@Formats.DateTime(pattern = "M/d/yyyy")
	public Date dateOfDeparture;

	@Formats.DateTime(pattern = "M/d/yyyy")
	public Date dateofSSBenefits;

	@Valid
	@OneToMany(mappedBy = "retiree", cascade = CascadeType.ALL)
	public List<MonetaryAccount> monetaryAccounts;

	@Valid
	@OneToMany(mappedBy = "retiree", cascade = CascadeType.ALL)
	public List<AccountInvestments> investmentAccounts = new ArrayList<AccountInvestments>();

	// @Valid
	// @OneToMany(mappedBy = "retiree", cascade = CascadeType.ALL)
	// public List<Mortgage> mortgages;

	public Integer pension = 0;
	public Integer maxSocialSecurityBenefits = 0;

	@Transient
	private int currentYear = 0;

	@Transient
	public List<MonetaryAccount> convertedAccounts = new ArrayList<MonetaryAccount>();


	@Transient
	public int ssBenefits = 0;
    /**
     * No argument constructor, this will createFilingStatus an models.user object
     */
    public Retiree() {
    }

    public void makeRealAccounts() {
    	for(MonetaryAccount a : monetaryAccounts) {
    		switch(a.type.toLowerCase()) {
    			case "cash":
    				convertedAccounts.add(a);
    				break;
    			case "401k":
    				Account401k ac4 = new Account401k(a);
    				convertedAccounts.add(ac4);
    				break;
    			case "roth":
    				AccountRoth acr = new AccountRoth(a);
    				convertedAccounts.add(acr);
    				break;
    			case "ira":
    				AccountIRA acira = new AccountIRA(a);
    				convertedAccounts.add(acira);
    				break;
    			default:
    				break;
    		}
    	}
	}

	public int addInterestUntilRetirement() {
			SimpleDateFormat df = new SimpleDateFormat("yyyy");
			int yearofRetirement = Integer.parseInt(df.format(this.dateOfRetirement));
			int currentYear = Integer.parseInt(df.format(new Date()));
			for(int i = 0; i < (yearofRetirement - currentYear); i++) {
				for(MonetaryAccount a : convertedAccounts) {
					a.addOneYearGrowth();
				}
				for(AccountInvestments a : investmentAccounts) {
					a.addOneYearGrowth();
				}
			}
			return yearofRetirement - currentYear + (currentYear - 2014); //Why - 2014, tax brackets and ugly 1 am hacks
	}

	public void finalizeAccounts() {
		this.monetaryAccounts = convertedAccounts;
	}

	public List<MonetaryAccount> getConvertedAccounts() {
		return convertedAccounts;
	}

	public List<MonetaryAccount> getRequiredRMDAccounts() {
		List<MonetaryAccount> requiredAccounts = new ArrayList<MonetaryAccount>();
		for(MonetaryAccount a : convertedAccounts) {
			if (a.requiredRMDAccount()) {
				requiredAccounts.add(a);
			}
		}
		return requiredAccounts;
	}

	public List<AccountInvestments> getInvestmentAccounts() {
		return this.investmentAccounts;
	}

	public List<MonetaryAccount> getMiscAccounts() {
		List<MonetaryAccount> miscAccounts = new ArrayList<MonetaryAccount>();
		for(MonetaryAccount a : convertedAccounts) {
			if (!a.requiredRMDAccount() && a.type == null) {
				miscAccounts.add(a);
			}
		}
		return miscAccounts;
	}

	public List<MonetaryAccount> getCashAccounts() {
		List<MonetaryAccount> miscAccounts = new ArrayList<MonetaryAccount>();
		for(MonetaryAccount a : convertedAccounts) {
			if (a.type != null) {
				miscAccounts.add(a);
			}
		}
		return miscAccounts;	
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public int getYearOfBirth() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		return Integer.parseInt(df.format(this.dateOfBirth));
	}

	public int getCurrentAge() {
		return this.currentYear - getYearOfBirth();
	}

	public void setCurrentYear(int y) {
		//Aliens
		if (y == 0) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy");
			this.currentYear = Integer.parseInt(df.format(this.dateOfRetirement));
		} else {
			this.currentYear = y;
		}
	}

	public void happyBirthday() {
		this.currentYear += 1;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfRetirement() {
		return dateOfRetirement;
	}

	public void setDateOfRetirement(Date dateOfRetirement) {
		this.dateOfRetirement = dateOfRetirement;
	}

	public Date getDateOfDeparture() {
		return dateOfDeparture;
	}

	public void setDateOfDeparture(Date dateOfDeparture) {
		this.dateOfDeparture = dateOfDeparture;
	}

	public List<MonetaryAccount> getMonetaryAccounts() {
		return monetaryAccounts;
	}

	public void setMonetaryAccounts(List<MonetaryAccount> monetaryAccounts) {
		this.monetaryAccounts = monetaryAccounts;
	}

	public Date getDateofSSBenefits() {
		return dateofSSBenefits;
	}

	public void setDateofSSBenefits(Date dateofSSBenefits) {
		this.dateofSSBenefits = dateofSSBenefits;
	}

	public void inflateSS(double rate) {
		this.maxSocialSecurityBenefits = (int)(this.maxSocialSecurityBenefits * rate);
	}

	public int getMaxSocialSecurityBenefits() {
		return maxSocialSecurityBenefits;
	}

	public void setMaxSocialSecurityBenefits(int maxSocialSecurityBenefits) {
		this.maxSocialSecurityBenefits = maxSocialSecurityBenefits;
	}

	public boolean canGetSSBenefits() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		int ssBenefitYear = Integer.parseInt(df.format(getDateofSSBenefits()));
		
		if(this.currentYear >= ssBenefitYear) {
			return true;
		}

		return false;
	}

	public void removeMoneyFromAccount(MonetaryAccount account, int amountOut) {
		account.removeBalance(amountOut);
	}
	/**
     * Constructor with common models.user types, this avoids the need to set these common
     * account
     * values separately.
     *
     * @param firstName                the earned models.income
     * @param lastName              the amount of lastName received
     * @param dateOfBirth          the amount of any other models.income not already covered
     * @param dateOfRetirement		the date the retirementPlan plans to retire
     */

    public Retiree(String firstName, String lastName, Date dateOfBirth, Date dateOfRetirement) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
		this.dateOfRetirement = dateOfRetirement;
    }

	@Override
	public String toString() {
		return "Retiree{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", dateOfBirth=" + dateOfBirth +
				", dateOfRetirement=" + dateOfRetirement +
				", dateOfDeparture=" + dateOfDeparture +
				", monetaryAccounts=" + monetaryAccounts +
				'}';
	}


	 private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
        stream.writeObject(ssBenefits);
    }

    
    private void readObject(ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        ssBenefits = (int) stream.readObject();

    }
	/**
	 * Generic query helper for entity with id Long
	 */
	public static Finder<Long, Retiree> find = new Finder<>(Retiree.class);

	public static Map<String, String> options() {
		LinkedHashMap<String, String> options = new LinkedHashMap<>();
		for (Retiree a : Retiree.find.orderBy("lastName").findList()) {
			options.put(String.valueOf(a.id), a.lastName + ", " + a.firstName);
		}
		return options;
	}


}

