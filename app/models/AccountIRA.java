package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.lang.String;
import com.sun.javafx.beans.IDProperty;

/**
 * Created by Sean Dolan on 3/1/2016.
 */
public class AccountIRA extends MonetaryAccount{
    /**
     * Creates a new IRA Account pointed at the given retiree
     * @param newRetiree
     */
    public AccountIRA(Retiree newRetiree){this.retiree = newRetiree;this.preTax = true;}
    public AccountIRA(Retiree retiree, String name, int startingBalance, double growthRate ){
        this.retiree = retiree;
        this.name = name;
        this.balance = startingBalance;
        this.growthRate = growthRate;
        this.preTax = true;
    }

   public AccountIRA(MonetaryAccount a) {
        this.id = a.id;
        this.retiree = a.retiree;
        this.name = a.name;
        this.balance = a.balance;
        this.growthRate = a.growthRate;
        this.preTax = true;
    }
    
    public static com.avaje.ebean.Model.Finder<Long, AccountIRA> find = new com.avaje.ebean.Model.Finder<>(AccountIRA.class);

    public static Map<String, String> options() {
        LinkedHashMap<String, String> options = new LinkedHashMap<>();
        for (AccountIRA a : AccountIRA.find.orderBy("name").findList()) {
            options.put(String.valueOf(a.id), a.name);
        }
        return options;
    }

    @Override
    public boolean requiredRMDAccount(){
        return true;
    }

}
