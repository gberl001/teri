package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.lang.String;
import java.io.*;

/**
 * Created by sean on 1/20/16.
 * <p>
 * First iteration of storing balances for different money accounts
 */
@Entity
@EntityConcurrencyMode(ConcurrencyMode.NONE)
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)

public class MonetaryAccount extends Model implements Comparable<MonetaryAccount>, Serializable {

	/**
	 * Generic query helper for entity with id Long
	 */
	public static com.avaje.ebean.Model.Finder<Long, MonetaryAccount> find = new com.avaje.ebean.Model.Finder<>(
			MonetaryAccount.class);

	public static Map<String, String> options() {
		LinkedHashMap<String, String> options = new LinkedHashMap<>();
		for (MonetaryAccount a : MonetaryAccount.find.orderBy("name desc").findList()) {
			options.put(String.valueOf(a.id), a.name);
		}
		return options;
	}

	@Id
	public long id;

	// Name for the account, Not really neccesary for the specific accounts
	public String name;

	// Id of the User retiree that the account is associated with
	@ManyToOne(fetch = FetchType.EAGER)
	public Retiree retiree;

	// Pre or post tax
	public boolean preTax = false;

	@Constraints.Min(0)
	public double balance = 0;

	// Percentage growth of the account every year
	public double growthRate = 0;
 	
    public String type;

    @Transient
    public int amountRemoved;

	/**
	 * Create a new Account with all the necessary fields
	 *
	 * @param retiree         : the retiree
	 * @param name            : the name of the account
	 * @param startingBalance : the amount of cash in the account already
	 * @param growthRate      : The amount the account will increase every year
	 */
	public MonetaryAccount(Retiree retiree, String name, int startingBalance, double growthRate) {
		this.retiree = retiree;
		this.name = name;
		this.balance = startingBalance;
		this.growthRate = growthRate;
	}

	/**
	 * Creates an empty account
	 *
	 * @param retiree the retiree
	 */
	public MonetaryAccount(Retiree retiree) {
		this.retiree = retiree;
	}

    public MonetaryAccount(){}

	/**
	 * Add the amount to the account
	 *
	 * @param amount
	 */
	public void addBalance(int amount) {
		balance += amount;
	}

	/**
	 * returns whether or not there is enough funds available in the account for withdrawl
	 *
	 * @param amount
	 * @return True if blance > amount
	 */
	public boolean isFundsAvailable(int amount) {
		return balance - amount >= 0;
	}

	/**
	 * remove the amount from the balance, it does not check if the balance is available
	 *
	 * @param amount
	 */
	public void removeBalance(int amount) {
		balance -= (double)amount;
		amountRemoved += (double)amount;
	}

	/**
	 * returns the RMD eligibility for the account
	 * False means RMD exmept
	 * @return the eligibility of the account
     */
	public boolean requiredRMDAccount(){
		return false;
	}

	public void setBalance(int amount) {
		balance = amount;
	}

	/**
	 * updates the balance to the next year
	 */
	public void addOneYearGrowth() {
		balance += balance * growthRate;
	}

	/**
	 * undos one year of growth
	 */
	public void removeOneYearGrowth() {
		balance += balance / growthRate;
	}

	/**
	 * Take the amount and add half before a years growth, and then the other half after
	 * Used for better estimating account growth
	 *
	 * @param amount
	 */
	public void addAmountAndGrowth(int amount) {
		balance += (Math.floor(amount / 2));
		addOneYearGrowth();
		balance += (Math.ceil((amount / 2)));
	}

	/**
	 * Getter for monetary account name
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Retiree getRetiree() {
		return retiree;
	}

	public void setRetiree(Retiree retiree) {
		this.retiree = retiree;
	}

	public boolean isPreTax() {
		return preTax;
	}

	public void setPreTax(boolean preTax) {
		this.preTax = preTax;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getGrowthRate() {
		return growthRate;
	}

	public void setGrowthRate(double growthRate) {
		this.growthRate = growthRate;
	}

	 private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
        stream.writeObject(amountRemoved);
    }

    
    private void readObject(ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        amountRemoved = (int) stream.readObject();

    }


	@Override
	public int compareTo(MonetaryAccount a) {
		return (int)((this.growthRate - a.growthRate) * 1000);
	}

	@Override
	public String toString() {
		return "MonetaryAccount{" +
				"id=" + id +
				", name='" + name + '\'' +
				//", retiree=" + (retiree == null ? "null" : retiree.toString()) +
				", preTax=" + preTax +
				", balance=" + balance +
				", growthRate=" + growthRate +
				", type=" + type + 
				'}';
	}
}
