package models;

/**
 * Payments.java
 * <p>
 * Represents the Payments section of the 1040 (lines 64-74). Includes lingering tax fields like
 * self-employment tax, household employment tax, and taxes from Form8959.
 * </p>
 * Created by piperchester on 10/21/2015.
 */
public class Payments {

    /**
     * If you have a  qualifying  child, attach  Schedule EIC
     */
    private int earnedIncomeCredit;

    /**
     * Additional child tax credit. Attach Schedule 8812
     */
    private int additionalChildTaxCredit;

    /**
     * Net premium tax credit. Attach Form 8962.
     */
    private int netPremiumTaxCredit;

    /**
     * Excess social security and tier 1 RRTA tax withheld
     */
    private int excessSSTaxWithheld;

    /**
     * Credit for federal tax on fuels. Attach Form 4136.
     */
    private int fuelTaxCredit;

    // Calculated from other Forms
    /**
     * Federal models.income tax witheld from Forms W-2 and 1099.
     */
    private int federalIncomeTax;

    /**
     * 2014 estimated tax models.payments and amount applied from 2013 return.
     */
    private int taxPayments;
    /**
     * American opportunity credit from Form 8863.
     */
    private int americanOpportunityCredit;

    /**
     * Credits from Form: 2439.
     */
    private int additionalCredits;


    protected Payments() {
		this(0,0,0,0,0);
	}

    /**
     * Constructs internal Payments fields.
     * @param earnedIncomeCredit
     * @param additionalChildTaxCredit
     * @param netPremiumTaxCredit
     * @param excessSSTaxWithheld
     * @param fuelTaxCredit
     */
    protected Payments(int earnedIncomeCredit, int additionalChildTaxCredit,
                    int netPremiumTaxCredit, int excessSSTaxWithheld, int fuelTaxCredit) {
        this.earnedIncomeCredit = earnedIncomeCredit;
        this.additionalChildTaxCredit = additionalChildTaxCredit;
        this.netPremiumTaxCredit = netPremiumTaxCredit;
        this.excessSSTaxWithheld = excessSSTaxWithheld;
        this.fuelTaxCredit = fuelTaxCredit;
    }
	
	protected void setIncomeTaxWithheld(int itw) {
		this.federalIncomeTax = itw;
	}
    protected void setEarnedIncomeCredit(int eic) {
		this.earnedIncomeCredit = eic;
	}

    protected void setAdditionalChildTaxCredit(int actc) {
		this.additionalChildTaxCredit = actc;
	}

    protected void setNetPremiumTaxCredit(int nptc) {
		this.netPremiumTaxCredit = nptc;
	}

    protected void setExcessSSTaxWithheld(int esstw) {
		this.excessSSTaxWithheld = esstw;
	}

    protected void setFuelTaxCredit(int ftc) {
		this.fuelTaxCredit = ftc;
	}

    /**
     * Calculates the Payments section amount and returns a int.
     *
     * @return the Payments section amount.
     */
    public int calculateTotalPayments() {
        return this.federalIncomeTax +
                this.taxPayments +
                this.earnedIncomeCredit +
                this.additionalChildTaxCredit +
                this.americanOpportunityCredit +
                this.netPremiumTaxCredit +
                this.excessSSTaxWithheld +
                this.fuelTaxCredit +
                this.additionalCredits;

    }

    public int getAdditionalChildTaxCredit() {
        return additionalChildTaxCredit;
    }

    public int getAdditionalCredits() {
        return additionalCredits;
    }

    public int getAmericanOpportunityCredit() {
        return americanOpportunityCredit;
    }

    public int getEarnedIncomeCredit() {
        return earnedIncomeCredit;
    }

    public int getExcessSSTaxWithheld() {
        return excessSSTaxWithheld;
    }

    public int getFederalIncomeTax() {
        return federalIncomeTax;
    }

    public int getFuelTaxCredit() {
        return fuelTaxCredit;
    }

    public int getNetPremiumTaxCredit() {
        return netPremiumTaxCredit;
    }

    public int getTaxPayments() {
        return taxPayments;
    }
}
