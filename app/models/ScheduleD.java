package models;

import models.pojo.TaxThresholds;

/**
 * ScheduleD.java
 * <p>
 * <p>
 * <p>
 * Created by BerlGeof on 11/6/2015.
 */
public class ScheduleD {

	private int netShortTerm;        // Line 7
	private int netLongTerm;        // Line 15
	private int capitalLossesDeductibleLimit;

	public ScheduleD() {
		this(0, 0, 0);
	}

	public ScheduleD(int netShortTerm, int netLongTerm, int capitalLossesDeductibleLimit) {
		this.netShortTerm = netShortTerm;
		this.netLongTerm = netLongTerm;
		this.capitalLossesDeductibleLimit = capitalLossesDeductibleLimit;
	}

	/**
	 * Calculate the results of the 28 Percent Rate Gain Worksheet
	 *
	 * @return the value of 28% Rate Gain Worksheet
	 */
	protected int calculate28PctRateGain() {
		return calculate28PercentRateGainWorksheet();
	}

	/**
	 * Calculate the results of the Unrecaptured gains worksheet
	 *
	 * @return the value of the unrecaptured gains worksheet
	 */
	protected int calculateUnrecapturedGains() {
		// Unrecaptured Gains Worksheet
		// TODO: Sale of business or property always assumed 0 (Keep or change?)
		return 0;
	}

	/**
	 * Covers line 13 on Form 1040 for 2014. A method to calculate capital gains that will count
	 * as income
	 *
	 * @return capital gains amount to be included as income
	 */
	public int calculateCapitalGains() {
		// Combine lines 7 and 15
		int line16 = calculateTotalNetGains();

		//		If line 16 is a gain, enter the amount on Form 1040, line 13, then go to line 17.
		//		If line 16 is a loss, skip lines 17 through 20 and go to line 21 and 22.
		//		If line 16 is zero, skip lines 17 - 21 enter 0 on 1040 line 13, then go to line 22.
		if (line16 > 0) {
			// Enter the amount from line 16 on Form 1040 line 13, then go to line 17
			return line16;
			// ELSE: Skip lines 18 - 21, go to line 22
		} else if (line16 < 0) {
			// Skip lines 17 through 21 and go to line 21 and 22
			// Line 21, return the smaller of line16 or capital losses limit
			return -Math.min(Math.abs(line16), capitalLossesDeductibleLimit);
		}

		// return 0 in any other case
		return 0;
	}

	public int calculateTaxOnIncomeWithGains(TaxThresholds taxThresholds, int taxableIncome,
											 int qualifiedDividends) {
		// Combine lines 7 and 15
		int line16 = calculateTotalNetGains();

		//		If line 16 is a gain, enter the amount on Form 1040, line 13, then go to line 17.
		//		If line 16 is a loss, skip lines 17 through 20 and go to line 21 and 22.
		//		If line 16 is zero, skip lines 17 through 21 enter 0 on 1040 line 13, then go to
		// line 22.

		// Enter the amount from line 16 on Form 1040 line 13, then go to line 17
		// Are lines 15 and 16 both gains? (Note, we already checked line16
		if (line16 > 0 && netLongTerm > 0) {
			// Go to line 18
			// Enter the amount from line 7 of the 28% Rate Gain Worksheet
			int line18 = calculate28PercentRateGainWorksheet();
			// Enter the amount from line 18 of the Unrecaptured Gain Worksheet
			int line19 = 0;
			// Are lines 18 and 19 both zero?
			if (line18 == 0 && line19 == 0) {
				// Complete Qualified Dividends and Capital Gain Tax Worksheet (1040
				// instructions)
				return calculateQualifiedDividendsAndCapitalGainTaxWorksheet(taxThresholds,
						taxableIncome, qualifiedDividends);
			} else {
				// Complete Schedule D Tax Worksheet, skip lines 21 and 22 (put results on
				// line 44)
				return calculateScheduleDTaxWorksheet(taxableIncome, qualifiedDividends,
						taxThresholds);
			}
			// ELSE: Skip lines 18 - 21, go to line 22
			// Do you have qualified dividends on Form 1040, line 9b, or Form 1040NR, line 10b?
		} else {
			if (qualifiedDividends > 0) {
				// Complete Qualified Dividends and Capital Gain Tax Worksheet (1040 instructions)
				return calculateQualifiedDividendsAndCapitalGainTaxWorksheet(taxThresholds,
						taxableIncome, qualifiedDividends);
			} else {
				// Tax as a typical tax with no gains
				return taxThresholds.calculateIncomeTax(taxableIncome);
			}
		}
	}

	public void setNetShortTerm(int capitalGains) {
		this.netShortTerm += capitalGains;
	}

	public void setNetLongTerm(int capitalGains) {
		this.netLongTerm += capitalGains;
	}

	public int getCapitalGains() {
		return calculateTotalNetGains();
	}

	private int getNetLongTermGains() {
		return netLongTerm;
	}

	private int getLine7() {
		return netShortTerm;
	}

	protected int calculateTotalNetGains() {
		return netLongTerm + netShortTerm;
	}

	/**
	 * Get Net Long Term Gains/Losses. This value is reported on Form 8949 Part II
	 *
	 * @return the amount of long term gains/losses
	 */
	public int getNetLongTerm() {
		return netLongTerm;
	}

	/**
	 * Get Net Short Term Gains/Losses. This value is reported on Form 8949 Part I
	 *
	 * @return the amount of long term gains/losses.
	 */
	public int getNetShortTerm() {
		return netShortTerm;
	}

	/**
	 * Calculates the result from the 28% Rate Gain Worksheet from the Schedule D instructions.
	 * This value is entered on line 18 of the 2014 Schedule D.
	 *
	 * @return the result from the 28% Rate Gain Worksheet
	 */
	protected int calculate28PercentRateGainWorksheet() {
		// Enter the total of all collectibles from 8949 Part II
		int line1 = 0; // Assume all gains are reported from 1099B (it is not common to be > 0)
		// int line2 = 0; // This should always be zero
		// int line3 = 0; // For reported casualties and theft (should always be zero)
		// int line4 = 0; // Related to collectibles, assume income not from collectibles

		// Enter long term capital loss carryover from line 14 on Schedule D
		// TODO: Currently assumes no long term loss carry over
		// int line5 = 0;

		// Enter Schedule D line 7 if it is a loss
		int line6 = Math.max(0, netShortTerm);

		// Combine lines 1 - 6, if <= 0 enter 0, enter this amount on line 18 of Schedule D
		return line1 + line6;
	}

	/**
	 * Calculates the result from the Tax Worksheet in the Schedule D Instructions. This value is
	 * entered on line 19 of the 2014 Schedule D as well as line 44 from the Form 1040.
	 *
	 * @param taxableIncome          the total taxable income (from line 44 of 2014 1040)
	 * @param qualifiedGains         the qualified dividends (from line 9b of 2014 1040)
	 * @param taxThresholds          the brackets and limits for the year
	 * @return the result from the Schedule D Tax Worksheet
	 */
	protected int calculateScheduleDTaxWorksheet(int taxableIncome, int qualifiedGains,
												 TaxThresholds taxThresholds) {

		// TODO: Assume disposition of property is always 0 (keep or change?)
		// Enter the amount from 4952 line 4g (ASSUME 0)
		// Enter the amount from Form 4952 line 4e (ASSUME 0)
		// Subtract line 4 from line 3, if zero or less enter zero (ASSUME 0)
		// Subtract line 5 from line 2, if zero or less enter zero (Becomes Qualified Gains)
		// Enter the smaller of line 15 or line 16 from Schedule D
		int minLongTermORNetGains = Math.min(getNetLongTermGains(), calculateTotalNetGains());
		// Enter the smaller of line 3 or line 4 (Will always be 0)
		// Subtract line 8 from line 7, if zero or less enter zero
		// (Will always be == minLongTermOrNetGains)

		// Add lines 6 and 9
		int totalGains = qualifiedGains + minLongTermORNetGains;
		// Add lines 18 and 19 of schedule D
		int additionalGains = calculate28PctRateGain() + calculateUnrecapturedGains();
		// Enter the smaller of line 9 or 11
		int minCapGainsORAdditionalGains = Math.min(minLongTermORNetGains, additionalGains);
		// Subtract line 12 from line 10
		int line13 = totalGains - minCapGainsORAdditionalGains;
		// Subtract line 13 from line 1, if zero or less enter zero
		int line14 = Math.max(0, taxableIncome - line13);


		double capGainsTax = taxThresholds.calculateCapitalGainsRate(
				taxableIncome - totalGains) * totalGains;


		int line19 = taxableIncome - totalGains;
		//		// Subtract line 17 from line 16, this is taxed at 0%
		//		int line20 = line16 - line17;

		// If lines 1 and 16 are the same, skip lines 21 - 41 and go to line 42.
		//		double line29 = 0;
		//		double line32 = 0;
		double line38 = 0;
		double line41 = 0;
		// If line16 is greater than the min bracket...
		if (capGainsTax > 0) {

			// Skipped lines pertaining to tax bracket calculation, they are unnecessary

			// If Schedule D, line 19, is zero or blank, skip lines 33 - 38 and go to line 39.
			int line37;
			if (calculateUnrecapturedGains() != 0) {
				// Enter the smaller of line 9 or schedule D line 19
				int line33 = Math.min(minLongTermORNetGains, calculateUnrecapturedGains());
				// Add lines 10 and 19
				int line34 = totalGains + line19;
				// Enter the amount from line 1
				int line35 = taxableIncome;
				// Subtract line 35 from line 34. If zero or less, enter -0-
				int line36 = Math.max(0, line34 - line35);
				// Subtract line 36 from line 33. If zero or less, enter -0-
				line37 = Math.max(0, line33 - line36);
				// Multiply line 37 by 0.25
				line38 = line37 * 0.25;
			}
			// TODO: Confirm 28% Rate Gain is always 0
			//			// If Schedule D line 18 is zero or blank, skip lines 39 - 41 and go to
			// line 42.
			//			if (calculate28PctRateGain() != 0) {
			//				// Add lines 19, 20, 28, 31, and 37
			//				int line39 = line19 + line20 + line28 + line31 + line37;
			//				// Subtract line 39 from line 1
			//				int line40 = taxableIncome = line39;
			//				// Multiply line 40 by 0.28
			//				line41 = line40 * 0.28;
			//			}
		}
		// Figure the tax on the value of line 19.
		// If the amount on line 19 is less than $100,000, use the Tax Table to figure the tax.
		// If the amount on line 19 is $100,000 or more, use the Tax Computation Worksheet
		// TODO: May need to store this value, it may change in future years
		// TODO: This capital gains bracket should be supplied
		int line42;
		if (line19 > 100000) {
			line42 = taxThresholds.calculateWithComputationTable(line19);
		} else {
			line42 = taxThresholds.calculateIncomeTax(line19);
		}
		// Add lines 29, 32, 38, 41 and 42
		int line43 = (int) (capGainsTax + line38 + line41 + line42);
		// Figure the tax on the amount on line 1.
		// If the amount on line 1 is less than $100,000, use the Tax Table to figure the tax.
		// If the amount on line 1 is $100,000 or more, use the Tax Computation Worksheet
		int line44;
		if (taxableIncome > 100000) {
			line44 = taxThresholds.calculateWithComputationTable(taxableIncome);
		} else {
			line44 = taxThresholds.calculateIncomeTax(taxableIncome);
		}
		// Tax on all taxable income (including capital gains and qualified dividends).
		// Enter the smaller of line 43 or line 44.
		// Also include this amount on Form 1040, line 44
		int line45 = Math.min(line43, line44);

		return line45;
	}

	protected int calculateQualifiedDividendsAndCapitalGainTaxWorksheet(
			TaxThresholds taxThresholds, int taxableIncome,
			int qualifiedDividends) {
		// I believe we simply run the taxBracket calculations from here
		//		return taxBrackets.calculateTax(taxableIncome);

		// *********** Worksheet Instructions ***************
		//		1. Enter the amount from Form 1040, line 43. (taxableIncome)
		//		2. Enter the amount from Form 1040, line 9b* (qualifiedDividends)
		//		3. Are you filing Schedule D?*
		//			Yes. Enter the smaller of line 15 or 16 of Schedule D.
		//			(15 - Long Term Gains, 16 - Net Total Short & Long Term Gains)
		// 			If either line 15 or line 16 is blank or a loss, enter -0-
		int netGains = Math.min(Math.max(0, calculateTotalNetGains()), Math.max(0, netLongTerm));
		// 		3. No. Enter the amount from Form 1040, line 13
		// We will always file Schedule D when using this calculation
		//		4. Add lines 2 (qualifiedDividends) and 3
		int dividendsAndGains = qualifiedDividends + netGains;
		//		5. If filing Form 4952 (investment interest expense deduction), enter any amount
		// 		   from line 4g of that form. Otherwise, enter 0 (Assume this is always 0)
		//		6. Subtract line 5 from line 4. If zero or less, enter -0-
		//		   Line6 will always equal line 4 since line 5 is always 0
		//		7. Subtract line 6 from line 1 (taxableIncome). If zero or less, enter -0-
		int incomeLessGains = Math.max(0, taxableIncome - dividendsAndGains);

		// Skipped lines pertaining to tax bracket calculation, they are unnecessary

		//		24. Figure the tax on the amount on line 7.
		//			If the amount on line 7 is less than $100,000, use the Tax Table to figure the
		// 			tax.
		//			If the amount on line 7 is $100,000 or more, use the Tax Computation Worksheet
		// TODO: May need to store this value, it may change in future years
		int line24;
		if (incomeLessGains > 100000) {
			line24 = taxThresholds.calculateWithComputationTable(incomeLessGains);
		} else {
			line24 = taxThresholds.calculateIncomeTax(incomeLessGains);
		}
		//		25. Add lines 20, 23, and 24 (10%, 15% and standard tax bracket tax)
		int line25 = (int) ((taxThresholds.calculateCapitalGainsRate(
				incomeLessGains) * dividendsAndGains) + line24);
		//		int line25 = (int) (line20 + line23 + line24);
		//		26. Figure the tax on the amount on line 1.
		//			If the amount on line 1 is less than $100,000, use the Tax Table to figure the
		// 			tax.
		//			If the amount on line 1 is $100,000 or more, use the Tax Computation Worksheet
		int line26;
		if (taxableIncome > 100000) {
			line26 = taxThresholds.calculateWithComputationTable(taxableIncome);
		} else {
			line26 = taxThresholds.calculateIncomeTax(taxableIncome);
		}
		//		27. Tax on all taxable income. Enter the smaller of line 25 or line 26. Also
		// 			include this amount on Form 1040, line 44.
		return Math.min(line25, line26);
	}

	public String toString() {
		return "Schedule D Form (Finish this toString())";
	}

}
