package models.examples;

import models.Retiree;
import models.RetirementPlan;
import models.TenFortyCalculator;
import models.pojo.FilingStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * MainCLI.java
 * <p>
 * A class to provide a command line interface for visualizing calculated results and manually
 * entering custom data.
 * <p>
 * Created by BerlGeof on 11/2/2015.
 */
public class MainCLI {

    private Scanner scanner;

    public MainCLI() {
        scanner = new Scanner(System.in);

        // Print an intro header
        System.out.println("----------------------------------------------");
        System.out.println("------------ Welcome to TERI CLI! ------------");
        System.out.println("----------------------------------------------");

        // Initiate the data collection process

		/* ********************************** */
        /* ********** General Info ********** */
		/* ********************************** */
        Retiree retiree = new Retiree();
        RetirementPlan plan = new RetirementPlan(retiree,null,"Main CLI", new FilingStatus(2, FilingStatus.SINGLE), 1);

        /** Marital Status */
        List<String> filingStatus = new ArrayList<>();
        filingStatus.add("Married");
        filingStatus.add("Single");
        printSelectionMenu(filingStatus, "Select a marital status");

        int selection = scanner.nextInt();
        if (filingStatus.get(selection-1).equals("Married")) {
            plan.filingStatus = new FilingStatus(1, FilingStatus.MARRIED_JOINTLY);
        } else if (filingStatus.get(selection-1).equals("Single")) {
            plan.filingStatus = new FilingStatus(2, FilingStatus.SINGLE);
        }

        /** Dependents */
        int dependentsCount = getInt("Please enter the number of dependents");
        plan.exemptions = dependentsCount;

        /** Create the calculator with the new header information */
        TenFortyCalculator taxForm = new TenFortyCalculator(plan);

		/* ********************************** */
		/* ************ Incomes ************* */
		/* ********************************** */

        /** Earned Income */
        taxForm.addWages(getInt("Please enter Earned Income"));

		taxForm.addQualifiedDividends(getInt("Please enter interest"));

        taxForm.addQualifiedDividends(getInt("Please enter dividends"));

        taxForm.addTaxableRefunds(getInt("Please enter taxable refunds"));

        taxForm.addAlimonyIncome(getInt("Please enter alimony"));

        taxForm.addBusinessIncome(getInt("Please enter business income"));

        taxForm.addLongTermCapitalGains(getInt("Please enter capital gains"));

        taxForm.addOtherGains(getInt("Please enter other gains"));

        taxForm.addIraDistributions(getInt("Please enter IRA Distributions"));

        /** Pension Income */
        taxForm.addPensionsAndAnnuities(getInt("Please enter income from Pensions"));

//        taxForm.setRealEstate(getInt("Please enter real estate income"));

//        taxForm.setFarmIncome(getInt("Please enter farm income"));

//        taxForm.setUnemployment(getInt("Please enter unemployment income"));

        /** Social Security Income */
        taxForm.setSocialSecurityIncome(getInt("Please enter income from Social Security"));

        taxForm.addOtherIncome(getInt("Please enter other income"));


		/* ********************************** */
		/* ***** Deductions & Credits******** */
		/* ********************************** */

//        taxForm.setEducatorExpenses(getInt("Please enter educator expenses"));

        taxForm.setForm2106Value(getInt("Please enter certain business expenses (form 2106)"));

        taxForm.setHealthSavingsDeduction(getInt("Please enter the amount contributed to HSA account(s)"));

//        taxForm.setMovingExpenses(getInt("Please enter moving expenses"));

        taxForm.setSelfEmploymentDeductable(getInt("Please enter deductible part of self-employment tax"));

        taxForm.setSepSIMPPlans(getInt("Please enter Self-employeed SEP, SIMPLE, and qualified plans"));

        taxForm.setSelfEmployHealthInsuranceDeduction(getInt("Please enter self-employed health insurance deduction"));

//        taxForm.setEarlyWithdrawlPenalty(getInt("Please enter penalty on early withdrawal of savings"));

//        taxForm.setAlimony(getInt("Please enter alimony paid"));

        /** Qualified Plan Contributions */
        taxForm.setIraDeduction(getInt("Please enter the amount contributed to Qualified Plans"));

        taxForm.setStudentLoanInterest(getInt("Please enter student loan interest deduction"));

//        taxForm.setTuitionAndFees(getInt("Please enter tuition and fees"));

//        taxForm.setDomesticProductionDeduction(getInt("Please enter domestic production activities deduction"));

//        taxForm.setAlternativeMinTax(getInt("Please enter alternative minimum tax"));

		taxForm.setMortgageInterestCredit(getInt("Please Enter Total Itemized Deductions"));




        taxForm.setPremiumTaxCredit(getInt("Please enter excess advance premium tax credit repayment"));

//        taxForm.setForeignTaxCredit(getInt("Please enter foreign tax credit"));

//        taxForm.setDependentExpenses(getInt("Please enter dependent car expenses"));

//        taxForm.setEduCredits(getInt("Please enter education credits"));

        taxForm.setRetirementSavingsCredit(getInt("Please enter retirement savings contributions credit"));

//        taxForm.setChildTaxCredit(getInt("Please enter child tax credit"));

//        taxForm.setResidentialEnergyCredits(getInt("Please enter residential energy credits"));

        taxForm.setOtherCredits(getInt("Please enter other credits"));



        taxForm.setSelfEmploymentTax(getInt("Please enter self employment tax"));

//        taxForm.setAdditionalRetirementTax(getInt("Please enter additional retirement taxes"));

//        taxForm.setFirstTimeHomebuyerCreditRepayment(getInt("Please enter first time homebuyer credit"));

//        taxForm.setHealthCare(getInt("Please enter healthcare taxes"));

        taxForm.setIncomeTaxWithheld(getInt("Please enter federal income tax withheld from w2 and 1099"));

//        taxForm.setEarnedIncomeCredit(getInt("Please enter earned income credit"));
//        taxForm.setAdditionalChildTaxCredit(getInt("Please enter additional child tax credit"));
//        taxForm.setNetPremiumTaxCredit(getInt("Please enter net premium tax credit"));
        taxForm.setExcessSSTaxWithheld(getInt("Please enter excess SS Tax withheld"));
//        taxForm.setFuelTaxCredit(getInt("Please enter fuel tax credits"));


		/* ********************************** */
		/* ******* Print a Summary ********** */
		/* ********************************** */

        /** Summarize the person and their values */
        System.out.println("----------------------------------------------");
        System.out.println("----------------- Summary --------------------");
        System.out.println("----------------------------------------------");
        System.out.println();
        System.out.println("Filing status: " + plan.filingStatus);
        System.out.println("Dependents: " + dependentsCount);

        /* ********************************** */
		/* ********** Income & AGI*********** */
		/* ********************************** */

        System.out.println("INCOME:\n" +taxForm.getIncomeSummary());
		System.out.println();
        System.out.println("AGI:\n" + taxForm.getAGISummary());
		System.out.println();

		/* ********************************** */
		/* ********** Deductions ************ */
		/* ********************************** */

        System.out.println("TAX AND CREDITS:\n" + taxForm.getTaxAndCreditsSummary());
		System.out.println();

		/* ********************************** */
		/* ********* Other Taxes ************ */
		/* ********************************** */

		System.out.println("OTHER TAXES:\n"+taxForm.getOtherTaxesSummary());
		System.out.println();

		/* ********************************** */
		/* ********** Payments ************** */
		/* ********************************** */

		System.out.println("PAYMENTS:\n"+taxForm.getPaymentsSummary());
		System.out.println();

		/* ********************************** */
		/* ********** Calculated Values ***** */
		/* ********************************** */

        System.out.println("Total AGI: " + taxForm.calculateAGI());
        System.out.println("Taxable Income: " + taxForm.calculateTotalIncome());
        System.out.println("Total Federal Due: " + taxForm.calculateTaxOwed());

        // Clean up
        scanner.close();
    }

    public static void main(String[] args) {
        new MainCLI();
    }

    public void printSelectionMenu(List<String> selections, String prompt) {
        // Print the menu
        System.out.println();
        System.out.println(prompt);
        System.out.println("----------------------------------------------");
        for (int i = 0; i < selections.size(); i++) {
            System.out.println((i + 1) + ". " + selections.get(i));
        }
        System.out.println("----------------------------------------------");
        System.out.println();
        System.out.print("Select an example or enter q to quit: ");
    }

    public int getInt(String prompt) {
        System.out.println();
        System.out.print(prompt + ": ");
        int retVal = scanner.nextInt();
        System.out.println();

        return retVal;
    }


}

