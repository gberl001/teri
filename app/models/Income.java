package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Income.java
 * <p>
 * A class to contain information regarding incomes and compute values based on the incomes. This
 * class covers lines 7 through 22 on the 1040 form.
 * <p>
 * Created by geoffberl on 10/21/15.
 */
public class Income {

	private long id;

	// Possibly use a map for incomes (this way if a new one is added we don't forget to modify
	// the formulas)
	@Constraints.Required
	@Constraints.Min(0)
	private int wages;                  // Line 7
	private int taxableInterest;        // Line 8a (ignore 8b, assume only taxable is given)
	private int qualifiedDividends;        // Line 9a (ignore 9b, assume only taxable is given)
	private int taxableRefunds;         // Line 10
	private int alimony;                // Line 11
	private int businessIncome;         // Line 12 (technically from schedule C)
	private int otherGains;             // Line 14 (technically from 4797)
	/**
	 * IRA Distributions has exceptions but only if they are rolled over or moved directly into
	 * a charitable contribution, in which case they are not taxed but for now that is not
	 * necessary.  In the event of the aforementioned cases, those values would satisfy 15a
	 */
	private int iraDistributions;       // Line 15b (Exceptions exist for line 15a)
	private int rothConversion;         // Line 15b (Assume it is totally taxable)
	private int pensionsAndAnnuities;   // Line 16b (Assume it is totally taxable)
	private int realEstate;             // Line 17
	private int farmIncome;             // Line 18 (technically from schedule F)
	private int unemployment;           // Line 19
	private int otherIncome;            // Line 21
	private int ordinaryDividends;
	private int taxExemptInterest;

	/**
	 * No argument constructor, this will createFilingStatus an models.income object but you must set/add the
	 * incomes
	 * as needed before
	 * calling the calculation to ensure the calculation is correct.
	 */
	public Income() {
		this(0, 0, 0, 0);
	}

	/**
	 * Constructor with common models.income types, this avoids the need to set these common
	 * account
	 * values separately.
	 *
	 * @param wages                the earned models.income
	 * @param alimony              the amount of alimony received
	 * @param otherIncome          the amount of any other models.income not already covered
	 * @param pensionsAndAnnuities the amount of models.income received from pensions and annuities
	 */
	protected Income(int wages, int alimony, int otherIncome, int pensionsAndAnnuities) {

		this.wages = wages;
		this.alimony = alimony;
		this.otherIncome = otherIncome;
		this.pensionsAndAnnuities = pensionsAndAnnuities;
	}

	/**
	 * Covers line 8a of the 1040 form.  This computes the amount of interest that must be counted
	 * as models.income and taxed
	 * as models.income.
	 *
	 * @return the amount of interest that is taxable as models.income.
	 */
	private int calculateTaxableInterest() {
		return taxableInterest;
	}

	/**
	 * Covers line 9a of the 1040 form.  This computes the amount of dividends received that must
	 * be counted as models.income
	 * and taxed as models.income.
	 *
	 * @return the amount of qualified dividends.
	 */
	private int calculateQualifiedDividends() {
		return qualifiedDividends;
	}

	private int calculateOrdinaryDividends() { return ordinaryDividends; }

	/**
	 * Covers line 22 of the 1040 form.  Combines taxable models.income from all accounts to
	 * product a total models.income.  WARNING: Assumes that ssiWS has been properly set up
	 *
	 * @return the total taxable models.income
	 * @throws RuntimeException when SS Income has not been properly set up
	 */
	protected int calculateIncome(SSIWorksheet ssiWS, ScheduleD scheduleD,
								  AGI agi) throws RuntimeException {
		if (ssiWS == null) {
			throw new RuntimeException("SSIWorksheet has not been set up, cannot be null");
		} else if (scheduleD == null) {
			throw new RuntimeException("ScheduleD has not been set up, cannot be null");
		}

		// Combine the amounts in the far right for lines 7 - 21 to calculate the total income
		try {
			return wages + calculateTaxableInterest() + calculateQualifiedDividends() +
					calculateOrdinaryDividends() + taxableRefunds + alimony + businessIncome +
					scheduleD.calculateCapitalGains() + otherGains + iraDistributions +
					rothConversion + pensionsAndAnnuities + realEstate + farmIncome +
					unemployment + ssiWS.calculateTaxableAmount(agi, this, scheduleD) +
					otherIncome;
		} catch (RuntimeException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public void setAlimony(int alimony) {
		this.alimony = alimony;
	}

	public void setBusinessIncome(int businessIncome) {
		this.businessIncome = businessIncome;
	}

	public void setFarmIncome(int farmIncome) {
		this.farmIncome = farmIncome;
	}

	public void setIraDistributions(int iraDistributions) {
		this.iraDistributions = iraDistributions;
	}

	public void setOtherGains(int otherGains) {
		this.otherGains = otherGains;
	}

	public void setOtherIncome(int otherIncome) {
		this.otherIncome = otherIncome;
	}

	public void setRealEstate(int realEstate) {
		this.realEstate = realEstate;
	}

	public void setTaxableRefunds(int taxableRefunds) {
		this.taxableRefunds = taxableRefunds;
	}

	public void setUnemployment(int unemployment) {
		this.unemployment = unemployment;
	}

	public void setWages(int wages) {
		this.wages = wages;
	}

	public void setRothConversion(int rothConversion) {
		this.rothConversion = rothConversion;
	}

	public int getWages() {
		return wages;
	}

	public int getTaxableRefunds() {
		return taxableRefunds;
	}

	/**
	 * Add qualified dividends to be considered as taxable income.  The framework of Income will
	 * handle the determination of what is taxable and what is not.
	 *
	 * @param qualifiedDividends dividends that may be taxable
	 */
	public void setQualifiedDividends(int qualifiedDividends) {
		this.qualifiedDividends = qualifiedDividends;
	}

	public int getQualifiedDividends() {
		return qualifiedDividends;
	}

	public int getAlimony() {
		return alimony;
	}

	public int getBusinessIncome() {
		return businessIncome;
	}

	public int getFarmIncome() {
		return farmIncome;
	}

	public int getIraDistributions() {
		return iraDistributions;
	}

	public int getOtherGains() {
		return otherGains;
	}

	public int getOtherIncome() {
		return otherIncome;
	}

	public int getRealEstate() {
		return realEstate;
	}

	public int getRothConversion() {
		return rothConversion;
	}

	public int getTaxableInterest() {
		return taxableInterest;
	}

	public int getUnemployment() {
		return unemployment;
	}

	@Override
	public String toString() {
		return "Income{" +
				"id=" + id +
				", wages=" + wages +
				", taxableInterest=" + taxableInterest +
				", qualifiedDividends=" + qualifiedDividends +
				", taxableRefunds=" + taxableRefunds +
				", alimony=" + alimony +
				", businessIncome=" + businessIncome +
				", otherGains=" + otherGains +
				", iraDistributions=" + iraDistributions +
				", rothConversion=" + rothConversion +
				", pensionsAndAnnuities=" + pensionsAndAnnuities +
				", realEstate=" + realEstate +
				", farmIncome=" + farmIncome +
				", unemployment=" + unemployment +
				", otherIncome=" + otherIncome +
				", ordinaryDividends=" + ordinaryDividends +
				", taxExemptInterest=" + taxExemptInterest +
				'}';
	}

	public int getOrdinaryDividends() {
		return ordinaryDividends;
	}

	public void setOrdinaryDividends(int ordinaryDividends) {
		this.ordinaryDividends = ordinaryDividends;
	}

	public int getTaxExemptInterest() {
		return taxExemptInterest;
	}

	public void setTaxExemptInterest(int taxExemptInterest) {
		this.taxExemptInterest = taxExemptInterest;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Add interest earned that is to be considered as taxable income.  The framework of Income
	 * will handle the determination of what is taxable and what is not.
	 *
	 * @param taxableInterest interest earned that may be taxable
	 */
	public void setTaxableInterest(int taxableInterest) {
		this.taxableInterest = taxableInterest;
	}

	public int getPensionsAndAnnuities() {
		return pensionsAndAnnuities;
	}

	public void setPensionsAndAnnuities(int pensionsAndAnnuities) {
		this.pensionsAndAnnuities = pensionsAndAnnuities;
	}
}
