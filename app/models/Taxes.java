package models;

import java.util.*;
import java.io.Serializable;

public class Taxes implements Serializable {
	private int totalAGI = 0;
	private int taxableIncome = 0;
	private int totalDue = 0;
	private int year = 0;

	private String ages;

	public Taxes(int agi, int ti, int td) {
		this.totalAGI = agi;
		this.taxableIncome = ti;
		this.totalDue = td;
	}
	public void setYear(int y) {
		this.year = y;
	}
	public int getYear() {
		return this.year;
	}
	
	public void setAges(String ages) {
		this.ages = ages;
	}

	public String getAges() {
		return this.ages;
	}
	public int getTotalAGI() {
		return this.totalAGI;
	}

	public int getTaxableIncome() {
		return this.taxableIncome;
	}

	public int getTotalDue() {
		return this.totalDue;
	}
}