package models;

import java.util.ArrayList;

public class SummaryData {
	public int totalTaxesConventional = 0;
	public ArrayList<YearlyTaxes> conventionalSummary;

	public int totalTaxesOptimized = 0;
	public ArrayList<YearlyTaxes> optimizedSummary;

	public void setConventional(ArrayList<YearlyTaxes> c) {
		this.conventionalSummary = c;
		for(YearlyTaxes y : c) {
			this.totalTaxesConventional += y.taxes.getTotalDue();
		}
	}

	public void setOptimized(ArrayList<YearlyTaxes> o) {
		this.optimizedSummary = o;
		for(YearlyTaxes y : o) {
			this.totalTaxesOptimized += y.taxes.getTotalDue();
		}
	}
}