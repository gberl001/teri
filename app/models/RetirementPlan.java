package models;

import com.avaje.ebean.Model;
import models.pojo.FilingStatus;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import javax.annotation.Nullable;
import javax.persistence.*;
import javax.validation.Valid;
import java.util.*;
import java.io.Serializable;

/**
 * Created by piper on 2/2/16.
 */
@Entity
public class RetirementPlan extends Model implements Serializable {

    @Id
    public long id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(columnDefinition="integer", name="retiree1_id")
	@Constraints.Required(message = "You must add at least the primary Retiree")
	@Valid
    public Retiree retiree1;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(columnDefinition="integer", name="retiree2_id")
    public Retiree retiree2;
	@Constraints.Required(message = "Please provide a plan name")
    public String name;
	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(columnDefinition="integer", name="filing_status_id")
    public FilingStatus filingStatus;
	public int exemptions = 1;

	public int desiredIncome = 0;

	public RetirementPlan() { }

    public RetirementPlan(Retiree retiree1, Retiree retiree2, String name, FilingStatus filingStatus, int exemptions) {
        this.retiree1 = retiree1;
        this.retiree2 = retiree2;
        this.name = name;
        this.filingStatus = filingStatus;
		this.exemptions = exemptions;
    }

	@Override
    public String toString() {
        return "RetirementPlan{" +
                "id=" + id +
                ", retiree1='" + retiree1 + '\'' +
                ", retiree2='" + retiree2 + '\'' +
                ", name='" + name + '\'' +
                ", filingStatus='" + filingStatus + '\'' +
				", exemptions='" + exemptions + "\'" +
                '}';
    }

	public Date getEarliestRetirementDate() {
		// If there is no retiree 2, just return retiree 1's date.
		if (retiree2 == null) {
			return retiree1.dateOfRetirement;
		}

		// Otherwise, check which one is earliest
		if (retiree1.dateOfRetirement.before(retiree2.dateOfRetirement)) {
			return retiree1.dateOfRetirement;
		}
		return retiree2.dateOfRetirement;
	}


    public String getNameYearString(int yinFuture) {
		int year = Calendar.getInstance().get(Calendar.YEAR) + yinFuture;

		if (retiree2 == null) {
			return retiree1.firstName + " (" + Integer.toString(year - retiree1.getYearOfBirth()) + ")";
		} else {
			return retiree1.firstName + " (" + Integer.toString(year - retiree1.getYearOfBirth()) + ")" + "/" + 
				retiree2.firstName + " (" + Integer.toString(year - retiree2.getYearOfBirth()) + ")";
		}
    }

    public void inflateSS(double rate) {
    	if (retiree1.canGetSSBenefits()) {
    		retiree1.inflateSS(rate);
    	}
    	if(retiree2 != null) {
	    	if (retiree2.canGetSSBenefits()) {
	    		retiree2.inflateSS(rate);
	    	}	
    	}
    }

    //Trust that yoy will call this
    public void setRetireeAges() {
    	retiree1.setCurrentYear(0);
    	
    	if(retiree2 != null) {
    		retiree2.setCurrentYear(0);
    	}
    }

    public void nextYear() {
    	retiree1.happyBirthday();

    	if(retiree2 != null) {
    		retiree2.happyBirthday();
    	}
    }

	public ArrayList<MonetaryAccount> getAccounts() {
		ArrayList<MonetaryAccount> retVal = new ArrayList<>(retiree1.monetaryAccounts);
		if(retiree2 != null) {
			retVal.addAll(retiree2.monetaryAccounts);
		}
		return retVal;
	}

	public ArrayList<AccountInvestments> getInvestmentAccounts() {
		ArrayList<AccountInvestments> retVal = new ArrayList<>(retiree1.investmentAccounts);
		if(retiree2 != null) {
			retVal.addAll(retiree2.investmentAccounts);
		}
		return retVal;
	}

	public void makeRealAccounts() {
		retiree1.makeRealAccounts();
		if(retiree2 != null) {
			retiree2.makeRealAccounts();
		}
	}

	public int addInterestUntilRetirement() {
		int years1 = retiree1.addInterestUntilRetirement();
		int years2 = Integer.MAX_VALUE;
		if(retiree2 != null) {
			years2 = retiree2.addInterestUntilRetirement();
		}
		return (years1 < years2) ? years1 : years2;
	}
	/**
	 * Generic query helper for entity with id Long
	 */
	public static Model.Finder<Long, RetirementPlan> find = new Model.Finder<>(RetirementPlan.class);

	/**
	 * Helper method to return all possible options, intended for use in drop down field.
	 * @return a map of RetirementPlan options from the database
	 */
	public static Map<String, String> options() {
		LinkedHashMap<String, String> options = new LinkedHashMap<>();

		for (RetirementPlan acct : RetirementPlan.find.orderBy("id").findList()) {
			options.put(String.valueOf(acct.id), acct.name);
		}

		return options;
	}
}
