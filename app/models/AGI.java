package models;

public class AGI {

	private int totalIncome;
	private int educatorExpenses = 0;
	private int form2106Value = 0;
	private int healthSavingsDeduction = 0;
	private int movingExpenses = 0;
	private int selfEmploymentDeductable = 0;
	private boolean useLongSE = false;
	private int sepSIMPPlans = 0;
	private int selfEmployHealthInsuranceDeduction = 0;
	private int earlyWithdrawlPenalty = 0;
	private int alimony = 0;
	private int iraDeduction = 0;
	private int studentLoanInterest = 0;
	private int tuitionAndFees = 0;
	private int domesticProductionDeduction = 0;

	protected AGI(int ti, int eduexp, int form2106, int hsd, int me, int sed, boolean ul, int ssp, int sehid, int ewp, int ali, int id, int sli, int taf, int dpd) {
		this.totalIncome = ti;
		this.educatorExpenses = eduexp;
		this.form2106Value = form2106;
		this.healthSavingsDeduction = hsd;
		this.movingExpenses = me;
		this.selfEmploymentDeductable = sed;
		this.useLongSE = ul;
		this.sepSIMPPlans = ssp;
		this.selfEmployHealthInsuranceDeduction = sehid;
		this.earlyWithdrawlPenalty = ewp;
		this.alimony = ali;
		this.iraDeduction = id;
		this.studentLoanInterest = sli;
		this.tuitionAndFees = taf;
		this.domesticProductionDeduction = dpd;
	}

	public int calculateAGI() {
		return this.totalIncome - (this.educatorExpenses + 
									this.form2106Value + 
									this.healthSavingsDeduction + 
									this.movingExpenses + 
									this.selfEmploymentDeductable + 
									this.sepSIMPPlans + 
									this.selfEmployHealthInsuranceDeduction + 
									this.earlyWithdrawlPenalty +
									this.alimony + 
									this.iraDeduction + 
									this.studentLoanInterest +
									this.tuitionAndFees + 
									this.domesticProductionDeduction);
	}

	public int calculateTotalAdjustment() {
		return this.educatorExpenses +
				this.form2106Value +
				this.healthSavingsDeduction +
				this.movingExpenses +
				this.selfEmploymentDeductable +
				this.sepSIMPPlans +
				this.selfEmployHealthInsuranceDeduction +
				this.earlyWithdrawlPenalty +
				this.alimony +
				this.iraDeduction +
				this.studentLoanInterest +
				this.tuitionAndFees +
				this.domesticProductionDeduction;
	}

	/**
	 * No argument constructor, this will createFilingStatus an models.AGI object but you must set/add the AGIs
	 * as needed before calling the calculation to ensure the calculation is correct.
	 */
	public AGI() {this(0, 0, 0, 0, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0); }

	protected AGI(int totalIncome) {
		this(totalIncome, 0, 0, 0, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0);
	}

	protected void setAlimony(int alimony) {
		this.alimony = alimony;
	}

	protected void setDomesticProductionDeduction(int domesticProductionDeduction) {
		this.domesticProductionDeduction = domesticProductionDeduction;
	}

	protected void setEarlyWithdrawlPenalty(int earlyWithdrawlPenalty) {
		this.earlyWithdrawlPenalty = earlyWithdrawlPenalty;
	}

	protected void setEducatorExpenses(int educatorExpenses) {
		this.educatorExpenses = educatorExpenses;
	}

	protected void setForm2106Value(int form2106Value) {
		this.form2106Value = form2106Value;
	}

	protected void setBusinessExpenses(int businessExpenses) {
		this.setForm2106Value(businessExpenses);
	}

	protected void setHealthSavingsDeduction(int healthSavingsDeduction) {
		this.healthSavingsDeduction = healthSavingsDeduction;
	}

	protected void setIraDeduction(int iraDeduction) {
		this.iraDeduction = iraDeduction;
	}

	protected void setMovingExpenses(int movingExpenses) {
		this.movingExpenses = movingExpenses;
	}

	protected void setSelfEmployHealthInsuranceDeduction(int selfEmployHealthInsuranceDeduction) {
		this.selfEmployHealthInsuranceDeduction = selfEmployHealthInsuranceDeduction;
	}

	protected void setSelfEmploymentDeductable(int selfEmploymentDeductable) {
		this.selfEmploymentDeductable = selfEmploymentDeductable;
	}

	protected void setSepSIMPPlans(int sepSIMPPlans) {
		this.sepSIMPPlans = sepSIMPPlans;
	}

	protected void setStudentLoanInterest(int studentLoanInterest) {
		this.studentLoanInterest = studentLoanInterest;
	}

	protected void setTotalIncome(int totalIncome) {
		this.totalIncome = totalIncome;
	}

	protected void setTuitionAndFees(int tuitionAndFees) {
		this.tuitionAndFees = tuitionAndFees;
	}

	protected void setUseLongSE(boolean useLongSE) {
		this.useLongSE = useLongSE;
	}

	public int getIraDeduction() {
		return iraDeduction;
	}

	public int getHealthSavingsDeduction() {
		return healthSavingsDeduction;
	}

	public int getAlimony() {
		return alimony;
	}

	public int getDomesticProductionDeduction() {
		return domesticProductionDeduction;
	}

	public int getEarlyWithdrawlPenalty() {
		return earlyWithdrawlPenalty;
	}

	public int getEducatorExpenses() {
		return educatorExpenses;
	}

	public int getForm2106Value() {
		return form2106Value;
	}

	public int getMovingExpenses() {
		return movingExpenses;
	}

	public int getSelfEmployHealthInsuranceDeduction() {
		return selfEmployHealthInsuranceDeduction;
	}

	public int getSelfEmploymentDeductable() {
		return selfEmploymentDeductable;
	}

	public int getSepSIMPPlans() {
		return sepSIMPPlans;
	}

	public int getStudentLoanInterest() {
		return studentLoanInterest;
	}

	public int getTotalIncome() {
		return totalIncome;
	}

	public int getTuitionAndFees() {
		return tuitionAndFees;
	}

	public int getBusinessExpenses() {
		return this.getForm2106Value();
	}
}
