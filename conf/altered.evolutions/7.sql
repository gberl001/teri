create table account_investments (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255) NOT NULL,
  retiree_id                integer NOT NULL,
  pre_tax                   boolean NOT NULL,
  balance                   decimal NOT NULL,
  growth_rate               decimal NOT NULL,
  cost_basis				decimal NOT NULL,
  type                      varchar(10) DEFAULT 'investment',
  FOREIGN KEY(retiree_id) REFERENCES monetary_account(id)
);