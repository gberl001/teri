drop table if exists time_period;
drop table if exists tax_bracket;
drop table if exists threshold_bracket;
drop table if exists tax_thresholds;

create table time_period (
  id                        integer primary key AUTOINCREMENT,
  start_year                integer,
  end_year                  integer,
  amount                    integer,
  account_id                integer,
  FOREIGN KEY(account_id) REFERENCES monetary_account(id))
;

create table tax_bracket (
  id                        integer primary key AUTOINCREMENT,
  low                integer,
  high               integer,
  rate               decimal,
  subtraction_amount integer,
  tax_threshold_id   integer,
  type               varchar(10),
  FOREIGN KEY (tax_threshold_id) REFERENCES tax_thresholds (id)
);

create table threshold_bracket (
  id                        integer primary key AUTOINCREMENT,
  threshold                 integer,
  rate                      decimal,
  tax_threshold_id          integer,
  type                      varchar(10),
  FOREIGN KEY(tax_threshold_id) REFERENCES tax_thresholds(id)
);

create table tax_thresholds (
  id                        integer primary key AUTOINCREMENT,
  year                      integer,
  standard_deduction        integer,
  qualified_div_threshold   integer,
  capital_loss_deduct_limit integer,
  filing_status_id          integer NOT NULL,
  FOREIGN KEY(filing_status_id) REFERENCES filing_status(id)
);

-- Tax Bracket Stuff -----------------------------------------------
-- Single
INSERT INTO tax_thresholds VALUES (1, 2014, 6200, 100000, 1500, 2);
INSERT INTO tax_bracket VALUES(1, 0, 9075, 0.1, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(2, 9076, 36900, 0.15, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(3, 36901, 89350, 0.25, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(4, 89351, 186350, 0.28, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(5, 186351, 405100, 0.33, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(6, 405101, 406750, 0.35, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(7, 406751, 9223372036854775807, 0.396, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(8, 100000, 186350, 0.28, 6824.25, 1, 'comp_table');
INSERT INTO tax_bracket VALUES(9, 186351, 405100, 0.33, 16141.75, 1, 'comp_table');
INSERT INTO tax_bracket VALUES(10, 405101, 406750, 0.35, 24243.75, 1, 'comp_table');
INSERT INTO tax_bracket VALUES(11, 406751, 9223372036854775807, 0.396, 42954.25, 1, 'comp_table');
INSERT INTO threshold_bracket VALUES (1, 36900, 0.15, 1, 'cap_gains');
INSERT INTO threshold_bracket VALUES (2, 405100, 0.25, 1, 'cap_gains');
INSERT INTO threshold_bracket VALUES (3, 25000, 0.50, 1, 'magi');
INSERT INTO threshold_bracket VALUES (4, 34000, 0.85, 1, 'magi');
-- Married
INSERT INTO tax_thresholds VALUES (2, 2014, 12400, 100000, 3000, 1);
INSERT INTO tax_bracket VALUES(12, 0, 18150, 0.1, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(13, 18151, 73800, 0.15, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(14, 73801, 148850, 0.25, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(15, 148851, 226850, 0.28, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(16, 226851, 405100, 0.33, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(17, 405101, 457600, 0.35, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(18, 457601, 9223372036854775807, 0.396, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(19, 100000, 148850, 0.25, 8287.5, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(20, 148851, 226850, 0.28, 12753, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(21, 226851, 405100, 0.33, 24095.5, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(22, 405101, 457600, 0.35, 32197.5, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(23, 457601, 9223372036854775807, 0.396, 53247.1, 2, 'comp_table');
INSERT INTO threshold_bracket VALUES (5, 73800, 0.15, 2, 'cap_gains');
INSERT INTO threshold_bracket VALUES (6, 457600, 0.25, 2, 'cap_gains');
INSERT INTO threshold_bracket VALUES (7, 32000, 0.50, 2, 'magi');
INSERT INTO threshold_bracket VALUES (8, 44000, 0.85, 2, 'magi');