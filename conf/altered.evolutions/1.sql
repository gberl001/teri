PRAGMA foreign_keys = OFF;

drop table if exists filing_status;
drop table if exists retirement_plan;
drop table if exists retiree;
drop table if exists account;  -- Removes old table that may exist
drop table if exists user;
drop table if exists monetary_account;
drop table if exists auth;
drop table if exists mortgage;
drop table if exists account401k;
drop table if exists account_ira;
drop table if exists account_roth;
drop table if exists account_investments;
drop table if exists time_period;
drop table if exists tax_bracket;
drop table if exists threshold_bracket;
drop table if exists tax_thresholds;

PRAGMA foreign_keys = ON;

create table filing_status (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255) NOT NULL
);

create table retiree (
  id                            integer primary key AUTOINCREMENT,
  first_name                    varchar(255) NOT NULL,
  last_name                     varchar(255),
  date_of_birth                 date NOT NULL,
  date_of_retirement            date NOT NULL,
  date_of_departure             date,
  max_social_security_benefits  integer DEFAULT 0,
  dateof_ssbenefits             date DEFAULT -787003200000,
  pension                       integer DEFAULT 0
);

create table monetary_account (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255) NOT NULL,
  retiree_id                integer NOT NULL,
  pre_tax                   boolean NOT NULL,
  balance                   decimal NOT NULL,
  growth_rate               decimal NOT NULL,
  type                      varchar(10) DEFAULT 'cash',
  FOREIGN KEY(retiree_id) REFERENCES monetary_account(id)
);

create table retirement_plan (
  id                        integer primary key AUTOINCREMENT,
  retiree1_id               integer NOT NULL,
  retiree2_id               integer DEFAULT NULL,
  name                      varchar(255) NOT NULL,
  exemptions                integer NOT NULL,
  filing_status_id          integer NOT NULL,
  desired_income            integer DEFAULT 0,
  FOREIGN KEY(retiree1_id) REFERENCES retirement_plan(id),
  FOREIGN KEY(retiree2_id) REFERENCES retirement_plan(id),
  FOREIGN KEY(filing_status_id) REFERENCES filing_status(id)
);

create table auth (
  id                        integer primary key AUTOINCREMENT,
  username                  varchar(255) NOT NULL,
  password_hash             varchar(60) NOT NULL
);

create table mortgage (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(100) NOT NULL,
  principal_balance         integer NOT NULL,
  term                      integer NOT NULL,
  interest_rate             decimal NOT NULL,
  begin_date                date NOT NULL,
  retiree_id                integer NOT NULL,
  FOREIGN KEY(retiree_id) REFERENCES retiree(id)
);

create table account_investments (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255) NOT NULL,
  retiree_id                integer NOT NULL,
  pre_tax                   boolean NOT NULL,
  balance                   decimal NOT NULL,
  growth_rate               decimal NOT NULL,
  cost_basis				decimal NOT NULL,
  type                      varchar(10) DEFAULT 'investment',
  FOREIGN KEY(retiree_id) REFERENCES monetary_account(id)
);

create table time_period (
  id                        integer primary key AUTOINCREMENT,
  start_year                integer,
  end_year                  integer,
  amount                    integer,
  account_id                integer,
  FOREIGN KEY(account_id) REFERENCES monetary_account(id))
;

create table tax_bracket (
  id                        integer primary key AUTOINCREMENT,
  low                integer,
  high               integer,
  rate               decimal,
  subtraction_amount integer,
  tax_threshold_id   integer,
  type               varchar(10),
  FOREIGN KEY (tax_threshold_id) REFERENCES tax_thresholds (id)
);

create table threshold_bracket (
  id                        integer primary key AUTOINCREMENT,
  threshold                 integer,
  rate                      decimal,
  tax_threshold_id          integer,
  type                      varchar(10),
  FOREIGN KEY(tax_threshold_id) REFERENCES tax_thresholds(id)
);

create table tax_thresholds (
  id                        integer primary key AUTOINCREMENT,
  year                      integer,
  standard_deduction        integer,
  qualified_div_threshold   integer,
  capital_loss_deduct_limit integer,
  filing_status_id          integer NOT NULL,
  FOREIGN KEY(filing_status_id) REFERENCES filing_status(id)
);

create index ix_retiree_filingStatus_1 on retirement_plan (filing_status_id);
create index ix_auth_1                 on auth(username);



-- ******** Inserts *************
PRAGMA foreign_keys = OFF;

-- Tax Bracket Stuff -----------------------------------------------
-- Single
INSERT INTO tax_thresholds VALUES (1, 2014, 6200, 100000, 1500, 2);
INSERT INTO tax_bracket VALUES(1, 0, 9075, 0.1, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(2, 9076, 36900, 0.15, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(3, 36901, 89350, 0.25, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(4, 89351, 186350, 0.28, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(5, 186351, 405100, 0.33, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(6, 405101, 406750, 0.35, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(7, 406751, 9223372036854775807, 0.396, 0, 1, 'income');
INSERT INTO tax_bracket VALUES(8, 100000, 186350, 0.28, 6824.25, 1, 'comp_table');
INSERT INTO tax_bracket VALUES(9, 186351, 405100, 0.33, 16141.75, 1, 'comp_table');
INSERT INTO tax_bracket VALUES(10, 405101, 406750, 0.35, 24243.75, 1, 'comp_table');
INSERT INTO tax_bracket VALUES(11, 406751, 9223372036854775807, 0.396, 42954.25, 1, 'comp_table');
INSERT INTO threshold_bracket VALUES (1, 36900, 0.15, 1, 'cap_gains');
INSERT INTO threshold_bracket VALUES (2, 405100, 0.25, 1, 'cap_gains');
INSERT INTO threshold_bracket VALUES (3, 25000, 0.50, 1, 'magi');
INSERT INTO threshold_bracket VALUES (4, 34000, 0.85, 1, 'magi');
-- Married
INSERT INTO tax_thresholds VALUES (2, 2014, 12400, 100000, 3000, 1);
INSERT INTO tax_bracket VALUES(12, 0, 18150, 0.1, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(13, 18151, 73800, 0.15, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(14, 73801, 148850, 0.25, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(15, 148851, 226850, 0.28, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(16, 226851, 405100, 0.33, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(17, 405101, 457600, 0.35, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(18, 457601, 9223372036854775807, 0.396, 0, 2, 'income');
INSERT INTO tax_bracket VALUES(19, 100000, 148850, 0.25, 8287.5, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(20, 148851, 226850, 0.28, 12753, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(21, 226851, 405100, 0.33, 24095.5, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(22, 405101, 457600, 0.35, 32197.5, 2, 'comp_table');
INSERT INTO tax_bracket VALUES(23, 457601, 9223372036854775807, 0.396, 53247.1, 2, 'comp_table');
INSERT INTO threshold_bracket VALUES (5, 73800, 0.15, 2, 'cap_gains');
INSERT INTO threshold_bracket VALUES (6, 457600, 0.25, 2, 'cap_gains');
INSERT INTO threshold_bracket VALUES (7, 32000, 0.50, 2, 'magi');
INSERT INTO threshold_bracket VALUES (8, 44000, 0.85, 2, 'magi');

-- Auth account(s) -------------------------------------------------
INSERT INTO auth VALUES (1, 'admin', '$2a$10$MDq/t9ddIGc03QdGLx65pOSOU8dlrPXuS9z2GEJ99zZyRbqnPq56G');
-- Filing Status Types ---------------------------------------------
INSERT INTO filing_status VALUES (1, 'Married Filing Jointly');
INSERT INTO filing_status VALUES (2, 'Single');

-- Accounts Stuff --------------------------------------------------
-- Elderly Couple
INSERT INTO retiree VALUES (1, 'Elderly', 'Husband', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retiree VALUES (2, 'Elderly', 'Wife', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retirement_plan (id, retiree1_id, retiree2_id, name, exemptions, filing_status_id) VALUES (1, 1, 2, 'Elderly Couple', 2, 1);
INSERT INTO monetary_account VALUES (1, 'Some Account', 1, 0, 200000.00, 0.03, 'cash');
INSERT INTO monetary_account VALUES (2, 'Another Account', 1, 1, 150000.00, 0.03, 'cash');
INSERT INTO monetary_account VALUES (4, 'Roth IRA', 1, 0, 42465.23, 0.12, 'roth');
INSERT INTO monetary_account VALUES (5, '401K', 1, 1, 1869203.10, 0.09, '401k');
INSERT INTO monetary_account VALUES (6, 'Annuity', 1, 0, 34195.93, 0.001, 'cash');
INSERT INTO monetary_account VALUES (7, 'Pension', 1, 1, 549672.02, 0.01, 'cash');
INSERT INTO monetary_account VALUES (3, 'The Mrs'' account', 2, 0, 100000.00, 0.025, 'cash');
INSERT INTO mortgage VALUES (1, 'Primary Residence', 100000, 30, 0.035, 1740692364000, 1);
-- Single Retired Guy
INSERT INTO retiree VALUES (3, 'Single', 'Retiree', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retirement_plan (id, retiree1_id, name, exemptions, filing_status_id) VALUES (2, 3, 'Single Retired Guy', 1, 2);
-- Monte Burns
INSERT INTO retiree VALUES (4, 'Monte', 'Burns', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retirement_plan (id, retiree1_id, name, exemptions, filing_status_id) VALUES (3, 4, 'Mr. Monte Burns', 1, 2);
-- Scrooge McDuck
INSERT INTO retiree VALUES (5, 'Scrooge', 'McDuck', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retirement_plan (id, retiree1_id, name, exemptions, filing_status_id) VALUES (4, 5, 'Scrooge McDuck', 1, 2);
-- Willy and Linda Loman
INSERT INTO retiree VALUES (6, 'Willy', 'Loman', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retiree VALUES (7, 'Linda', 'Loman', -787003200000,	1106456400000, 1740692364000, 0, -787003200000, 0);
INSERT INTO retirement_plan (id, retiree1_id, retiree2_id, name, exemptions, filing_status_id) VALUES (5, 6, 7, 'The Lomans', 4, 1);
PRAGMA foreign_keys = ON;