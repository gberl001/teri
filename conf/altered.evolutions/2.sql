PRAGMA foreign_keys = OFF;

drop table if exists account401k;

drop table if exists account_ira;

drop table if exists account_roth;

drop table if exists filing_status;

drop table if exists monetary_account;

drop table if exists retiree;

drop table if exists retirement_plan;

drop table if exists time_period;

PRAGMA foreign_keys = ON;

create table account401k (
  dtype                     varchar(10) not null,
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255),
  retiree_id                integer,
  pre_tax                   integer(1),
  balance                   integer,
  growth_rate               double,
  FOREIGN KEY(retiree_id) REFERENCES account401k(id))
;

create table account_ira (
  dtype                     varchar(10) not null,
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255),
  retiree_id                integer,
  pre_tax                   integer(1),
  balance                   integer,
  growth_rate               double,
  FOREIGN KEY(retiree_id) REFERENCES account_ira(id))
;

create table account_roth (
  dtype                     varchar(10) not null,
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255),
  retiree_id                integer,
  pre_tax                   integer(1),
  balance                   integer,
  growth_rate               double,
  FOREIGN KEY(retiree_id) REFERENCES account_roth(id))
;

create table filing_status (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255))
;

create table monetary_account (
  id                        integer primary key AUTOINCREMENT,
  name                      varchar(255),
  retiree_id                integer,
  pre_tax                   integer(1),
  balance                   integer,
  growth_rate               double,
  FOREIGN KEY(retiree_id) REFERENCES monetary_account(id))
;

create table retiree (
  id                        integer primary key AUTOINCREMENT,
  first_name                varchar(255),
  last_name                 varchar(255),
  date_of_birth             date,
  date_of_retirement        date
);

create table retirement_plan (
  id                        integer primary key AUTOINCREMENT,
  retiree1_id                  integer,
  retiree2_id                  integer,
  name                      varchar(255),
  exemptions                integer,
  filing_status_id          integer,
  FOREIGN KEY(retiree1_id) REFERENCES retirement_plan(id),
  FOREIGN KEY(retiree2_id) REFERENCES retirement_plan(id),
  FOREIGN KEY(filing_status_id) REFERENCES filing_status(id)
);

create table time_period (
  id                        integer primary key AUTOINCREMENT,
  start_year                integer,
  end_year                  integer,
  amount                    integer,
  account_id                integer,
  FOREIGN KEY(account_id) REFERENCES monetary_account(id))
;

--alter table account401k add constraint fk_account401k_retiree_1 foreign key (retiree_id) references retiree (id);
create index ix_account401k_retiree_1 on account401k (retiree_id);
--alter table account_ira add constraint fk_account_ira_retiree_2 foreign key (retiree_id) references retiree (id);
create index ix_account_ira_retiree_2 on account_ira (retiree_id);
--alter table account_roth add constraint fk_account_roth_retiree_3 foreign key (retiree_id) references retiree (id);
create index ix_account_roth_retiree_3 on account_roth (retiree_id);
--alter table monetary_account add constraint fk_monetary_account_retiree_4 foreign key (retiree_id) references retiree (id);
create index ix_monetary_account_retiree_4 on monetary_account (retiree_id);
--alter table retirement_plan add constraint fk_retirement_plan_retiree1_5 foreign key (retiree1_id) references retiree (id);
create index ix_retirement_plan_retiree1_5 on retirement_plan (retiree1_id);
--alter table retirement_plan add constraint fk_retirement_plan_retiree2_6 foreign key (retiree2_id) references retiree (id);
create index ix_retirement_plan_retiree2_6 on retirement_plan (retiree2_id);
--alter table retirement_plan add constraint fk_retirement_plan_filingStatu_7 foreign key (filing_status_id) references filing_status (id);
create index ix_retirement_plan_filingStatu_7 on retirement_plan (filing_status_id);
--alter table time_period add constraint fk_time_period_account_8 foreign key (account_id) references monetary_account (id);
create index ix_time_period_account_8 on time_period (account_id);


