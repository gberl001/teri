import play.sbt.PlayImport._
import play.sbt.routes.RoutesKeys._

name := "teri_play_framework"

version := "1.0"

organization := "com.teri"

description := "A Tax Efficient Retirement IncomeController Calculator"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "org.webjars" % "jquery" % "2.1.4",
  "org.webjars" % "bootstrap" % "3.3.6",
  "joda-time" % "joda-time" % "2.3",
  "org.webjars" % "font-awesome" % "4.5.0",
  "org.xerial" % "sqlite-jdbc" % "3.8.11.2",
  "org.mindrot" % "jbcrypt" % "0.3m"
)

resolvers += "SQLite-JDBC Repository" at "https://oss.sonatype.org/content/repositories/snapshots"

lazy val `teri_play_framework` = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/base")

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
// NOTE: We should update this but it requires removing all static keywords from the controllers
//       so it should probably be a calculated event.
// routesGenerator := InjectedRoutesGenerator