package models.pojo;

import junit.framework.TestCase;

import java.util.Set;
import java.util.TreeSet;

/**
 * TestTaxThresholds.java
 * <p>
 * <p>
 * <p>
 * Created by geoffberl on 10/21/15.
 */
public class TestTaxThresholds extends TestCase {


	public void testTaxableIncomeMarriedJoint2014() {
		Set<TaxBracket> married14 = new TreeSet<>();
		married14.add(new TaxBracket(0, 18150, 0.1)); // 1815.00
		married14.add(new TaxBracket(18151, 73800, 0.15)); // 8347.35
		married14.add(new TaxBracket(73801, 148850, 0.25)); // 5337.25
		married14.add(new TaxBracket(148851, 226850, 0.28));
		married14.add(new TaxBracket(226851, 405100, 0.33));
		married14.add(new TaxBracket(405101, 457600, 0.35));
		married14.add(new TaxBracket(457601, Integer.MAX_VALUE, 0.396));
		TaxThresholds taxThresholds = new TaxThresholds(1, 2014, null, null, married14, null,
				12400,
				100000, 3000, new FilingStatus(1, FilingStatus.MARRIED_JOINTLY));

		// Test the tax on a Taxable Income of $95,150 (Actual comes to 15499.6 so rounding up...)
		assertEquals(15500, taxThresholds.calculateIncomeTax(95150));
	}

}

