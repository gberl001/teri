package models.pojo;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * TestTaxThresholds.java
 * <p>
 * Tax Computation Table is used, in specific cases, for taxable incomes greater than $100,000.
 * The tax is also computed differently whereas rather than adding all brackets up to a point,
 * the isRequired is taxed only on one specific bracket. This test case is designed to test those
 * cases.
 * <p>
 * Created by geoffberl on 10/21/15.
 */
public class TestTaxComputationTable {

	private static TaxThresholds taxThresholds;

	@BeforeClass
	public static void setup() {
		Set<TaxBracket> taxComputationMarried14 = new TreeSet<>();
		Set<TaxBracket> taxComputationSingle14 = new TreeSet<>();
		taxComputationMarried14.add(new TaxBracket(100000, 148850, 8287.5, 0.25));
		taxComputationMarried14.add(new TaxBracket(148851, 226850, 12753, 0.28));
		taxComputationMarried14.add(new TaxBracket(226851, 405100, 24095.5, 0.33));
		taxComputationMarried14.add(new TaxBracket(405101, 457600, 32197.5, 0.35));
		taxComputationMarried14.add(new TaxBracket(457601, Integer.MAX_VALUE, 53247.1, 0.396));
		taxComputationSingle14.add(new TaxBracket(100000, 186350, 6824.25, 0.28));
		taxComputationSingle14.add(new TaxBracket(186351, 405100, 16141.75, 0.33));
		taxComputationSingle14.add(new TaxBracket(405101, 406750, 24243.75, 0.35));
		taxComputationSingle14.add(new TaxBracket(406751, Integer.MAX_VALUE, 42954.25, 0.396));
		taxThresholds = new TaxThresholds(1, 2014, taxComputationMarried14, null, null, null,
				12400,
				100000, 3000, new FilingStatus(1, FilingStatus.MARRIED_JOINTLY));
	}


	@Test
	public void testGeneralCase() {
		assertThat(taxThresholds.calculateWithComputationTable(140850), is(26925));
	}
}

