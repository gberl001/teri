package models.pojo;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by BerlGeof on 11/16/2015.
 */
public class ThresholdBracketCollectionTest {

	private static TaxThresholds taxThresholds;

	@BeforeClass
	public static void setup() {
		Set<ThresholdBracket> magiBrackets = new TreeSet<>();
		magiBrackets.add(new ThresholdBracket(32000, 0.5));
		magiBrackets.add(new ThresholdBracket(44000, 0.85));
		taxThresholds = new TaxThresholds(1, 2014, null, null, null, magiBrackets, 12400, 100000,
				3000, new FilingStatus(1, FilingStatus.MARRIED_JOINTLY));
	}

	@Test
	public void testFirstThreshold() throws Exception {
		// A single test for 50%
		assertThat(taxThresholds.calculateMAGITaxRate(33000), is(0.5));
	}

	@Test
	public void testSecondThreshold() throws Exception {
		// A single test for 85%
		assertThat(taxThresholds.calculateMAGITaxRate(44001), is(0.85));
	}
}