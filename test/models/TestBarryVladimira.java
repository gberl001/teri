package models;

import models.pojo.FilingStatus;
import models.pojo.TaxBracket;
import models.pojo.TaxThresholds;
import models.pojo.ThresholdBracket;
import org.hamcrest.core.Is;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * TestBarryVladimira.java
 *
 * A Test for a married couple with a modest income and fairly modest capital gains.
 * Testing specifically for the following cases
 * - Social Security should be taxable at 85% based on their SSI.
 * - ...
 *
 * Created by BerlGeof on 11/6/2015.
 */
public class TestBarryVladimira {

	private static final int EXP_ADJUSTMENTS = 6000;
	private static final int EXP_CAPITAL_GAINS = 70000;
	private static final int EXP_AGI = 96750;
	private static final int EXP_DEDUCTIONS = 12400;
	private static final int EXP_EXEMPTIONS = 11850;
	private static final int EXP_TOTAL_INCOME = 102750;
	private static final int EXP_TAXABLE_INCOME = 72500;
	private static final int EXP_TOTAL_CREDITS = 0;
	private static final int EXP_SOCIAL_SECURITY = 12750;
	// TODO: Actually computes to ### when using ginormous tax table
	private static final int EXP_TAX_LESS_CREDITS = 251;
	private static final int EXP_OTHER_TAXES = 0;
	private static final int EXP_PAYMENTS = 0;
	// TODO: Actually computes to ### when using ginormous tax table
	private static final int EXP_TAXES_DUE = 251;

	private static TenFortyCalculator form;

	@BeforeClass
	public static void setup() {

		// Stuff normally from the database
		// TODO: Setup tests so they are able to utilize the database
		Set<TaxBracket> taxBrackets = new TreeSet<>();
		Set<TaxBracket> taxComputationBrackets = new TreeSet<>();
		Set<ThresholdBracket> capGainsBrackets = new TreeSet<>();
		Set<ThresholdBracket> magiBrackets = new TreeSet<>();
		taxBrackets.add(new TaxBracket(0, 18150, 0.1));
		taxBrackets.add(new TaxBracket(18151, 73800, 0.15));
		taxBrackets.add(new TaxBracket(73801, 148850, 0.25));
		taxBrackets.add(new TaxBracket(148851, 226850, 0.28));
		taxBrackets.add(new TaxBracket(226851, 405100, 0.33));
		taxBrackets.add(new TaxBracket(405101, 457600, 0.35));
		taxBrackets.add(new TaxBracket(457601, Integer.MAX_VALUE, 0.396));
		taxComputationBrackets.add(new TaxBracket(100000, 148850, 8287.5, 0.25));
		taxComputationBrackets.add(new TaxBracket(148851, 226850, 12753, 0.28));
		taxComputationBrackets.add(new TaxBracket(226851, 405100, 24095.5, 0.33));
		taxComputationBrackets.add(new TaxBracket(405101, 457600, 32197.5, 0.35));
		taxComputationBrackets.add(new TaxBracket(457601, Integer.MAX_VALUE, 53247.1, 0.396));
		capGainsBrackets.add(new ThresholdBracket(36900, 0.15));
		capGainsBrackets.add(new ThresholdBracket(457600, 0.2));
		magiBrackets.add(new ThresholdBracket(32000, 0.5));
		magiBrackets.add(new ThresholdBracket(44000, 0.85));

		FilingStatus status = new FilingStatus(1, FilingStatus.MARRIED_JOINTLY);
		TaxThresholds taxThresholds = new TaxThresholds(1, 2014, taxComputationBrackets,
				capGainsBrackets, taxBrackets, magiBrackets, 12400, 100000, 3000, status);

		Retiree retiree = new Retiree("Barry", "Vladimira", new Date(), new Date());

		RetirementPlan plan = new RetirementPlan(retiree, null, "Barry V", status, 3);

		form = new TenFortyCalculator(plan, taxThresholds);

		form.addWages(0);
		form.addOrdinaryDividends(0);
		form.addLongTermCapitalGains(70000);
		form.addPensionsAndAnnuities(15000);
		form.setSocialSecurityIncome(15000);
		form.addRothConversion(5000);

		//		form.setTotalItemizedDeductionAmount(16760);
		form.setHealthSavingsDeduction(5000);
		form.setStudentLoanInterest(1000);
		form.setPropertyTaxCredit(4000);
		form.setCharitableContributionsCredit(3000);
	}

	@Test
	public void testTotalIncome() throws Exception {
		assertEquals(EXP_TOTAL_INCOME, form.calculateTotalIncome());
	}

	@Test
	public void testSocialSecurity() throws Exception {
		// Note: total income should be total of income values minus 15% of SS Income
		assertEquals(EXP_SOCIAL_SECURITY,
				form.getSocialSecurityWorksheet().calculateTaxableAmount(form.agi(), form.income(),
						form.getScheduleD()));
	}

	@Test
	public void testAdjustments() {
		assertEquals(EXP_ADJUSTMENTS, form.agi().calculateTotalAdjustment());
	}

	@Test
	public void testAGI() throws Exception {
		assertEquals(EXP_AGI, form.calculateAGI());
	}

	@Test
	public void testDeductions() throws Exception {
		// Should be itemized, not standard
		assertEquals(EXP_DEDUCTIONS,
				form.taxAndCredits().calculateDeductions(form.getStandardDeduction()));
	}

	@Test
	public void testExemptions() {
		assertEquals(EXP_EXEMPTIONS, form.calculateExemptions());
	}

	@Test
	public void testTaxableIncome() {
		assertEquals(EXP_TAXABLE_INCOME,
				form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
						form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction
								()));
	}

	@Test
	public void testTotalCredits() {
		assertEquals(EXP_TOTAL_CREDITS, form.taxAndCredits().calculateTotalCredits());
	}

	@Test
	public void testCapitalGainsIncome() {
		// As a single filer, you can only claim 3000 in losses.
		assertThat(form.getScheduleD().calculateCapitalGains(), Is.is(EXP_CAPITAL_GAINS));
	}

	@Test
	public void testTotalTaxAndCredits() {
		// Assert that the actual is within 1% of the expected value
		assertEquals(EXP_TAX_LESS_CREDITS, form.calculateTaxAndCredits(), EXP_TAX_LESS_CREDITS*0.1);
	}

	@Test
	public void testOtherTaxes() throws Exception {
		assertEquals(EXP_OTHER_TAXES, form.calculateOtherTaxes());
	}

	@Test
	public void testPayments() throws Exception {
		assertEquals(EXP_PAYMENTS, form.calculateTotalPayments());
	}

	@Test
	public void testTaxesDue() throws Exception {
		// Assert that the actual is within 1% of the expected value
		assertEquals(EXP_TAXES_DUE, form.calculateTaxOwed(), EXP_TAXES_DUE*0.1);
	}
}
