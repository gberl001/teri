package models;

import models.pojo.FilingStatus;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by BerlGeof on 11/6/2015.
 */
public class TestHanSoloAndLeia {

	private static TenFortyCalculator form;

	private static final int EXP_ADJUSTMENTS = 3000;
	private static final int EXP_CAPITAL_GAINS = 17500;
	private static final int EXP_AGI = 170750;
	private static final int EXP_DEDUCTIONS = 22000;
	private static final int EXP_EXEMPTIONS = 7900;
	private static final int EXP_TOTAL_INCOME = 173750;
	private static final int EXP_TAXABLE_INCOME = 140850;
	private static final int EXP_TOTAL_CREDITS = 0;
	// TODO: Actually computes to 28288 when using ginormous tax table
	private static final int EXP_TAX_LESS_CREDITS = 25175;
	private static final int EXP_OTHER_TAXES = 0;
	private static final int EXP_PAYMENTS = 0;
	// TODO: Actually computes to 28288 when using ginormous tax table
	private static final int EXP_TAXES_DUE = 25175;

//	@BeforeClass
//	public static void setup() {
//
//		Retiree retiree = new Retiree("The", "Solos", new Date(),
//				new FilingStatus(1, FilingStatus.options().get("1")), 2);
//
//		form = new TenFortyCalculator(retiree);
//
//		form.addLongTermCapitalGains(17500);
//		form.addPensionsAndAnnuities(92500);
//		form.setSocialSecurityIncome(75000);
//
//		form.setHealthSavingsDeduction(3000);
//
//		form.setTotalItemizedDeductionAmount(22000);
//	}
//
//	@Test
//	public void testTotalIncome() throws Exception {
//		// Note: total income should be total of income values minus 15% of SS Income
//		assertEquals(EXP_TOTAL_INCOME, form.calculateTotalIncome());
//	}
//
//	@Test
//	public void testAdjustments() {
//		assertEquals(EXP_ADJUSTMENTS, form.agi().calculateTotalAdjustment());
//	}
//
//	@Test
//	public void testAGI() throws Exception {
//		assertEquals(EXP_AGI, form.calculateAGI());
//	}
//
//	@Test
//	public void testDeductions() throws Exception {
//		// Should be itemized, not standard
//		assertEquals(EXP_DEDUCTIONS, form.taxAndCredits().calculateDeductions(form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testExemptions() {
//		assertEquals(EXP_EXEMPTIONS, form.calculateExemptions());
//	}
//
//	@Test
//	public void testTaxableIncome() {
//		assertEquals(EXP_TAXABLE_INCOME, form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
//				form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testTotalCredits() {
//		assertEquals(EXP_TOTAL_CREDITS, form.taxAndCredits().calculateTotalCredits());
//	}
//
//	@Test
//	public void testCapitalGainsIncome() {
//		// As a single filer, you can only claim 3000 in losses.
//		assertThat(form.getScheduleD().calculateCapitalGains(), is(EXP_CAPITAL_GAINS));
//	}
//
//	@Test
//	public void testTotalTaxAndCredits() {
////		int taxableIncome = form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
////				form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction());
////		int qualifiedDiv = form.income().getQualifiedDividends();
////
////		System.out.println("TOTAL INCOME: " + form.calculateTotalIncome());
////
////		System.out.println(
////				"TAX: " + form.getScheduleD().calculateTaxOnIncomeWithGains(form.getTaxBrackets(),
////						taxableIncome, qualifiedDiv, form.getCapitalGainsBrackets(),
////						form.getTaxComputationBrackets()));
////
////		System.out.println("Schedule D WS: " + form.getScheduleD().calculateScheduleDTaxWorksheet(
////				taxableIncome, qualifiedDiv, form.getTaxBrackets(),
////				form.getTaxComputationBrackets(), form.getCapitalGainsBrackets()));
////
////		System.out.println(
////				"QUALIFIED GAINS WS: " + form.getScheduleD()
////						.calculateQualifiedDividendsAndCapitalGainTaxWorksheet(
////						form.getTaxBrackets(), form.getTaxComputationBrackets(),
////						form.getCapitalGainsBrackets(), taxableIncome, qualifiedDiv));
////
////		System.out.println("CAP GAINS: " + form.getScheduleD().getCapitalGains());
////
////		System.out.println(form.getTaxAndCreditsSummary());
//
//		assertEquals(EXP_TAX_LESS_CREDITS, form.calculateTaxAndCredits());
//	}
//
//	@Test
//	public void testOtherTaxes() throws Exception {
//		assertEquals(EXP_OTHER_TAXES, form.calculateOtherTaxes());
//	}
//
//	@Test
//	public void testPayments() throws Exception {
//		assertEquals(EXP_PAYMENTS, form.calculateTotalPayments());
//	}
//
//	@Test
//	public void testTaxesDue() throws Exception {
//		assertEquals(EXP_TAXES_DUE, form.calculateTaxOwed());
//	}

}
