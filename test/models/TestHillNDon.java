package models;

import models.pojo.FilingStatus;
import org.hamcrest.core.Is;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by BerlGeof on 11/6/2015.
 */
public class TestHillNDon {

	private static final int EXP_ADJUSTMENTS = 2500;
	private static final int EXP_CAPITAL_GAINS = 65000;
	private static final int EXP_AGI = 187300;
	private static final int EXP_DEDUCTIONS = 24000;
	private static final int EXP_EXEMPTIONS = 7900;
	private static final int EXP_TOTAL_INCOME = 189800;
	private static final int EXP_TAXABLE_INCOME = 155400;
	private static final int EXP_TOTAL_CREDITS = 0;
	private static final int EXP_SOCIAL_SECURITY = 32300;
	// TODO: Actually computes to 24062 when using ginormous tax table
	private static final int EXP_TAX_LESS_CREDITS = 23819;
	private static final int EXP_OTHER_TAXES = 0;
	private static final int EXP_PAYMENTS = 0;
	// TODO: Actually computes to 24062 when using ginormous tax table
	private static final int EXP_TAXES_DUE = 23819;

	private static TenFortyCalculator form;

//	@BeforeClass
//	public static void setup() {
//
//		Retiree retiree = new Retiree("Hill", "Don", new Date(),
//				new FilingStatus(1, FilingStatus.options().get("1")), 2);
//
//		form = new TenFortyCalculator(retiree);
//
//		form.addWages(15000);
//		form.addOrdinaryDividends(2500);
//		form.addLongTermCapitalGains(65000);
//		form.addPensionsAndAnnuities(75000);
//		form.setSocialSecurityIncome(38000);
//		form.addRothConversion(0);
//
////		form.setTotalItemizedDeductionAmount(16760);
//		form.setHealthSavingsDeduction(2500);
//		form.setMortgageInterestCredit(24000);
//	}
//
//	@Test
//	public void testTotalIncome() throws Exception {
//		assertEquals(EXP_TOTAL_INCOME, form.calculateTotalIncome());
//	}
//
//	@Test
//	public void testSocialSecurity() throws Exception {
//		// Note: total income should be total of income values minus 15% of SS Income
//		assertEquals(EXP_SOCIAL_SECURITY,
//				form.getSocialSecurityWorksheet().calculateTaxableAmount(form.agi(), form.income(),
//						form.getScheduleD()));
//	}
//
//	@Test
//	public void testAdjustments() {
//		assertEquals(EXP_ADJUSTMENTS, form.agi().calculateTotalAdjustment());
//	}
//
//	@Test
//	public void testAGI() throws Exception {
//		assertEquals(EXP_AGI, form.calculateAGI());
//	}
//
//	@Test
//	public void testDeductions() throws Exception {
//		// Should be itemized, not standard
//		assertEquals(EXP_DEDUCTIONS,
//				form.taxAndCredits().calculateDeductions(form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testExemptions() {
//		assertEquals(EXP_EXEMPTIONS, form.calculateExemptions());
//	}
//
//	@Test
//	public void testTaxableIncome() {
//		assertEquals(EXP_TAXABLE_INCOME,
//				form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
//						form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction
//								()));
//	}
//
//	@Test
//	public void testTotalCredits() {
//		assertEquals(EXP_TOTAL_CREDITS, form.taxAndCredits().calculateTotalCredits());
//	}
//
//	@Test
//	public void testCapitalGainsIncome() {
//		// As a single filer, you can only claim 3000 in losses.
//		assertThat(form.getScheduleD().calculateCapitalGains(), Is.is(EXP_CAPITAL_GAINS));
//	}
//
//	@Test
//	public void testTotalTaxAndCredits() {
//		assertEquals(EXP_TAX_LESS_CREDITS, form.calculateTaxAndCredits());
//	}
//
//	@Test
//	public void testOtherTaxes() throws Exception {
//		assertEquals(EXP_OTHER_TAXES, form.calculateOtherTaxes());
//	}
//
//	@Test
//	public void testPayments() throws Exception {
//		assertEquals(EXP_PAYMENTS, form.calculateTotalPayments());
//	}
//
//	@Test
//	public void testTaxesDue() throws Exception {
//		assertEquals(EXP_TAXES_DUE, form.calculateTaxOwed());
//	}
}
