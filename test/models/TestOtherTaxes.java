package models;

import junit.framework.TestCase;
import models.OtherTaxes;

/**
 * TestOtherTaxes.java
 * <p>
 * </p>
 * Created by piper on 10/22/15.
 */
public class TestOtherTaxes extends TestCase {

    public void testCalculateOtherTaxes() throws Exception {
        OtherTaxes otherTaxes = new OtherTaxes(1,0,0,0);

        // Check simple calculation
        assertEquals(otherTaxes.getOtherTaxes(), 1);
    }
}
