package models;

import models.pojo.FilingStatus;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by BerlGeof on 11/6/2015.
 */
public class TestRetiredHusbandAndWife {

	private static TenFortyCalculator form;

//	@BeforeClass
//	public static void setup() {
//
//		Retiree retiree = new Retiree("Retired", "Couple", new Date(),
//				new FilingStatus(1, FilingStatus.options().get("1")), 2);
//
//		form = new TenFortyCalculator(retiree);
//
//		form.addOrdinaryDividends(15000);
//		form.addLongTermCapitalGains(5000);
//		form.addPensionsAndAnnuities(70000);
//		form.setSocialSecurityIncome(33000);
//		form.setTotalItemizedDeductionAmount(15000);
//	}
//
//	@Test
//	public void testTotalIncome() throws Exception {
//		// Note: total income should be total of income values minus 15% of SS Income
//		assertEquals(118050, form.calculateTotalIncome());
//	}
//
//	@Test
//	public void testAdjustments() {
//		assertEquals(0, form.agi().calculateTotalAdjustment());
//	}
//
//	@Test
//	public void testAGI() throws Exception {
//		assertEquals(118050, form.calculateAGI());
//	}
//
//	@Test
//	public void testDeductions() throws Exception {
//		// Should be itemized, not standard
//		assertEquals(15000, form.taxAndCredits().calculateDeductions(form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testExemptions() {
//		assertEquals(7900, form.calculateExemptions());
//	}
//
//	@Test
//	public void testTaxableIncome() {
//		assertEquals(95150, form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
//				form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testTotalCredits() {
//		assertEquals(0, form.taxAndCredits().calculateTotalCredits());
//	}
//
//	@Test
//	public void testTotalTaxAndCredits() {
//		int taxableIncome = form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
//				form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction());
//		int qualifiedDiv = form.income().getQualifiedDividends();
//
//		System.out.println(
//				"TAX: " + form.getScheduleD().calculateTaxOnIncomeWithGains(form.getTaxBrackets(),
//						taxableIncome, qualifiedDiv, form.getCapitalGainsBrackets(),
//						form.getTaxComputationBrackets()));
//
//		System.out.println("Schedule D WS: " + form.getScheduleD().calculateScheduleDTaxWorksheet(
//				taxableIncome, qualifiedDiv, form.getTaxBrackets(),
//				form.getTaxComputationBrackets(), form.getCapitalGainsBrackets()));
//
//		System.out.println(
//				"QUALIFIED GAINS WS: " + form.getScheduleD()
//						.calculateQualifiedDividendsAndCapitalGainTaxWorksheet(
//						form.getTaxBrackets(), form.getTaxComputationBrackets(),
//						form.getCapitalGainsBrackets(), taxableIncome, qualifiedDiv));
//
//		System.out.println("CAP GAINS: " + form.getScheduleD().getCapitalGains());
//
//		System.out.println(form.getTaxAndCreditsSummary());
//
//
//		// TODO: Using the ginormous tax table it comes out to 15006...
//		assertEquals(15000, form.calculateTaxAndCredits());
//	}
//
//	@Test
//	public void testOtherTaxes() throws Exception {
//		assertEquals(0, form.calculateOtherTaxes());
//	}
//
//	@Test
//	public void testPayments() throws Exception {
//		assertEquals(0, form.calculateTotalPayments());
//	}
//
//	@Test
//	public void testTaxesDue() throws Exception {
//		// TODO: Using the ginormous tax table it comes out to 15006...
//		assertEquals(15000, form.calculateTaxOwed());
//	}

}
