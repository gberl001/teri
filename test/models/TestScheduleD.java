package models;

import models.pojo.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * TestScheduleD.java
 * <p>
 * A test to ensure that the ScheduleD class is calculating correctly.
 * <p>
 * Created by BerlGeof on 11/6/2015.
 */
public class TestScheduleD {

	private static ScheduleD scheduleD;
	private static TaxThresholds taxThresholds;
	private static final int TAXABLE_INCOME = 96464;
	private static final int QUALIFIED_DIVIDENDS = 196;
	private static final int EXPECTED_NET_CAPITAL_GAIN = 11039;

	@BeforeClass
	public static void setup() {
		// Create Schedule D with Net short and Long term values respectively
		Set<TaxBracket> married14 = new TreeSet<>();
		Set<TaxBracket> taxComputationBrackets = new TreeSet<>();
		Set<ThresholdBracket> capGainsBrackets = new TreeSet<>();
		married14.add(new TaxBracket(0, 18150, 0.1));
		married14.add(new TaxBracket(18151, 73800, 0.15));
		married14.add(new TaxBracket(73801, 148850, 0.25));
		married14.add(new TaxBracket(148851, 226850, 0.28));
		married14.add(new TaxBracket(226851, 405100, 0.33));
		married14.add(new TaxBracket(405101, 457600, 0.35));
		married14.add(new TaxBracket(457601, Integer.MAX_VALUE, 0.396));
		taxComputationBrackets.add(new TaxBracket(100000, 148850, 8287.5, 0.25));
		taxComputationBrackets.add(new TaxBracket(148851, 226850, 12753, 0.28));
		taxComputationBrackets.add(new TaxBracket(226851, 405100, 24095.5, 0.33));
		taxComputationBrackets.add(new TaxBracket(405101, 457600, 32197.5, 0.35));
		taxComputationBrackets.add(new TaxBracket(457601, Integer.MAX_VALUE, 53247.1, 0.396));
		capGainsBrackets.add(new ThresholdBracket(73800, 0.15));
		capGainsBrackets.add(new ThresholdBracket(457600, 0.25));

		taxThresholds = new TaxThresholds(1, 2014, taxComputationBrackets, capGainsBrackets,
				married14, null, 12400, 100000, 3000,
				new FilingStatus(1, FilingStatus.MARRIED_JOINTLY));

		scheduleD = new ScheduleD(-1187, 12226, 3000);
	}

	@Test
	public void testCalculateNetCapitalGain() throws Exception {
		// Net capital gains should be short term plus long term gains
		assertEquals(EXPECTED_NET_CAPITAL_GAIN, scheduleD.calculateTotalNetGains());
	}

	@Test
	public void testCalculate28Percent() throws Exception {
		// This should always return 0
		assertEquals(0, scheduleD.calculate28PercentRateGainWorksheet());
	}

	@Test
	public void testCalculateUnrecapturedGainsWS() {
		// This should always return 0
		assertThat(scheduleD.calculateUnrecapturedGains(), is(0));
	}

	@Test
	public void testQualifiedGainsAndDividends() throws Exception {
		assertEquals(14704,
				scheduleD.calculateQualifiedDividendsAndCapitalGainTaxWorksheet(taxThresholds,
						TAXABLE_INCOME, QUALIFIED_DIVIDENDS));
	}

	@Test
	public void testScheduleDTaxWorksheet() throws Exception {
		assertThat(scheduleD.calculateScheduleDTaxWorksheet(TAXABLE_INCOME, QUALIFIED_DIVIDENDS,
				taxThresholds), is(14704));
	}

}
