package models;

import models.pojo.FilingStatus;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by BerlGeof on 11/6/2015.
 */
public class TestRetiredGuy {

	private static TenFortyCalculator form;

	private static final int EXP_ADJUSTMENTS = 0;
	private static final int EXP_CAPITAL_GAINS = -3000;
	private static final int EXP_AGI = 93500;
	private static final int EXP_DEDUCTIONS = 13500;
	private static final int EXP_EXEMPTIONS = 3950;
	private static final int EXP_TOTAL_INCOME = 93500;
	private static final int EXP_TAXABLE_INCOME = 76050;
	private static final int EXP_TOTAL_CREDITS = 0;
	// TODO: Actually computes to 14875 when using ginormous tax table
	private static final int EXP_TAX_LESS_CREDITS = 14868;
	private static final int EXP_OTHER_TAXES = 0;
	private static final int EXP_PAYMENTS = 0;
	// TODO: Actually computes to 14875 when using ginormous tax table
	private static final int EXP_TAXES_DUE = 14868;

//	@BeforeClass
//	public static void setup() {
//
//		Retiree retiree = new Retiree("Retired", "Guy", new Date(),
//				new FilingStatus(2, FilingStatus.options().get("2")), 1);
//
//		form = new TenFortyCalculator(retiree);
//
//		form.addOrdinaryDividends(2500);
//		form.addLongTermCapitalGains(-3000);
//		form.addPensionsAndAnnuities(60000);
//		form.setSocialSecurityIncome(40000);
//		form.setTotalItemizedDeductionAmount(13500);
//	}
//
//	@Test
//	public void testTotalIncome() throws Exception {
//		// Note: total income should be total of income values minus 15% of SS Income
//		assertEquals(EXP_TOTAL_INCOME, form.calculateTotalIncome());
//	}
//
//	@Test
//	public void testAdjustments() {
//		assertEquals(EXP_ADJUSTMENTS, form.agi().calculateTotalAdjustment());
//	}
//
//	@Test
//	public void testAGI() throws Exception {
//		assertEquals(EXP_AGI, form.calculateAGI());
//	}
//
//	@Test
//	public void testDeductions() throws Exception {
//		// Should be itemized, not standard
//		assertEquals(EXP_DEDUCTIONS, form.taxAndCredits().calculateDeductions(form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testExemptions() {
//		assertEquals(EXP_EXEMPTIONS, form.calculateExemptions());
//	}
//
//	@Test
//	public void testTaxableIncome() {
//		assertEquals(EXP_TAXABLE_INCOME, form.taxAndCredits().calculateTaxableIncome(form.calculateAGI(),
//				form.getExemptions(), form.getFilingStatus(), form.getStandardDeduction()));
//	}
//
//	@Test
//	public void testTotalCredits() {
//		assertEquals(EXP_TOTAL_CREDITS, form.taxAndCredits().calculateTotalCredits());
//	}
//
//	@Test
//	public void testCapitalGainsIncome() {
//		// As a single filer, you can only claim 3000 in losses.
//		assertThat(form.getScheduleD().calculateCapitalGains(), is(EXP_CAPITAL_GAINS));
//	}
//
//	@Test
//	public void testTotalTaxAndCredits() {
//		assertEquals(EXP_TAX_LESS_CREDITS, form.calculateTaxAndCredits());
//	}
//
//	@Test
//	public void testOtherTaxes() throws Exception {
//		assertEquals(EXP_OTHER_TAXES, form.calculateOtherTaxes());
//	}
//
//	@Test
//	public void testPayments() throws Exception {
//		assertEquals(EXP_PAYMENTS, form.calculateTotalPayments());
//	}
//
//	@Test
//	public void testTaxesDue() throws Exception {
//		assertEquals(EXP_TAXES_DUE, form.calculateTaxOwed());
//	}

}
