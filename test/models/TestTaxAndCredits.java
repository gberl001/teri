package models;

import junit.framework.TestCase;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * TestTaxAndCredits.java
 * <p>
 * <p>
 * <p>
 * Created by geoffberl on 10/21/15.
 */
public class TestTaxAndCredits extends TestCase {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public void testGetStandardDeduction() throws Exception {

    }
    public void testGetCalculateDeduction() throws Exception{

    }

    public void testGetCredits() throws Exception {
//        Set<TaxBracket> married14 = new TreeSet<>();
//        married14.add(new TaxBracket(0, 18150, 1));
        TaxAndCredits taxAndCredits = new TaxAndCredits();

        taxAndCredits.setForeignTaxCredit(1);
        taxAndCredits.setDependentExpenses(1);
        taxAndCredits.setEduCredits(1);
        taxAndCredits.setRetirementSavingsCredit(1);
        taxAndCredits.setChildTaxCredit(1);
        taxAndCredits.setResidentialEnergyCredits(1);
        taxAndCredits.setOtherCredits(1);

        // Check simple calculation
        assertEquals(taxAndCredits.getCredits(), 7);
    }
}
