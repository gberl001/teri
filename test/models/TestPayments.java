package models;

import junit.framework.TestCase;

/**
 * TestPayments.java
 * <p>
 * </p>
 * Created by piper on 10/22/15.
 */
public class TestPayments extends TestCase {

    public void testCalculatePayments() throws Exception {
        Payments payments = new Payments(1,0,0,0,0);

        // Check simple calculation
        assertEquals(payments.calculateTotalPayments(), 1);
    }
}
