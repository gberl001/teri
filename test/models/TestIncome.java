package models;

import models.pojo.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * TestIncome.java
 *
 * A test to ensure that the Income calculator is functioning properly.
 *
 * Created by BerlGeof on 11/3/2015.
 */
public class TestIncome {

	private static Income income;
	private static ScheduleD scheduleD;
	private static SSIWorksheet ssiWorksheet;

	@BeforeClass
	public static void setup() {
		// Stuff normally from the database
		Set<TaxBracket> taxBrackets = new TreeSet<>();
		Set<TaxBracket> taxComputationBrackets = new TreeSet<>();
		Set<ThresholdBracket> capGainsBrackets = new TreeSet<>();
		Set<ThresholdBracket> magiBrackets = new TreeSet<>();
		taxBrackets.add(new TaxBracket(0, 18150, 0.1));
		taxBrackets.add(new TaxBracket(18151, 73800, 0.15));
		taxBrackets.add(new TaxBracket(73801, 148850, 0.25));
		taxBrackets.add(new TaxBracket(148851, 226850, 0.28));
		taxBrackets.add(new TaxBracket(226851, 405100, 0.33));
		taxBrackets.add(new TaxBracket(405101, 457600, 0.35));
		taxBrackets.add(new TaxBracket(457601, Integer.MAX_VALUE, 0.396));
		taxComputationBrackets.add(new TaxBracket(100000, 148850, 8287.5, 0.25));
		taxComputationBrackets.add(new TaxBracket(148851, 226850, 12753, 0.28));
		taxComputationBrackets.add(new TaxBracket(226851, 405100, 24095.5, 0.33));
		taxComputationBrackets.add(new TaxBracket(405101, 457600, 32197.5, 0.35));
		taxComputationBrackets.add(new TaxBracket(457601, Integer.MAX_VALUE, 53247.1, 0.396));
		capGainsBrackets.add(new ThresholdBracket(36900, 0.15));
		capGainsBrackets.add(new ThresholdBracket(457600, 0.2));
		magiBrackets.add(new ThresholdBracket(32000, 0.5));
		magiBrackets.add(new ThresholdBracket(44000, 0.85));

		TaxThresholds taxThresholds = new TaxThresholds(1, 2014, taxComputationBrackets,
				capGainsBrackets, taxBrackets, magiBrackets, 12400, 100000, 3000,
				new FilingStatus(1, FilingStatus.MARRIED_JOINTLY));

		// Input form values for income
		income = new Income();
		int qualifiedDividends = 196;
		income.setQualifiedDividends(qualifiedDividends);
		income.setPensionsAndAnnuities(70000);

		// Add schedule D for capital gains
		scheduleD = new ScheduleD(-1187, 12226, 3000);

		// Add SSI worksheet for SSI
		ssiWorksheet = new SSIWorksheet(taxThresholds);
		ssiWorksheet.setSocialSecurityIncome(33000);
	}

	@Test
	public void testTotalIncome() {
		// Should be the sum of all income except only add $28,050 from SSI.
		assertThat(income.calculateIncome(ssiWorksheet, scheduleD, null), is(109285));
	}


}