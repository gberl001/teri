package models;

import junit.framework.TestCase;
import models.pojo.FilingStatus;
import models.pojo.TaxThresholds;
import models.pojo.ThresholdBracket;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * TestSSIWorksheet.java
 * <p>
 * A test to ensure that the SSI Worksheet class is functioning properly
 * <p>
 * Created by BerlGeof on 11/16/2015.
 */
public class TestSSIWorksheet extends TestCase {

	@Test
	public void testCalculateTaxableAmount() throws Exception {

		// Create a set of Married MAGI brackets
		Set<ThresholdBracket> magiBrackets = new TreeSet<>();
		magiBrackets.add(new ThresholdBracket(32000, 0.5));
		magiBrackets.add(new ThresholdBracket(44000, 0.85));

		// Create the threshold object (only MAGI brackets are necessary)
		TaxThresholds taxThresholds = new TaxThresholds(1, 2014, null, null, null, magiBrackets,
				12400, 100000, 3000, new FilingStatus(1, FilingStatus.MARRIED_JOINTLY));

		Income income = new Income();
		income.setWages(50000);

		SSIWorksheet ssiWS = new SSIWorksheet(taxThresholds);
		ssiWS.setSocialSecurityIncome(33000);
		// Prove that having 33,000 SSI results in a taxable amount of $28,050.
		assertThat(ssiWS.calculateTaxableAmount(null, income, null), is(28050));
	}
}