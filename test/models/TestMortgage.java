package models;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by BerlGeof on 11/6/2015.
 */
public class TestMortgage {


	private static Mortgage mortgage;

	@BeforeClass
	public static void setup() {
		// TODO: Create Mortgage test, currently there are no real methods to test, just a summary
		mortgage = new Mortgage(100000, 30, 0.035, new Date(2000, 1, 1));
		System.out.println(mortgage.getAmortizationSummary());
	}

	@Test
	public void testInterestDue() throws Exception {
		assertEquals(true, true);
	}

	@Test
	public void testPrincipalDue() throws Exception {
		assertEquals(true, true);
	}

	@Test
	public void testMonthlyPayment() throws Exception {
		assertEquals(true, true);
	}

}
