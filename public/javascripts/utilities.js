// Utilities that may be used across the app.
//
// - number formatting
// - date picker
$(document).ready(function() {
    // Date picker
    $(document).ready(function() {
        $(".date-picker").datepicker({
            minViewMode: 1  // don't drop down to single day view
        });
    });

    // Create number formatter: $42,000
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    });

    // Select and replace span text with currency format
    $(".format-dollar").each(function() {
        var formattedText = formatter.format($(this).text());
        $(this).text(formattedText);
    });
});
