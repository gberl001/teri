// Building the Summary table with DataTables, as well
// as the jQuery UI panel. Also formatting the currency
// values in the table itself.
$(document).ready(function() {

    // Override settings dropdown, as DataTables polluting navbar namespace
    var settingsToggle = $('#settingsDropdown');
    settingsToggle.click(function() {
        if (settingsToggle.hasClass('open')) {
            settingsToggle.removeClass('open');
        } else {
            settingsToggle.addClass('open');
        }
    });

    // Initialize tabs
    $("#tabbedContainer").tabs({
        active: 0  // start on the baseline tab
    });

    // Create a DataTable from the summary table
    $('#summaryTable').DataTable({
        dom: '<"top"f>ti<"bottom-button-row pull-right button-default-override"B><"clear">',  // layout of buttons
        buttons: [
            'excel'
        ],
        order: [],
        scrollX: true,
        iDisplayLength: 50
    });

    // Create a DataTable from the summary table
    $('#optimizedTable').DataTable({
        dom: '<"top"f>ti<"bottom-button-row pull-right button-default-override"B><"clear">',  // layout of buttons
        buttons: [
            'excel'
        ],
        order: [],
        scrollX: true,
        iDisplayLength: 50
    });


    // Create number formatter: $42,000
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    });

    // Select and replace span text with currency format
    $(".format-dollar").each(function() {
        var formattedText = formatter.format($(this).text());
        $(this).text(formattedText);
    });
});